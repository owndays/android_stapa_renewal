package com.owndays.stapa.welcome.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.owndays.stapa.BaseViewModel
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.Event
import com.owndays.stapa.data.Result.Success
import com.owndays.stapa.data.source.remote.model.request.LoginRequest
import com.owndays.stapa.data.source.remote.model.response.LoginResponse
import com.owndays.stapa.data.source.repository.UserRepository
import kotlinx.coroutines.launch

class WelcomeViewModel(
    private val userRepository: UserRepository
) : BaseViewModel(){

    private val _dataLoading = MutableLiveData<Boolean>().apply { value = false }
    val dataLoading : LiveData<Boolean> = _dataLoading

    // Two-way databinding, exposing MutableLiveData
    val username = MutableLiveData<String>().apply { value = "" }
    val password = MutableLiveData<String>().apply { value = "" }

    //Event
    private val _onLoginComplete = MutableLiveData<Event<Int>>()
    val onLoginComplete: LiveData<Event<Int>> = _onLoginComplete

    private val _onLoadUserComplete = MutableLiveData<Event<String>>()
    val onLoadUserComplete: LiveData<Event<String>> = _onLoadUserComplete

    private val _onCheckLanguage = MutableLiveData<Event<String>>()
    val onCheckLanguage: LiveData<Event<String>> = _onCheckLanguage

    private val _onPopupApiError = MutableLiveData<Event<String>>()
    val onPopupApiError: LiveData<Event<String>> = _onPopupApiError

    private val _onPopupError = MutableLiveData<Event<Int>>()
    val onPopupError: LiveData<Event<Int>> = _onPopupError

    private var accessToken = ""

    init {
        accessToken = ServiceLocator.getUserToken()
    }

    fun validateLogin(){
        if(!username.value.isNullOrEmpty() && !password.value.isNullOrEmpty())
            logIn(username.value?.toInt()?:0, password.value?.toInt()?:0)
        else _onPopupError.value = Event(R.string.err_msg_001)
    }

    fun loadUser(){
        viewModelScope.launch {
            val resultUserToken = userRepository.loadUserToken()
            if(resultUserToken is Success){
                ServiceLocator.setUserToken(resultUserToken.data)
                _onLoadUserComplete.value = Event(resultUserToken.data)
            }else _onLoadUserComplete.value = Event("")
        }
    }

    private fun logIn(_userName: Int, _password: Int){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.logIn(ServiceLocator.getLanguage(), LoginRequest(_userName, _password),
                { _loginResponse ->
                    ServiceLocator.setUserToken(_loginResponse.token?:"")
                    saveLoginToken(_loginResponse)
                    _dataLoading.value = false
                    _onLoginComplete.value = Event(_loginResponse.registerStatus?:0)
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message?:"")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun saveLoginToken(loginResponse: LoginResponse){
        viewModelScope.launch {
            userRepository.saveUserToken(loginResponse)
        }
    }
}