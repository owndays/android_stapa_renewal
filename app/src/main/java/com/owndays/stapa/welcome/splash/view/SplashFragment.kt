package com.owndays.stapa.welcome.splash.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.SplashFragmentBinding
import com.owndays.stapa.main.view.LOGIN_FRAGMENT
import com.owndays.stapa.main.view.MILE_FRAGMENT
import com.owndays.stapa.utils.getViewModelFactory
import com.owndays.stapa.welcome.view.WelcomeActivity
import com.owndays.stapa.welcome.viewmodel.WelcomeViewModel

class SplashFragment : Fragment(){

    private lateinit var viewBinding : SplashFragmentBinding
    private val welcomeViewModel by viewModels<WelcomeViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = SplashFragmentBinding.inflate(layoutInflater)
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()

        welcomeViewModel.loadUser()
    }

    private fun setupView(){

    }

    private fun setupEvent(){
        welcomeViewModel.apply {
            onLoadUserComplete.observe(viewLifecycleOwner, EventObserver { _token ->
                if(_token.isNullOrEmpty())
                    (activity as WelcomeActivity).fragmentNavigate(LOGIN_FRAGMENT)
                else (activity as WelcomeActivity).fragmentNavigate(MILE_FRAGMENT)
            })
        }
    }
}