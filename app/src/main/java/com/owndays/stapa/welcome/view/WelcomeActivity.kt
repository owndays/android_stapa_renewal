package com.owndays.stapa.welcome.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.owndays.stapa.BaseActivity
import com.owndays.stapa.R
import com.owndays.stapa.databinding.WelcomeActivityBinding
import com.owndays.stapa.main.view.LOGIN_FRAGMENT
import com.owndays.stapa.main.view.MILE_FRAGMENT
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.utils.LocaleManager

class WelcomeActivity : BaseActivity(){

    private lateinit var viewBinding : WelcomeActivityBinding
    private lateinit var navController : NavController

    //Language re-config
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase?:baseContext))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = WelcomeActivityBinding.inflate(layoutInflater).apply {
            setContentView(root)

            navController = findNavController(R.id.fragmentContainer)
        }
    }

    override fun onBackPressed() {
        //TODO: Double Back Exit
    }

    fun fragmentNavigate(fragmentCode : String){
        when(fragmentCode){
            LOGIN_FRAGMENT -> {
                navController.navigate(R.id.login_fragment_dest)
            }
            MILE_FRAGMENT -> {
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
            else -> { }
        }
    }
}