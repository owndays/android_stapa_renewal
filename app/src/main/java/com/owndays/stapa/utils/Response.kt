package com.owndays.stapa.utils

import android.text.TextUtils
import com.google.gson.Gson
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import retrofit2.Response
import timber.log.Timber

fun <T> Response<T>.getError(): ErrorResponse? {
    try {
        val errorBody = this.errorBody()?.string()
        if (!TextUtils.isEmpty(errorBody)) {
            Timber.d("errorBody $errorBody")
            return Gson().fromJson(errorBody, ErrorResponse::class.java)
        }
    } catch (e: Exception) {
        Timber.e(e, "errorBody")
        e.printStackTrace()
    }
    return null
}
