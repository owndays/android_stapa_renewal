package com.owndays.stapa.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.os.Build

object NetworkUtil {
    private var networkStatus = false
    private lateinit var mContext : Context

    fun setContext(context : Context){
        mContext = context
        setupNetworkCallback()
    }

    private fun setupNetworkCallback(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val cm = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            cm.registerDefaultNetworkCallback(object : ConnectivityManager.NetworkCallback() {
                override fun onAvailable(network: Network) {
                    networkStatus = true
                }

                override fun onLost(network: Network) {
                    networkStatus = false
                }
            })
        }
    }
}