package com.owndays.stapa.utils

import android.webkit.JavascriptInterface

class GachaWebViewJSHandler(private val onJSCallback : (message: String) -> Unit ) {

    /** Show a toast from the web page  */
    @JavascriptInterface
    fun gachaCallbackHandler(message: String) {
        onJSCallback.invoke(message)
    }
}