package com.owndays.stapa.utils

import java.text.SimpleDateFormat
import java.util.*

fun String.toDateUnitTimeFromPattern(pattern: String): Long {
    val parser = SimpleDateFormat(pattern)
    return parser.parse(this).time
}

fun String.toDateFromCustomPattern(pattern: String): Date {
    return SimpleDateFormat(pattern, Locale.getDefault()).parse(this)
}

fun Date.toCustomPatternDateString(pattern: String): String {
    return SimpleDateFormat(pattern, Locale.getDefault()).format(this)
}

fun String.toNewPatternDateString(oldPattern: String, newPattern: String): String {
    return if(this.isNotEmpty()) this.toDateFromCustomPattern(oldPattern).toCustomPatternDateString(newPattern)
    else ""
}