package com.owndays.stapa.utils

import androidx.fragment.app.Fragment
import com.owndays.stapa.StapaApplication
import com.owndays.stapa.ViewModelFactory

fun Fragment.getViewModelFactory() : ViewModelFactory {
    val bloodRepository = (requireContext().applicationContext as StapaApplication).bloodRepository
    val ideaRepository = (requireContext().applicationContext as StapaApplication).ideaRepository
    val languagesRepository = (requireContext().applicationContext as StapaApplication).languageRepository
    val notificationRepository = (requireContext().applicationContext as StapaApplication).notificationRepository
    val orderRepository = (requireContext().applicationContext as StapaApplication).orderRepository
    val registerFormRepository = (requireContext().applicationContext as StapaApplication).registerFormRepository
    val shopRepository = (requireContext().applicationContext as StapaApplication).shopRepository
    val staffRepository = (requireContext().applicationContext as StapaApplication).staffRepository
    val userRepository = (requireContext().applicationContext as StapaApplication).userRepository
    return ViewModelFactory(
        bloodRepository,
        ideaRepository,
        languagesRepository,
        notificationRepository,
        orderRepository,
        registerFormRepository,
        shopRepository,
        staffRepository,
        userRepository,
        this
    )
}