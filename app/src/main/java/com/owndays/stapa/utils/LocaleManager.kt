package com.owndays.stapa.utils

import android.content.Context
import android.content.res.Configuration
import com.owndays.stapa.ServiceLocator
import java.util.*

//Hate to do this but no choice, since the application must be language-changeable
object LocaleManager{

    fun setLocale(context: Context): Context {
        return updateLanguage(context, ServiceLocator.getLanguage())
    }

    private fun updateLanguage(context: Context?, language: String?): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)

        val config = Configuration(context!!.resources.configuration)
        config.setLocale(locale)
        return context.createConfigurationContext(config)
    }
}