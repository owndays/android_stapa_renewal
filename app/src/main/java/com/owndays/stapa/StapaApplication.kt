package com.owndays.stapa

import android.app.Application
import com.owndays.stapa.data.source.repository.*
import com.owndays.stapa.utils.NetworkUtil
import timber.log.Timber

class StapaApplication : Application(){

    val bloodRepository: BloodRepository
        get() = ServiceLocator.provideBloodRepository()
    val ideaRepository: IdeaRepository
        get() = ServiceLocator.provideIdeaRepository()
    val languageRepository: LanguageRepository
        get() = ServiceLocator.provideLanguageRepository()
    val notificationRepository: NotificationRepository
        get() = ServiceLocator.provideNotificationRepository()
    val orderRepository: OrderRepository
        get() = ServiceLocator.provideOrderRepository()
    val registerFormRepository: RegisterFormRepository
        get() = ServiceLocator.provideRegisterFormRepository()
    val shopRepository: ShopRepository
        get() = ServiceLocator.provideShopRepository(this)
    val staffRepository: StaffRepository
        get() = ServiceLocator.provideStaffRepository()
    val userRepository: UserRepository
        get() = ServiceLocator.provideUserRepository(this)

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        NetworkUtil.setContext(this)
    }
}