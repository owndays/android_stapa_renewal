package com.owndays.stapa

import android.content.Context
import android.content.Intent
import androidx.lifecycle.ViewModel
import com.owndays.stapa.welcome.view.WelcomeActivity

open class BaseViewModel : ViewModel(){

    fun forceLogout(context: Context){
        ServiceLocator.clearUserToken()
        val intent = Intent(context, WelcomeActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)
    }
}