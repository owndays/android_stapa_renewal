package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName

data class PurchaseRequest(
    @SerializedName("address_id") val addressId: Int?,
    @SerializedName("details") val details: List<ProductCartRequest>?
)