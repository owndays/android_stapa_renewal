package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Order(
    @SerializedName("id") val id: Int?,
    @SerializedName("code") val code: String?,
    @SerializedName("date") val date: String,
    @SerializedName("status") val status: String?,
    @SerializedName("details") val details: List<OrderDetail>,
    @SerializedName("address") val address: Address?
)