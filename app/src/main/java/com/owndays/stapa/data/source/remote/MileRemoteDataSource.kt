package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.model.Mile
import com.owndays.stapa.data.model.MileDetail
import com.owndays.stapa.data.source.MileDataSource
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class MileRemoteDataSource(private val owndaysService: OwndaysService, private val gson: Gson) : MileDataSource, BaseRemoteDataSource(){

    override suspend fun getUserMileWaitingList(accessToken: String, lang: String,
                                                successHandler: (List<MileDetail>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserMileWaitingList(accessToken, lang),
            { _mileDetailList ->
                val mileDetailList = gson.fromJson(gson.toJsonTree(_mileDetailList), Array<MileDetail>::class.java)
                successHandler(mileDetailList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun getUserMileHistoryList(accessToken: String, lang: String,
                                                successHandler: (Mile) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserMileHistoryList(accessToken, lang),
            { _mile ->
                val mile = gson.fromJson(gson.toJsonTree(_mile), Mile::class.java)
                successHandler(mile)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun getUserMileEarnList(accessToken: String, lang: String,
        year: String,
        month: String, successHandler: (List<MileDetail>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserMileEarnList(accessToken, lang, year, month),
            { _mileDetailList ->
                val mileDetailList = gson.fromJson(gson.toJsonTree(_mileDetailList), Array<MileDetail>::class.java)
                successHandler(mileDetailList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun getUserMileSpendList(accessToken: String, lang: String,
        year: String,
        month: String, successHandler: (List<MileDetail>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserMileSpendList(accessToken, lang, year, month),
            { _mileDetailList ->
                val mileDetailList = gson.fromJson(gson.toJsonTree(_mileDetailList), Array<MileDetail>::class.java)
                successHandler(mileDetailList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }
}