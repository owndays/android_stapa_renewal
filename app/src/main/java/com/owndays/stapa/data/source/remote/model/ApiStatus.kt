package com.owndays.stapa.data.source.remote.model

import com.google.gson.annotations.SerializedName

data class ApiStatus(
    @SerializedName("status") val status: Boolean?,
    @SerializedName("message") val message: String?,
    @SerializedName("mile_balance") val mileBalance: Int?,
    @SerializedName("language_code") val languageCode: String?,
    @SerializedName("error_code") val errorCode: String?
)