package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.model.Language
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface LanguageRepository {

    //remote
    suspend fun getLanguageList(accessToken: String, lang: String,
                                successHandler: (List<Language>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}