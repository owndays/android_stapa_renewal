package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.model.Notification
import com.owndays.stapa.data.source.NotificationDataSource
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class NotificationRemoteDataSource(private val owndaysService: OwndaysService, private val gson: Gson) : NotificationDataSource, BaseRemoteDataSource(){

    override suspend fun getUserNotificationList(accessToken: String, lang: String,
                                                 successHandler: (List<Notification>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserNotificationList(accessToken, lang),
            { _notificationList ->
                val notificationList = gson.fromJson(gson.toJsonTree(_notificationList), Array<Notification>::class.java)
                successHandler(notificationList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun getNotificationDetail(accessToken: String, lang: String,
        notificationId: String, successHandler: (Notification) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getNotificationDetail(accessToken, lang, notificationId),
            { _notification ->
                val notification = gson.fromJson(gson.toJsonTree(_notification), Notification::class.java)
                successHandler(notification)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }
}