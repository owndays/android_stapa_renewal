package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.model.Category
import com.owndays.stapa.data.source.IdeaDataSource
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.IdeaListResponse

class IdeaRemoteDataSource (private val owndaysService: OwndaysService, private val gson: Gson): IdeaDataSource, BaseRemoteDataSource(){

    override suspend fun getIdeaList(accessToken: String, lang: String,
                                     page: String,
                                     perPage: String,
                                     keyword: String,
                                     categoryId: String, successHandler: (IdeaListResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getIdeaList(accessToken, lang, page, perPage, keyword, categoryId),
            { _ideaListResponse ->
                val ideaListResponse = gson.fromJson(gson.toJsonTree(_ideaListResponse), IdeaListResponse::class.java)
                successHandler(ideaListResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun getUserIdeaList(accessToken: String, lang: String,
                                         page: String,
                                         perPage: String,
                                         owner: String,
                                         keyword: String,
                                         categoryId: String, successHandler: (IdeaListResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserIdeaList(accessToken, lang, page, perPage, owner, keyword, categoryId),
            { _ideaListResponse ->
                val ideaListResponse = gson.fromJson(gson.toJsonTree(_ideaListResponse), IdeaListResponse::class.java)
                successHandler(ideaListResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun getIdeaCategoryList(accessToken: String, lang: String,
                                             successHandler: (List<Category>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getIdeaCategoryList(accessToken, lang),
            { _categoryList ->
                val categoryList = gson.fromJson(gson.toJsonTree(_categoryList), Array<Category>::class.java)
                successHandler(categoryList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }
}