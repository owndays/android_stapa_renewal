package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Refine(
    @SerializedName("id") val id: Int?,
    @SerializedName("name") val name: String?
)

const val REFINE_ALL_PRODUCT = 1
const val REFINE_TRADABLE_PRODUCT = 2
const val REFINE_PICKUP_PRODUCT = 3