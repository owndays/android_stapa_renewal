package com.owndays.stapa.data.source.remote.model.response

import com.google.gson.annotations.SerializedName

data class FCMTokenResponse(
    @SerializedName("id") val id: Int?
)