package com.owndays.stapa.data.source.local

import com.owndays.stapa.data.Result
import com.owndays.stapa.data.Result.Error
import com.owndays.stapa.data.Result.Success
import com.owndays.stapa.data.dao.DigitalIdDao
import com.owndays.stapa.data.model.DigitalId
import com.owndays.stapa.data.source.DigitalIdDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DigitalIdLocalDataSource(
    private val digitalIdDao: DigitalIdDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : DigitalIdDataSource{
    override suspend fun getUserDigitalIdCard(accessToken: String, lang: String,
                                              successHandler: (DigitalId) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

    }

    override suspend fun saveDigitalId(digitalId: DigitalId) {
        digitalIdDao.insertOrUpdateDigitalId(digitalId)
    }

    override suspend fun loadDigitalId(): Result<DigitalId> {
        return withContext(ioDispatcher) {
            try {
                Success(digitalIdDao.getDigitalId())
            } catch (e: Exception) {
                Error(e)
            }
        }
    }

    override suspend fun clearDigitalId() {
        digitalIdDao.deleteDigitalId()
    }
}