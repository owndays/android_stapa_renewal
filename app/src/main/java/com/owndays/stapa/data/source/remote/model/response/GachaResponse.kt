package com.owndays.stapa.data.source.remote.model.response

import com.google.gson.annotations.SerializedName

data class GachaResponse(
    @SerializedName("month") val month: String?,
    @SerializedName("this_month") val this_month: Int?,
    @SerializedName("remain") val remain: Int?,
    @SerializedName("description") val description: String,
    @SerializedName("dates") val dates: List<Int>?
)