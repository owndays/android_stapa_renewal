package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Shop(
    @SerializedName("code") val id: Int?,
    @SerializedName("name") val name: String?,
    @SerializedName("address") val address: String?,
    @SerializedName("address2") val address2: String?,
    @SerializedName("distance") val distance: String?,
    @SerializedName("lat") val lat: String?,
    @SerializedName("lng") val lng: String?,
    @SerializedName("already_check_in") val alreadyCheckIn: Boolean?,
    @SerializedName("check_in_date") val checkInDate: String,
    @SerializedName("cal_distance") val calDistance: Float?
)