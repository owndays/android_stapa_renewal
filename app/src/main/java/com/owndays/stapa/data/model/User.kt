package com.owndays.stapa.data.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import com.google.gson.annotations.SerializedName

//@Entity(tableName = "user", primaryKeys = ["user_id"])
data class User(
    @ColumnInfo(name = "user_id") @SerializedName("id") val id: Int?,
    @ColumnInfo(name = "user_staff_id") @SerializedName("staff_id") val staffId: String?,
    @ColumnInfo(name = "user_name") @SerializedName("name") val name: String?,
    @ColumnInfo(name = "user_image_url") @SerializedName("image_url") val imageUrl: String?,
    @ColumnInfo(name = "user_blood_id") @SerializedName("blood_id") val bloodId: Int?,
    @ColumnInfo(name = "user_hobby") @SerializedName("hobby") val hobby: String?,
    @ColumnInfo(name = "user_country_id") @SerializedName("country_id") val countryId: Int?,
    @ColumnInfo(name = "user_language_id") @SerializedName("language_id") val languageId: Int?,
    @ColumnInfo(name = "user_department_name") @SerializedName("department_name") val departmentName: String?,
    @ColumnInfo(name = "user_image_base_64") @SerializedName("image_base_64") val imageBase64: String?,
    @ColumnInfo(name = "user_phone") @SerializedName("phone") val phone: String?,
    @ColumnInfo(name = "user_badges") @SerializedName("badges") val badges: Int?,
    var isSelected: Boolean = false,
    @Embedded @SerializedName("language") val language: Language?,
    @Embedded @SerializedName("blood") val blood: Blood?
)