package com.owndays.stapa.data.source

import com.owndays.stapa.data.model.ShiftData
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface ShiftDataSource {

    //remote
    suspend fun getUserShiftList(accessToken: String, lang: String,
                                 year: String,
                                 month: String,
                                 successHandler: (List<ShiftData>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}