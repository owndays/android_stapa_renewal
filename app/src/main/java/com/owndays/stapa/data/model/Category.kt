package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Category(
    @SerializedName("id") val id: Int?,
    @SerializedName("name") val name: String?
)

/*
* 0 : All of category
* 1 : Exclusive owndays
* 2 : V.I.P.
* 3 : Travel
* 4 : Sports
* 5 : Applience
* 6 : Beauty care
* 7 : Gourmet
* 8 : Furniture & home accessory
* 9 : Activity
* 10 : Relaxation
* 11 : Men's beauty
* 12 : Kitchen
* 13 : Hobby
* 14 : Lady's fashion
* 15 : Men's fashion
* 16 : Kids
* 17 : Other
* 18 : Pet
* 19 : Fitness
* */