package com.owndays.stapa.data.source

import com.owndays.stapa.data.model.Notification
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface NotificationDataSource {

    //remote
    suspend fun getUserNotificationList(accessToken: String, lang: String,
                                        successHandler: (List<Notification>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun getNotificationDetail(accessToken: String, lang: String,
                                      notificationId: String,
                                      successHandler: (Notification) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}