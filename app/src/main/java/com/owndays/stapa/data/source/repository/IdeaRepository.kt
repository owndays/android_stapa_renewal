package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.model.Category
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.IdeaListResponse

interface IdeaRepository {

    //remote
    suspend fun getIdeaList(accessToken: String, lang: String,
                            page: String,
                            perPage: String,
                            keyword: String,
                            categoryId: String, successHandler: (IdeaListResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun getUserIdeaList(accessToken: String, lang: String,
                                page: String,
                                perPage: String,
                                owner: String,
                                keyword: String,
                                categoryId: String, successHandler: (IdeaListResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun getIdeaCategoryList(accessToken: String, lang: String,
                                    successHandler: (List<Category>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}