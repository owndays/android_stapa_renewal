package com.owndays.stapa.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "digitalId", primaryKeys = ["digitalId_id"])
data class DigitalId(
    @ColumnInfo(name = "digitalId_id") @SerializedName("id") val id: Int?,
    @ColumnInfo(name = "digitalId_status") @SerializedName("status") val status: Int?,
    @ColumnInfo(name = "digitalId_department_name") @SerializedName("department_name") val departmentName: String?,
    @ColumnInfo(name = "digitalId_name") @SerializedName("name") val name: String?,
    @ColumnInfo(name = "digitalId_image_base_64") @SerializedName("image_base_64") val imageBase64: String?,
    @ColumnInfo(name = "digitalId_image_url") @SerializedName("image_url") val imageUrl: String?
)