package com.owndays.stapa.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "stock", primaryKeys = ["id"])
data class Stock(
    val id: Int,
    @ColumnInfo(name = "stock_qty") @SerializedName("qty") val qty: Int?,
    @ColumnInfo(name = "stock_limit_qty") @SerializedName("limit_qty") val limitQty: Int?,
    @ColumnInfo(name = "stock_display_type") @SerializedName("display_type") val displayType: Boolean?
)