package com.owndays.stapa.data.source

import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.RegisterFormResponse

interface RegisterFormDataSource {

    //remote
    suspend fun getRegisterForm(accessToken: String, lang: String,
                                queryLang: String,
                                successHandler: (RegisterFormResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}