package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class CheckInLog(
    @SerializedName("country_name") val countryName: String?,
    @SerializedName("provinces") val provinces: List<Province>?
)