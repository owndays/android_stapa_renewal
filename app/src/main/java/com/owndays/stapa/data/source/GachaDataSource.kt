package com.owndays.stapa.data.source

import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.GachaResponse

interface GachaDataSource {

    //remote
    suspend fun getUserGachaHistoryList(accessToken: String, lang: String,
                                        successHandler: (GachaResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}