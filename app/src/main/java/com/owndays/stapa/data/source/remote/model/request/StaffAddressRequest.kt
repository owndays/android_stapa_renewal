package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName

data class StaffAddressRequest(
    @SerializedName("id") val id: Int?,
    @SerializedName("name") val name: String?,
    @SerializedName("zip") val zip: String?,
    @SerializedName("province_id") val provinceId: Int?,
    @SerializedName("town") val town: String?,
    @SerializedName("city") val city: String?,
    @SerializedName("phone") val phone: String?,
    @SerializedName("other") val other: String?
)