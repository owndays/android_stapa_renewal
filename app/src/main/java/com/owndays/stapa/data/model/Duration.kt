package com.owndays.stapa.data.model

data class Duration(
    val date: Int,
    val dayOfWeek: Int,
    var isChecked: Boolean = false,
    var shiftData: ShiftData? = null
)

/*
* date
* 1 : SUN
* 2 : MON
* 3 : TUE
* 4 : WED
* 5 : THU
* 6 : FRI
* 7 : SAT
* */