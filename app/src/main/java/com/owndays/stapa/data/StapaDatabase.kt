package com.owndays.stapa.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.owndays.stapa.data.dao.DigitalIdDao
import com.owndays.stapa.data.dao.ShopDao
import com.owndays.stapa.data.dao.UserDao
import com.owndays.stapa.data.model.DigitalId
import com.owndays.stapa.data.model.Product
import com.owndays.stapa.data.model.Stock
import com.owndays.stapa.data.source.remote.model.response.LoginResponse

@Database(entities = [LoginResponse::class, DigitalId::class, Product::class, Stock::class], version = 1, exportSchema = true)
abstract class StapaDatabase : RoomDatabase(){

    abstract fun userDao(): UserDao

    abstract fun digitalIdDao(): DigitalIdDao

    abstract fun shopDao(): ShopDao
}