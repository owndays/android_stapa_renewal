package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

const val NORMAL_RANK = "Normal"
const val BRONZE_RANK = "Bronze"
const val SILVER_RANK = "Silver"
const val GOLD_RANK = "Gold"
const val PLATINUM_RANK = "Platinum"
const val DIAMOND_RANK = "Diamond"

data class Rank(
    @SerializedName("type") val type: String?,
    //Normal / Bronze / Silver / Gold / Platinum / Diamond
    @SerializedName("name") val name: String?,
    @SerializedName("next_rank") val nextRank: Int?,
    @SerializedName("message") val message: String?
)