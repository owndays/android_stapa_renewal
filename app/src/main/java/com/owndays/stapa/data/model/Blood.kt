package com.owndays.stapa.data.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Blood(
    @ColumnInfo(name = "blood_id") @SerializedName("id") val id: Int?,
    @ColumnInfo(name = "blood_name") @SerializedName("name") val name: String?
)