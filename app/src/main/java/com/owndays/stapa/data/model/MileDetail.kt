package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class MileDetail(
    @SerializedName("id") val id: Int?,
    @SerializedName("date") val date: String?,
    @SerializedName("image_url") val imageUrl: String?,
    @SerializedName("message") val message: String?,
    @SerializedName("comment") val comment: String?,
    @SerializedName("mile") val mile: Int?,
    @SerializedName("kind") val kind: Int?,
    @SerializedName("order_code") val orderCode: String?,
    @SerializedName("status") val status: String?
)