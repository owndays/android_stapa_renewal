package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.model.Notification
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface NotificationRepository {

    //remote
    suspend fun getNotificationDetail(accessToken: String, lang: String,
                                      notificationId: String,
                                      successHandler: (Notification) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}