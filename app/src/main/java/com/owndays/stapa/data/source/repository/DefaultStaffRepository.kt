package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.model.User
import com.owndays.stapa.data.source.StaffDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class DefaultStaffRepository(
    private val staffDataSource: StaffDataSource
) : StaffRepository{

    override suspend fun getStaffList(accessToken: String, lang: String,
                                      successHandler: (List<User>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        staffDataSource.getStaffList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }
}