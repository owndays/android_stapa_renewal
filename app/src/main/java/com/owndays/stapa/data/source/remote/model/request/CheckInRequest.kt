package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName

data class CheckInRequest(
    @SerializedName("shop_code") val ShopCode: String?
)