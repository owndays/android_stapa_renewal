package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.model.ShiftData
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.ShiftDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class ShiftRemoteDataSource(private val owndaysService: OwndaysService, private val gson: Gson) : ShiftDataSource, BaseRemoteDataSource(){

    override suspend fun getUserShiftList(accessToken: String, lang: String,
        year: String,
        month: String, successHandler: (List<ShiftData>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserShiftList(accessToken, lang, year, month),
            { _shiftDataList ->
                val shiftDataList = gson.fromJson(gson.toJsonTree(_shiftDataList), Array<ShiftData>::class.java)
                successHandler(shiftDataList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }
}