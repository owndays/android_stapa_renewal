package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class StaffCheckIn(
    @SerializedName("name") val name: String?,
    @SerializedName("image_url") val imageUrl: String?
)