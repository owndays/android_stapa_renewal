package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName
import com.owndays.stapa.data.model.Address
import com.owndays.stapa.data.model.DigitalId
import com.owndays.stapa.data.model.User

data class StaffRegisterRequest(
    @SerializedName("user") val user: User?,
    @SerializedName("digital_id") val digitalId: DigitalId?,
    @SerializedName("address") val address: Address?
)