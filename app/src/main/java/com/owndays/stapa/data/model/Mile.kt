package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Mile(
    @SerializedName("balance") val balance: Int?,
    @SerializedName("grand") val grand: Int?,
    @SerializedName("rank") val rank: Rank?,
    @SerializedName("histories") val histories: List<History>?
)