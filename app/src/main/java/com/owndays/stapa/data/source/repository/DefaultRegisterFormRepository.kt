package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.source.RegisterFormDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.RegisterFormResponse

class DefaultRegisterFormRepository(
    private val registerFormDataSource: RegisterFormDataSource
):RegisterFormRepository{

    override suspend fun getRegisterForm(accessToken: String, lang: String,
        queryLang: String, successHandler: (RegisterFormResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        registerFormDataSource.getRegisterForm(accessToken, lang, queryLang, successHandler, errorHandler, languageCallBack)
    }
}