package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class History(
    @SerializedName("year") val year: String?,
    @SerializedName("earn") val earnMileHistories: List<MileHistory>?,
    @SerializedName("spend") val spendMileHistories: List<MileHistory>?
)