package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.model.Language
import com.owndays.stapa.data.source.LanguageDataSource
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class LanguageRemoteDataSource(private val owndaysService: OwndaysService, private val gson: Gson) : LanguageDataSource, BaseRemoteDataSource(){

    override suspend fun getLanguageList(accessToken: String, lang: String,
                                         successHandler: (List<Language>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getLanguageList(accessToken, lang),
            { _languageList ->
                val languageList = gson.fromJson(gson.toJsonTree(_languageList), Array<Language>::class.java)
                successHandler(languageList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }
}