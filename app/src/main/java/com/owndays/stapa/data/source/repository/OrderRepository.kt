package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.model.Product
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.ProductListResponse
import com.owndays.stapa.data.source.remote.model.response.TopStoreResponse

interface OrderRepository {

    //remote
    suspend fun getProductList(accessToken: String, lang: String,
                               successHandler: (TopStoreResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun getProductFilteredList(accessToken: String, lang: String,
                                       refineId: String,
                                       categoryId: String,
                                       sortId: String,
                                       perPage: String,
                                       page: String,
                                       successHandler: (ProductListResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun getProductDetail(accessToken: String, lang: String,
                                 productId: String,
                                 successHandler: (Product) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}