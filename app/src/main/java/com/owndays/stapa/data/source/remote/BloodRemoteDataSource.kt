package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.model.Blood
import com.owndays.stapa.data.source.BloodDataSource
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class BloodRemoteDataSource (private val owndaysService: OwndaysService, private val gson: Gson): BloodDataSource, BaseRemoteDataSource(){

    override suspend fun getBloodList(accessToken: String, lang: String,
        queryLang: String, successHandler: (List<Blood>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getBloodList(accessToken, lang, queryLang),
            { _bloodList ->
                val bloodList = gson.fromJson(gson.toJsonTree(_bloodList), Array<Blood>::class.java)
                successHandler(bloodList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

}