package com.owndays.stapa.data.source.remote.model.response

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity(tableName = "login_response", primaryKeys = ["id"])
data class LoginResponse(
    val id: Int,
    @SerializedName("register_status") val registerStatus: Int?,
    @SerializedName("token") val token: String?,
    @SerializedName("country_id") val countryId: Int?
)