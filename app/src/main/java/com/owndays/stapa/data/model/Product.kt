package com.owndays.stapa.data.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import com.google.gson.annotations.SerializedName

@Entity(tableName = "product", primaryKeys = ["product_id"])
data class Product(
    @ColumnInfo(name = "product_id") @SerializedName("id") val id: Int?,
    @ColumnInfo(name = "product_name") @SerializedName("name") val name: String?,
    @ColumnInfo(name = "product_image_url") @SerializedName("image_url") var imageUrl: String?,
    @ColumnInfo(name = "product_maker") @SerializedName("maker") val maker: String?,
    @ColumnInfo(name = "product_mile") @SerializedName("mile") val mile: Int?,
    @ColumnInfo(name = "product_purchase_allow") @SerializedName("purchase_allow") val purchaseAllow: Boolean?,
    @Embedded @SerializedName("stock") val stock: Stock?,
    @ColumnInfo(name = "product_detail") @SerializedName("detail") val detail: String?,
    @ColumnInfo(name = "product_cart_qty") var cartQty: Int?
){
    @Ignore @SerializedName("image_urls") val imageUrls: List<String>? = emptyList()
}

/*
* Product Type
* 1 : All products
* 2 : Tradable products
* 3 : Pickup products
* */

/*
* Sort by
* 1 : High to low
* 2 : Low to high
* 3 : Newest
* 4 : Popular
* */