package com.owndays.stapa.data.source.remote.model.response

import com.google.gson.annotations.SerializedName

data class CheckInResponse(
    @SerializedName("user_shop") val userShop: String?,
    @SerializedName("check_in_shop") val checkInShop: String?,
    @SerializedName("mile") val mile: Int?
)