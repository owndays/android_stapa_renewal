package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName
import com.owndays.stapa.data.model.Document

data class IdeaRequest(
    @SerializedName("category_id") val categoryId: String?,
    @SerializedName("title") val title: String?,
    @SerializedName("what") val what: String?,
    @SerializedName("why") val why: String?,
    @SerializedName("how") val how: String?,
    @SerializedName("documents") val documents: List<Document>?
)