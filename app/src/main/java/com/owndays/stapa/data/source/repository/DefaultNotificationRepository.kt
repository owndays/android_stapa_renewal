package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.model.Notification
import com.owndays.stapa.data.source.NotificationDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class DefaultNotificationRepository (
    private val notificationDataSource: NotificationDataSource
): NotificationRepository{

    override suspend fun getNotificationDetail(accessToken: String, lang: String,
        notificationId: String, successHandler: (Notification) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        notificationDataSource.getNotificationDetail(accessToken, lang, notificationId, successHandler, errorHandler, languageCallBack)
    }
}