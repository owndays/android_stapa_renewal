package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.Result
import com.owndays.stapa.data.model.Product
import com.owndays.stapa.data.model.Shop
import com.owndays.stapa.data.model.ShopDetail
import com.owndays.stapa.data.source.ShopDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

class DefaultShopRepository (
    private val shopRemoteDataSource: ShopDataSource,
    private val shopLocalDataSource: ShopDataSource,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
): ShopRepository{

    override suspend fun getShopList(accessToken: String, lang: String,
                                     successHandler: (List<Shop>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        shopRemoteDataSource.getShopList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getShopDetail(accessToken: String, lang: String,
        shopCode: String, successHandler: (ShopDetail) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        shopRemoteDataSource.getShopDetail(accessToken, lang, shopCode, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun insertCart(productList: List<Product>) {
        withContext(ioDispatcher){
            coroutineScope { shopLocalDataSource.insertCart(productList) }
        }
    }

    override suspend fun getShopCart(): Result<List<Product>> {
        return shopLocalDataSource.getShopCart()
    }

    override suspend fun clearCart() {
        withContext(ioDispatcher){
            coroutineScope { shopLocalDataSource.clearCart() }
        }
    }
}