package com.owndays.stapa.data.source

import com.owndays.stapa.data.Result
import com.owndays.stapa.data.model.DigitalId
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface DigitalIdDataSource {

    //remote
    suspend fun getUserDigitalIdCard(accessToken: String, lang: String, successHandler: (DigitalId) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //local
    suspend fun saveDigitalId(digitalId: DigitalId)

    //local
    suspend fun loadDigitalId() : Result<DigitalId>

    //local
    suspend fun clearDigitalId()
}