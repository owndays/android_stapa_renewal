package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.model.Category
import com.owndays.stapa.data.source.IdeaDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.IdeaListResponse

class DefaultIdeaRepository (
    private val ideaDataSource: IdeaDataSource
): IdeaRepository{

    override suspend fun getIdeaList(accessToken: String, lang: String,
                                     page: String,
                                     perPage: String,
                                     keyword: String,
                                     categoryId: String, successHandler: (IdeaListResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        ideaDataSource.getIdeaList(accessToken, lang, page, perPage, keyword, categoryId, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserIdeaList(accessToken: String, lang: String,
                                         page: String,
                                         perPage: String,
                                         owner: String,
                                         keyword: String,
                                         categoryId: String, successHandler: (IdeaListResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        ideaDataSource.getUserIdeaList(accessToken, lang, page, perPage, owner, keyword, categoryId, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getIdeaCategoryList(accessToken: String, lang: String,
                                             successHandler: (List<Category>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        ideaDataSource.getIdeaCategoryList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }
}