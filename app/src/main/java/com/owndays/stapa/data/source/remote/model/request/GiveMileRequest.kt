package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName

data class GiveMileRequest(
    @SerializedName("staff_id") val staffId: String?,
    @SerializedName("mile") val mile: Int?,
    @SerializedName("message") val message: String?
)