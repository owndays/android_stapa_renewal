package com.owndays.stapa.data.source

import com.owndays.stapa.data.model.Language
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface LanguageDataSource {

    //remote
    suspend fun getLanguageList(accessToken: String, lang: String,
                                successHandler: (List<Language>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}