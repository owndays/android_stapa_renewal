package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.model.Address
import com.owndays.stapa.data.source.AddressDataSource
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class AddressRemoteDataSource(private val owndaysService: OwndaysService, private val gson: Gson) : AddressDataSource, BaseRemoteDataSource(){

    override suspend fun getUserAddressList(accessToken: String, lang: String,
                                            successHandler: (List<Address>) -> Unit,
                                            errorHandler: (ErrorResponse?) -> Unit,
                                            languageCallback: (String) -> Unit) {

        makeCall(owndaysService.getUserAddressList(accessToken, lang),
            { _adressList ->
                val addressList = gson.fromJson(gson.toJsonTree(_adressList), Array<Address>::class.java)
                successHandler(addressList.toList())
            },
            { errorHandler(it) },
            { languageCallback(it) }
        )
    }
}