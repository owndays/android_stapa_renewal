package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Paginate(
    @SerializedName("total") val total: Int?,
    @SerializedName("page") val page: Int?,
    @SerializedName("per_page") val perPage: Int?,
    @SerializedName("total_page") val totalPage: Int?
)