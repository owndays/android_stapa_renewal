package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Address(
    @SerializedName("id") val id: Int?,
    @SerializedName("default") val default: String?,
    @SerializedName("full_address") val fullAddress: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("zip") val zip: String?,
    @SerializedName("province") val province: Province?,
    @SerializedName("province_id") val provinceId: Int?,
    @SerializedName("town") val town: String?,
    @SerializedName("city") val city: String?,
    @SerializedName("street") val street: String?,
    @SerializedName("other") val other: String?,
    @SerializedName("phone") val phone: String?,
    var showDelete: Boolean = false
)