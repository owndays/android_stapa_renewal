package com.owndays.stapa.data.source.remote.model

import com.google.gson.annotations.SerializedName

const val UNAUTHENTICATED = "Unauthenticated"

data class ErrorResponse(@SerializedName("response_status") val responseStatus: ResponseStatus?){

    data class ResponseStatus (@SerializedName("status") val status: Boolean?,
                               @SerializedName("language_code") val languageCode: String?,
                               @SerializedName("message") val message: String?,
                               @SerializedName("mile_balance") val mileBalance: Int?,
                               @SerializedName("error_code") val errorCode: String?)
}