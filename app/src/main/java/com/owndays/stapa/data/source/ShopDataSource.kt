package com.owndays.stapa.data.source

import com.owndays.stapa.data.Result
import com.owndays.stapa.data.model.Product
import com.owndays.stapa.data.model.Shop
import com.owndays.stapa.data.model.ShopDetail
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface ShopDataSource {

    //remote
    suspend fun getShopList(accessToken: String, lang: String,
                            successHandler: (List<Shop>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun getShopDetail(accessToken: String, lang: String,
                              shopCode: String,
                              successHandler: (ShopDetail) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //local
    suspend fun insertCart(productList: List<Product>)

    //local
    suspend fun getShopCart(): Result<List<Product>>

    //local
    suspend fun clearCart()
}