package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Sort(
    @SerializedName("id") val id: Int?,
    @SerializedName("name") val name: String?
)