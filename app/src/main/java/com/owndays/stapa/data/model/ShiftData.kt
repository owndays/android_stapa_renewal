package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class ShiftData(
    @SerializedName("start_date") val startDate: String?,
    @SerializedName("end_date") val endDate: String?,
    @SerializedName("event_date") val eventDate: String?,
    @SerializedName("event_time") val eventTime: String?,
    @SerializedName("event_shop") val eventShop: String
)
//shiftData date format : yyyy/MM/dd