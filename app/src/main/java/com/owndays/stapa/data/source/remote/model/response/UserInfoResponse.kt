package com.owndays.stapa.data.source.remote.model.response

import com.google.gson.annotations.SerializedName
import com.owndays.stapa.data.model.DigitalId
import com.owndays.stapa.data.model.Mile
import com.owndays.stapa.data.model.User

data class UserInfoResponse(
    @SerializedName("user") val user: User?,
    @SerializedName("mile") val mile: Mile?,
    @SerializedName("digital_id") val digitalId: DigitalId?
)