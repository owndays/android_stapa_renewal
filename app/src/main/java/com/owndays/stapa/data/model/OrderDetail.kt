package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class OrderDetail(
    @SerializedName("qty") val id: Int?,
    @SerializedName("sub_total") val subTotal: Int?,
    @SerializedName("product") val product: Product?
)