package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName

data class ProfileUpdateRequest(
    @SerializedName("name") val name: String?,
    @SerializedName("image_base_64") val imageBase64: String?,
    @SerializedName("blood_id") val bloodId: Int?,
    @SerializedName("hobby") val hobby: String?
)