package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.Result
import com.owndays.stapa.data.model.*
import com.owndays.stapa.data.source.*
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.request.*
import com.owndays.stapa.data.source.remote.model.response.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext

class DefaultUserRepository(
    private val addressDataSource: AddressDataSource,
    private val checkInDataSource: CheckInDataSource,
    private val digitalIdDataSource: DigitalIdDataSource,
    private val digitalIdLocalDataSource: DigitalIdDataSource,
    private val gachaDataSource: GachaDataSource,
    private val mileDataSource: MileDataSource,
    private val notificationDataSource: NotificationDataSource,
    private val orderDataSource: OrderDataSource,
    private val provinceDataSource: ProvinceDataSource,
    private val shiftDataSource: ShiftDataSource,
    private val userRemoteDataSource: UserDataSource,
    private val userLocalDataSource: UserDataSource,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : UserRepository{

    override suspend fun addUserAddress(accessToken: String, lang: String,
        staffAddressRequest: StaffAddressRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.addUserAddress(accessToken, lang, staffAddressRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun editUserAddress(accessToken: String, lang: String,
        addressId: String,
        staffAddressRequest: StaffAddressRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.editUserAddress(accessToken, lang, addressId, staffAddressRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun deleteUserAddress(accessToken: String, lang: String,
        addressId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.deleteUserAddress(accessToken, lang, addressId, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserAddressList(accessToken: String, lang: String,
                                            successHandler: (List<Address>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        addressDataSource.getUserAddressList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserCheckInLogList(accessToken: String, lang: String,
                                               successHandler: (List<CheckInLog>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        checkInDataSource.getUserCheckInLogList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserCheckInMile(accessToken: String, lang: String,
        checkInRequest: CheckInRequest, successHandler: (CheckInResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        checkInDataSource.getUserCheckInMile(accessToken, lang, checkInRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun updateDigitalIdCardImage(accessToken: String, lang: String,
        digitalIdCardRequest: DigitalIdCardRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.updateDigitalIdCardImage(accessToken, lang, digitalIdCardRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserDigitalIdCard(accessToken: String, lang: String,
                                              successHandler: (DigitalId) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        digitalIdDataSource.getUserDigitalIdCard(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun saveDigitalId(digitalId: DigitalId) {
        withContext(ioDispatcher){
            coroutineScope { digitalIdLocalDataSource.saveDigitalId(digitalId) }
        }
    }

    override suspend fun loadDigitalId(): Result<DigitalId> {
        return digitalIdLocalDataSource.loadDigitalId()
    }

    override suspend fun clearDigitalId() {
        withContext(ioDispatcher){
            coroutineScope { digitalIdLocalDataSource.clearDigitalId() }
        }
    }

    override suspend fun getUserGachaHistoryList(accessToken: String, lang: String,
                                                 successHandler: (GachaResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        gachaDataSource.getUserGachaHistoryList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun postIdea(accessToken: String, lang: String,
        ideaRequest: IdeaRequest, successHandler: (IdeaPostResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.postIdea(accessToken, lang, ideaRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun likeIdea(accessToken: String, lang: String,
        ideaId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.likeIdea(accessToken, lang, ideaId, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun postComment(accessToken: String, lang: String,
        ideaId: String,
        commentRequest: CommentRequest, successHandler: (Comment) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.postComment(accessToken, lang, ideaId, commentRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun editComment(accessToken: String, lang: String,
        ideaId: String,
        commentId: String,
        commentRequest: CommentRequest, successHandler: (Comment) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.editComment(accessToken, lang, ideaId, commentId, commentRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun deleteComment(accessToken: String, lang: String,
        ideaId: String,
        commentId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.deleteComment(accessToken, lang, ideaId, commentId, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun changeLanguage(accessToken: String, lang: String,
        languageRequest: LanguageRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.changeLanguage(accessToken, lang, languageRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun giveMile(accessToken: String, lang: String,
        giveMileRequest: GiveMileRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.giveMile(accessToken, lang, giveMileRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun acceptMile(accessToken: String, lang: String,
        mileId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.acceptMile(accessToken, lang, mileId, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun rejectMile(accessToken: String, lang: String,
        mileId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.rejectMile(accessToken, lang, mileId, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserMileWaitingList(accessToken: String, lang: String,
                                                successHandler: (List<MileDetail>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        mileDataSource.getUserMileWaitingList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserMileHistoryList(accessToken: String, lang: String,
                                                successHandler: (Mile) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        mileDataSource.getUserMileHistoryList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserMileEarnList(accessToken: String, lang: String,
        year: String,
        month: String, successHandler: (List<MileDetail>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        mileDataSource.getUserMileEarnList(accessToken, lang, year, month, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserMileSpendList(accessToken: String, lang: String,
        year: String,
        month: String, successHandler: (List<MileDetail>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        mileDataSource.getUserMileSpendList(accessToken, lang, year, month, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun readNotification(accessToken: String, lang: String,
        notificationId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.readNotification(accessToken, lang, notificationId, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun readAllNotification(accessToken: String, lang: String,
                                             successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {
        userRemoteDataSource.readAllNotification(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun updateFCMToken(accessToken: String, lang: String,
        fcmTokenRequest: FCMTokenRequest, successHandler: (FCMTokenResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.updateFCMToken(accessToken, lang, fcmTokenRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun clearFCMToken(accessToken: String, lang: String,
                                       successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {
        userRemoteDataSource.clearFCMToken(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserNotificationList(accessToken: String, lang: String,
                                                 successHandler: (List<Notification>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        notificationDataSource.getUserNotificationList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun purchaseProduct(accessToken: String, lang: String,
        purchaseRequest: PurchaseRequest, successHandler: (PurchaseResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.purchaseProduct(accessToken, lang, purchaseRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserOrderHistoryList(accessToken: String, lang: String,
                                                 successHandler: (List<Order>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        orderDataSource.getUserOrderHistoryList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserProvinceList(accessToken: String, lang: String,
                                             successHandler: (List<Province>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        provinceDataSource.getUserProvinceList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun refreshToken(accessToken: String, lang: String,
                                      successHandler: (RefreshTokenResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.refreshToken(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun register(accessToken: String, lang: String,
        staffRegisterRequest: StaffRegisterRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.register(accessToken, lang, staffRegisterRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUserShiftList(accessToken: String, lang: String,
        year: String,
        month: String, successHandler: (List<ShiftData>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        shiftDataSource.getUserShiftList(accessToken, lang, year, month, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun logIn(lang: String,
        loginRequest: LoginRequest, successHandler: (LoginResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.logIn(lang, loginRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun checkIn(accessToken: String, lang: String,
        checkInRequest: CheckInRequest, successHandler: (ConfirmCheckInResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.checkIn(accessToken, lang, checkInRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getUser(accessToken: String, lang: String,
                                 successHandler: (UserInfoResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.getUser(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun updateUserProfile(accessToken: String, lang: String,
        updateRequest: ProfileUpdateRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        userRemoteDataSource.updateUserProfile(accessToken, lang, updateRequest, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun saveUserToken(loginResponse: LoginResponse) {
        withContext(ioDispatcher){
            coroutineScope { userLocalDataSource.saveUserToken(loginResponse) }
        }
    }

    override suspend fun loadUserToken(): Result<String> {
        return userLocalDataSource.loadUserToken()
    }

    override suspend fun clearUser() {
        withContext(ioDispatcher){
            coroutineScope { userLocalDataSource.clearUser() }
        }
    }
}