package com.owndays.stapa.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.owndays.stapa.data.source.remote.model.response.LoginResponse

@Dao
interface UserDao{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdateUser(loginResponse: LoginResponse)

    @Query("SELECT * FROM login_response LIMIT 1")
    fun getUserToken(): LoginResponse

    @Query("DELETE FROM login_response")
    fun deleteUserToken()
}