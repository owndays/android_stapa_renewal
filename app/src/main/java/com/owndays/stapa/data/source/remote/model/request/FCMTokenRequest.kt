package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName

data class FCMTokenRequest(
    @SerializedName("token") val fcmToken: String?
)