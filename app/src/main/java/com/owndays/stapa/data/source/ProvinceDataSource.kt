package com.owndays.stapa.data.source

import com.owndays.stapa.data.model.Province
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface ProvinceDataSource {

    //remote
    suspend fun getUserProvinceList(accessToken: String, lang: String,
                                    successHandler: (List<Province>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}