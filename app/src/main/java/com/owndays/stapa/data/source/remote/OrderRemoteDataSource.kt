package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.model.Order
import com.owndays.stapa.data.model.Product
import com.owndays.stapa.data.source.OrderDataSource
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.ProductListResponse
import com.owndays.stapa.data.source.remote.model.response.TopStoreResponse

class OrderRemoteDataSource(private val owndaysService: OwndaysService, private val gson: Gson) : OrderDataSource, BaseRemoteDataSource(){

    override suspend fun getUserOrderHistoryList(accessToken: String, lang: String,
                                                 successHandler: (List<Order>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserOrderHistoryList(accessToken, lang),
            { _orderList ->
                val orderList = gson.fromJson(gson.toJsonTree(_orderList), Array<Order>::class.java)
                successHandler(orderList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun getProductList(accessToken: String, lang: String,
                                        successHandler: (TopStoreResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getProductList(accessToken, lang),
            { _topStoreResponse ->
                val topStoreResponse = gson.fromJson(gson.toJsonTree(_topStoreResponse), TopStoreResponse::class.java)
                successHandler(topStoreResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun getProductFilteredList(accessToken: String, lang: String,
        refineId: String,
        categoryId: String,
        sortId: String,
        perPage: String,
        page: String, successHandler: (ProductListResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getProductFilteredList(accessToken, lang, refineId, categoryId, sortId, perPage, page),
            { _productListResponse ->
                val productListResponse = gson.fromJson(gson.toJsonTree(_productListResponse), ProductListResponse::class.java)
                successHandler(productListResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun getProductDetail(accessToken: String, lang: String,
        productId: String, successHandler: (Product) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getProductDetail(accessToken, lang, productId),
            { _product ->
                val product = gson.fromJson(gson.toJsonTree(_product), Product::class.java)
                successHandler(product)
            },
            { errorHandler(it) },
            { languageCallBack(it)}
        )
    }
}