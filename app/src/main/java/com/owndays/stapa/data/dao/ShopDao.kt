package com.owndays.stapa.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.owndays.stapa.data.model.Product

@Dao
interface ShopDao{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdateCart(product: Product)

    @Query("SELECT * FROM product LIMIT 1")
    fun getCart(): List<Product>

    @Query("DELETE FROM product")
    fun deleteCart()
}