package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.model.Blood
import com.owndays.stapa.data.source.BloodDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class DefaultBloodRepository (
    private val bloodDataSource: BloodDataSource
) : BloodRepository{

    override suspend fun getBloodList(accessToken: String, lang: String,
        queryLang: String, successHandler: (List<Blood>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        bloodDataSource.getBloodList(accessToken, lang, queryLang, successHandler, errorHandler, languageCallBack)
    }
}