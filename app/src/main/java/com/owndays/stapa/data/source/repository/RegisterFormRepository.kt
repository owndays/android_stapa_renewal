package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.RegisterFormResponse

interface RegisterFormRepository {

    //remote
    suspend fun getRegisterForm(accessToken: String, lang: String,
                                queryLang: String,
                                successHandler: (RegisterFormResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}