package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.model.Province
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.ProvinceDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class ProvinceRemoteDataSource (private val owndaysService: OwndaysService, private val gson: Gson): ProvinceDataSource, BaseRemoteDataSource(){

    override suspend fun getUserProvinceList(accessToken: String, lang: String,
                                             successHandler: (List<Province>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserProvinceList(accessToken, lang),
            { _provinceList ->
                val provinceList = gson.fromJson(gson.toJsonTree(_provinceList), Array<Province>::class.java)
                successHandler(provinceList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }
}