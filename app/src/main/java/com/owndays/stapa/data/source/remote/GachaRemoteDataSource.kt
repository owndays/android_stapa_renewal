package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.source.GachaDataSource
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.GachaResponse

class GachaRemoteDataSource(private val owndaysService: OwndaysService, private val gson: Gson) : GachaDataSource, BaseRemoteDataSource(){

    override suspend fun getUserGachaHistoryList(accessToken: String, lang: String,
                                                 successHandler: (GachaResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserGachaHistoryList(accessToken, lang),
            { _gachaResponse ->
                val gachaResponse = gson.fromJson(gson.toJsonTree(_gachaResponse), GachaResponse::class.java)
                successHandler(gachaResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }
}