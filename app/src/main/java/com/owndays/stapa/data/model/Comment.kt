package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Comment(
    @SerializedName("id") val id: Int?,
    @SerializedName("name") val name: String?,
    @SerializedName("department_name") val departmentName: String?,
    @SerializedName("image_url") val imageUrl: String?,
    @SerializedName("time") var time: String?,
    @SerializedName("text") var text: String?,
    var inEditMode: Boolean = false
)