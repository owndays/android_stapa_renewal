package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName

data class DigitalIdCardRequest(
    @SerializedName("image_base_64") val imageBase64: String?
)