package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.Result
import com.owndays.stapa.data.Result.Error
import com.owndays.stapa.data.model.Comment
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.UserDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.request.*
import com.owndays.stapa.data.source.remote.model.response.*

class UserRemoteDataSource (private val owndaysService: OwndaysService, private val gson: Gson): UserDataSource, BaseRemoteDataSource(){



    override suspend fun addUserAddress(accessToken: String, lang: String,
        staffAddressRequest: StaffAddressRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.addUserAddress(accessToken, lang, staffAddressRequest),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun editUserAddress(accessToken: String, lang: String,
        addressId: String,
        staffAddressRequest: StaffAddressRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.editUserAddress(accessToken, lang, addressId, staffAddressRequest),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun deleteUserAddress(accessToken: String, lang: String,
        addressId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.deleteUserAddress(accessToken, lang, addressId),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun updateDigitalIdCardImage(accessToken: String, lang: String,
        digitalIdCardRequest: DigitalIdCardRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.updateDigitalIdCardImage(accessToken, lang, digitalIdCardRequest),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun postIdea(accessToken: String, lang: String,
        ideaRequest: IdeaRequest, successHandler: (IdeaPostResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.postIdea(accessToken, lang, ideaRequest),
            { _ideaPostResponse ->
                val ideaPostResponse = gson.fromJson(gson.toJsonTree(_ideaPostResponse), IdeaPostResponse::class.java)
                successHandler(ideaPostResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun likeIdea(accessToken: String, lang: String,
        ideaId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.likeIdea(accessToken, lang, ideaId),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun postComment(accessToken: String, lang: String,
        ideaId: String,
        commentRequest: CommentRequest, successHandler: (Comment) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.postComment(accessToken, lang, ideaId, commentRequest),
            { _comment ->
                val comment = gson.fromJson(gson.toJsonTree(_comment), Comment::class.java)
                successHandler(comment)
            },
            { errorHandler(it) },
            { languageCallBack(it)}
        )
    }

    override suspend fun editComment(accessToken: String, lang: String,
        ideaId: String,
        commentId: String,
        commentRequest: CommentRequest, successHandler: (Comment) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.editComment(accessToken, lang, ideaId, commentId, commentRequest),
            { _comment ->
                val comment = gson.fromJson(gson.toJsonTree(_comment), Comment::class.java)
                successHandler(comment)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun deleteComment(accessToken: String, lang: String,
        ideaId: String,
        commentId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.deleteComment(accessToken, lang, ideaId, commentId),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun changeLanguage(accessToken: String, lang: String,
        languageRequest: LanguageRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.changeLanguage(accessToken, lang, languageRequest),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun giveMile(accessToken: String, lang: String,
        giveMileRequest: GiveMileRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.giveMile(accessToken, lang, giveMileRequest),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it)}
        )
    }

    override suspend fun acceptMile(accessToken: String, lang: String,
        mileId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.acceptMile(accessToken, lang, mileId),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun rejectMile(accessToken: String, lang: String,
        mileId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.rejectMile(accessToken, lang, mileId),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it)}
        )
    }

    override suspend fun readNotification(accessToken: String, lang: String, notificationId: String,
                                          successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.readNotification(accessToken, lang, notificationId),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun readAllNotification(accessToken: String, lang: String,
                                             successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {
        makeCall(owndaysService.readAllNotification(accessToken, lang),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun updateFCMToken(accessToken: String, lang: String,
        fcmTokenRequest: FCMTokenRequest, successHandler: (FCMTokenResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.updateFCMToken(accessToken, lang, fcmTokenRequest),
            { _fcmTokenResponse ->
                val fcmTokenResponse = gson.fromJson(gson.toJsonTree(_fcmTokenResponse), FCMTokenResponse::class.java)
                successHandler(fcmTokenResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun clearFCMToken(accessToken: String, lang: String,
                                       successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.clearFCMToken(accessToken, lang),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun purchaseProduct(accessToken: String, lang: String,
        purchaseRequest: PurchaseRequest, successHandler: (PurchaseResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.purchaseProduct(accessToken, lang, purchaseRequest),
            { _purchaseResponse ->
                val purchaseResponse = gson.fromJson(gson.toJsonTree(_purchaseResponse), PurchaseResponse::class.java)
                successHandler(purchaseResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun refreshToken(accessToken: String, lang: String,
                                      successHandler: (RefreshTokenResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.refreshToken(accessToken, lang),
            { _refreshTokenResponse ->
                val refreshTokenResponse = gson.fromJson(gson.toJsonTree(_refreshTokenResponse), RefreshTokenResponse::class.java)
                successHandler(refreshTokenResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun register(accessToken: String, lang: String,
        staffRegisterRequest: StaffRegisterRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.register(accessToken, lang, staffRegisterRequest),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun logIn(lang: String,
        loginRequest: LoginRequest, successHandler: (LoginResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.login(lang, loginRequest),
            { _loginResponse ->
                val loginResponse = gson.fromJson(gson.toJsonTree(_loginResponse), LoginResponse::class.java)
                successHandler(loginResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun checkIn(accessToken: String, lang: String,
        checkInRequest: CheckInRequest, successHandler: (ConfirmCheckInResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.checkIn(accessToken, lang, checkInRequest),
            { _confirmCheckInResponser ->
                val confirmCheckInResponse = gson.fromJson(gson.toJsonTree(_confirmCheckInResponser), ConfirmCheckInResponse::class.java)
                successHandler(confirmCheckInResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it)}
        )
    }

    override suspend fun getUser(accessToken: String, lang: String,
                                 successHandler: (UserInfoResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUser(accessToken, lang),
            { _userInfoResponse ->
                val userInfoResponse = gson.fromJson(gson.toJsonTree(_userInfoResponse), UserInfoResponse::class.java)
                successHandler(userInfoResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun updateUserProfile(accessToken: String, lang: String,
        updateRequest: ProfileUpdateRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.updateUserProfile(accessToken, lang, updateRequest),
            { successHandler(it) },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun saveUserToken(loginResponse: LoginResponse) {
        //Do nothing, let local handle it
    }

    override suspend fun loadUserToken(): Result<String> {
        //Do nothing, let local handle
        return Error(Exception())
    }

    override suspend fun clearUser() {
        //Do nothing, let local handle it
    }
}