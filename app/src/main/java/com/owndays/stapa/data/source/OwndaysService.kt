package com.owndays.stapa.data.source

import com.owndays.stapa.data.source.remote.model.ApiResponse
import com.owndays.stapa.data.source.remote.model.request.*
import retrofit2.Call
import retrofit2.http.*

interface OwndaysService {

    //-------------------------------- ADDRESS ---------------------------------
    @GET("user/address")//List<Address>
    fun getUserAddressList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @POST("user/address")//List<Address>
    fun addUserAddress(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Body addressRequest: StaffAddressRequest): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @PUT("user/address/{address_id}")//List<Address>
    fun editUserAddress(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("address_id") addressId: String,
        @Body addressRequest: StaffAddressRequest): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @DELETE("user/address/{address_id}")//List<Address>
    fun deleteUserAddress(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("address_id") addressId: String): Call<ApiResponse>


    //-------------------------------- BLOOD ---------------------------------
    @GET("bloods")//List<Blood>
    fun getBloodList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Query("lang") langQuery: String): Call<ApiResponse>


    //-------------------------------- CHECK IN --------------------------------
    @GET("checkin/log")//List<CheckInLog>
    fun getUserCheckInLogList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    @POST("checkin/check-mile")//CheckInResponse
    fun getUserCheckInMile(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Body checkInRequest: CheckInRequest): Call<ApiResponse>

    @POST("checkin/stamp")//ConfirmCheckInResponse
    fun checkIn(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Body checkInRequest: CheckInRequest): Call<ApiResponse>


    //-------------------------------- DIGITAL ID -----------------------------
    @GET("digital")//DigitalId
    fun getUserDigitalIdCard(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @PUT("digital")//DigitalId
    fun updateDigitalIdCardImage(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Body digitalIdCardRequest: DigitalIdCardRequest): Call<ApiResponse>


    //-------------------------------- GACHA ----------------------------------
    @GET("gacha/history")//GachaResponse
    fun getUserGachaHistoryList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>


    //-------------------------------- IDEA ----------------------------------
    @GET("ideas")//IdeaListResponse
    fun getIdeaList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Query("page") page: String,
        @Query("per_page") perPage: String,
        @Query("keyword") keyword: String,
        @Query("category_id") categoryId: String): Call<ApiResponse>

    @GET("ideas")//IdeaListResponse
    fun getUserIdeaList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Query("page") page: String,
        @Query("per_page") perPage: String,
        @Query("owner") owner: String,
        @Query("keyword") keyword: String,
        @Query("category_id") categoryId: String): Call<ApiResponse> //owner = 1

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @POST("ideas")//IdeaListResponse
    fun postIdea(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Body ideaRequest: IdeaRequest): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @DELETE("ideas/{idea_id}/comments/{comment_id}")//IdeaListResponse
    fun deleteComment(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("idea_id") ideaId: String,
        @Path("comment_id") commentId: String): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @PUT("ideas/{idea_id}/like")//IdeaListResponse
    fun likeIdea(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("idea_id") ideaId: String): Call<ApiResponse>

    @POST("ideas/{idea_id}/comments")//Comment
    fun postComment(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("idea_id") ideaId: String,
        @Body commentRequest: CommentRequest): Call<ApiResponse>

    @PUT("ideas/{idea_id}/comments/{comment_id}")//Comment
    fun editComment(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("idea_id") ideaId: String,
        @Path("comment_id") commentId: String,
        @Body commentRequest: CommentRequest): Call<ApiResponse>

    @GET("ideas/categories")//List<Category>
    fun getIdeaCategoryList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>


    //-------------------------------- LANGUAGE ------------------------------
    @GET("setting/language")//List<Language>
    fun getLanguageList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @PUT("setting/language")//List<Language>
    fun changeLanguage(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Body languageRequest: LanguageRequest): Call<ApiResponse>


    //-------------------------------- LOGIN --------------------------------
    @POST("login")//LoginResponse
    fun login(
        @Header("Accept-Language") lang: String,
        @Body loginRequest: LoginRequest): Call<ApiResponse>


    //-------------------------------- MILE ----------------------------------
    @GET("mile")//Mile
    fun getUserMileHistoryList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    @POST("mile/give")//Mile
    fun giveMile(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Body giveMileRequest: GiveMileRequest): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @PUT("mile/waiting/accept/{mile_id}")//Mile
    fun acceptMile(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("mile_id") mileId: String): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @PUT("mile/waiting/reject/{mile_id}")//Mile
    fun rejectMile(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("mile_id") mileId: String): Call<ApiResponse>

    @GET("mile/waiting")//List<MileDetail>
    fun getUserMileWaitingList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    @GET("mile/detail/earn")//List<MileDetail>
    fun getUserMileEarnList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Query("year") year: String,
        @Query("month") month: String): Call<ApiResponse>

    @GET("mile/detail/spend")//List<MileDetail>
    fun getUserMileSpendList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Query("year") year: String,
        @Query("month") month: String): Call<ApiResponse>


    //-------------------------------- NOTIFICATION -----------------------------
    @GET("notifications")//List<Notification>
    fun getUserNotificationList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    @GET("notifications/{notification_id}")//NotificationDetail
    fun getNotificationDetail(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("notification_id") notificationId: String): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @PUT("notifications/read/{notification_id}")
    fun readNotification(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("notification_id") notificationId: String): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @PUT("notifications/read")
    fun readAllNotification(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    @POST("notifications/fcm")//FCMTokenResponse
    fun updateFCMToken(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Body fcmTokenRequest: FCMTokenRequest): Call<ApiResponse>

    @POST("notifications/fcm/1")//FCMTokenResponse
    fun clearFCMToken(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>


    //-------------------------------- ORDER ----------------------------------
    @GET("orders")//List<Order>
    fun getUserOrderHistoryList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    @PUT("orders")//PurchaseResponse
    fun purchaseProduct(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Body purchaseRequest: PurchaseRequest): Call<ApiResponse>

    @GET("store")//TopStoreResponse
    fun getProductList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    @GET("products")//ProductListResponse
    fun getProductFilteredList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Query("refine_id") refineId: String,
        @Query("category_id") categoryId: String,
        @Query("sort_id") sortId: String,
        @Query("per_page") perPage: String,
        @Query("page") page: String): Call<ApiResponse>

    @GET("products/{product_id}")//Product
    fun getProductDetail(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("product_id") productId: String): Call<ApiResponse>


    //-------------------------------- PROVINCE ---------------------------------
    @GET("user/address/provinces")//List<Province>
    fun getUserProvinceList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>


    //-------------------------------- REGISTER -------------------------------
    @GET("register")//RegisterFormResponse
    fun getRegisterForm(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Query("lang") langQuery: String): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @POST("register")//RegisterFormResponse
    fun register(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Body staffRegisterRequest: StaffRegisterRequest): Call<ApiResponse>


    //-------------------------------- SHIFT ----------------------------------
    @GET("user/shift")//List<ShiftData>
    fun getUserShiftList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Query("year") year: String,
        @Query("month") month: String): Call<ApiResponse>


    //-------------------------------- SHOP ----------------------------------
    @GET("shops")//List<Shop>
    fun getShopList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    @GET("shops/{shop_code}")//ShopDetail
    fun getShopDetail(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Path("shop_code") shopCode: String): Call<ApiResponse>


    //-------------------------------- USER ----------------------------------
    @GET("user")//UserInfoResponse
    fun getUser(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

    //Return could be any, but we ignore return obj from server, only status and message that we want
    @PUT("user")//UserInfoResponse
    fun updateUserProfile(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String,
        @Body updateRequest: ProfileUpdateRequest): Call<ApiResponse>

    @GET("staffs")//List<User>
    fun getStaffList(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>


    //-------------------------------- REFRESH TOKEN ------------------------------
    @POST("refresh-token")//RefreshTokenResponse
    fun refreshToken(
        @Header("Authorization") token: String,
        @Header("Accept-Language") lang: String): Call<ApiResponse>

}