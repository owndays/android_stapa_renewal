package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class MileHistory (
    @SerializedName("month") val month: String?,
    @SerializedName("system") val system: Int?,
    @SerializedName("user") val user: Int?,
    @SerializedName("content") val content: Int?,
    @SerializedName("total") val total: Int?,
    @SerializedName("percent_system") val percentSystem: Int?,
    @SerializedName("percent_user") val percentUser: Int?,
    @SerializedName("percent_content") val percentContent: Int?,
    @SerializedName("percent_summary") val percentSummary: Int?
)