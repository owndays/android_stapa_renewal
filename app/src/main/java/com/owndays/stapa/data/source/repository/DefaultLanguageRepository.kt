package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.model.Language
import com.owndays.stapa.data.source.LanguageDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class DefaultLanguageRepository(
    private val languageDataSource: LanguageDataSource
): LanguageRepository {

    override suspend fun getLanguageList(accessToken: String, lang: String,
                                         successHandler: (List<Language>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

       languageDataSource.getLanguageList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }
}