package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName

data class CommentRequest(
    @SerializedName("text") val commentText: String?
)