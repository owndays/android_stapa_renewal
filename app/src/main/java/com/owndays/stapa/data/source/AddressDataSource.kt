package com.owndays.stapa.data.source

import com.owndays.stapa.data.model.Address
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface AddressDataSource {

    //remote
    suspend fun getUserAddressList(accessToken: String, lang: String,
                                   successHandler: (List<Address>) -> Unit,
                                   errorHandler: (ErrorResponse?) -> Unit,
                                   languageCallBack: (String) -> Unit)
}