package com.owndays.stapa.data.model

import androidx.room.ColumnInfo
import com.google.gson.annotations.SerializedName

data class Language(
    @ColumnInfo(name = "language_id") @SerializedName("id") val id: Int?,
    @ColumnInfo(name = "language_code") @SerializedName("code") val code: String?,
    @ColumnInfo(name = "language_name") @SerializedName("name") val name: String?
)