package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.model.Product
import com.owndays.stapa.data.source.OrderDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.ProductListResponse
import com.owndays.stapa.data.source.remote.model.response.TopStoreResponse

class DefaultOrderRepository(
    private val orderDataSource: OrderDataSource
) : OrderRepository {

    override suspend fun getProductList(accessToken: String, lang: String,
                                        successHandler: (TopStoreResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        orderDataSource.getProductList(accessToken, lang, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getProductFilteredList(accessToken: String, lang: String,
        refineId: String,
        categoryId: String,
        sortId: String,
        perPage: String,
        page: String, successHandler: (ProductListResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        orderDataSource.getProductFilteredList(accessToken, lang, refineId, categoryId, sortId, perPage, page, successHandler, errorHandler, languageCallBack)
    }

    override suspend fun getProductDetail(accessToken: String, lang: String,
        productId: String, successHandler: (Product) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        orderDataSource.getProductDetail(accessToken, lang, productId, successHandler, errorHandler, languageCallBack)
    }
}