package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.model.CheckInLog
import com.owndays.stapa.data.source.CheckInDataSource
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.request.CheckInRequest
import com.owndays.stapa.data.source.remote.model.response.CheckInResponse

class CheckInRemoteDataSource(private val owndaysService: OwndaysService, private val gson: Gson) : CheckInDataSource, BaseRemoteDataSource(){

    override suspend fun getUserCheckInLogList(accessToken: String, lang: String,
                                               successHandler: (List<CheckInLog>) -> Unit,
                                               errorHandler: (ErrorResponse?) -> Unit,
                                               languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserCheckInLogList(accessToken, lang),
            { _checkInLogList ->
                val checkInLogList = gson.fromJson(gson.toJsonTree(_checkInLogList), Array<CheckInLog>::class.java)
                successHandler(checkInLogList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun getUserCheckInMile(accessToken: String, lang: String,
        checkInRequest: CheckInRequest, successHandler: (CheckInResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserCheckInMile(accessToken, lang, checkInRequest),
            { _checkInResponse ->
                val checkInResponse = gson.fromJson(gson.toJsonTree(_checkInResponse), CheckInResponse::class.java)
                successHandler(checkInResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

}