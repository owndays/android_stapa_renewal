package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Slider(
    @SerializedName("id") val id: Int?,
    @SerializedName("image_url") val imageUrl: String?
)