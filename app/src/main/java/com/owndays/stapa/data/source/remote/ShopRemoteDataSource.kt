package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.Result
import com.owndays.stapa.data.Result.Error
import com.owndays.stapa.data.model.Product
import com.owndays.stapa.data.model.Shop
import com.owndays.stapa.data.model.ShopDetail
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.ShopDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class ShopRemoteDataSource (private val owndaysService: OwndaysService, private val gson: Gson): ShopDataSource, BaseRemoteDataSource(){

    override suspend fun getShopList(accessToken: String, lang: String,
                                     successHandler: (List<Shop>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getShopList(accessToken, lang),
            { _shopList ->
                val shopList = gson.fromJson(gson.toJsonTree(_shopList), Array<Shop>::class.java)
                successHandler(shopList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun getShopDetail(accessToken: String, lang: String,
        shopCode: String, successHandler: (ShopDetail) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getShopDetail(accessToken, lang, shopCode),
            { _shopDetail ->
                val shopDetail = gson.fromJson(gson.toJsonTree(_shopDetail), ShopDetail::class.java)
                successHandler(shopDetail)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun insertCart(productList: List<Product>) {
        //Do nothing, let local handle
    }

    override suspend fun getShopCart(): Result<List<Product>> {
        //Do nothing, let local handle
        return Error(Exception())
    }

    override suspend fun clearCart() {
        //Do nothing, let local handle
    }
}