package com.owndays.stapa.data.source

import com.owndays.stapa.data.model.Mile
import com.owndays.stapa.data.model.MileDetail
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface MileDataSource {

    //remote
    suspend fun getUserMileWaitingList(accessToken: String, lang: String,
                                       successHandler: (List<MileDetail>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun getUserMileHistoryList(accessToken: String, lang: String,
                                       successHandler: (Mile) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun getUserMileEarnList(accessToken: String, lang: String,
                                year: String,
                                month: String,
                                successHandler: (List<MileDetail>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun getUserMileSpendList(accessToken: String, lang: String,
                                 year: String,
                                 month: String,
                                 successHandler: (List<MileDetail>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}