package com.owndays.stapa.data.source.remote.model.response

import com.google.gson.annotations.SerializedName
import com.owndays.stapa.data.model.Blood
import com.owndays.stapa.data.model.Language
import com.owndays.stapa.data.model.Province

data class RegisterFormResponse(
    @SerializedName("profile_name") val id: String?,
    @SerializedName("department_name") val departmentName: String?,
    @SerializedName("digital_id_name") val digitalIdName: String?,
    @SerializedName("bloods") val bloods: List<Blood>?,
    @SerializedName("languages") val languages: List<Language>?,
    @SerializedName("provinces") val provinces: List<Province>?
)