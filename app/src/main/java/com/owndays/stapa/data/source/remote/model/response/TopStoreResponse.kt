package com.owndays.stapa.data.source.remote.model.response

import com.google.gson.annotations.SerializedName
import com.owndays.stapa.data.model.Product
import com.owndays.stapa.data.model.Slider

data class TopStoreResponse(
    @SerializedName("sliders") val sliders: List<Slider>?,
    @SerializedName("new_arrivals") val newArrivals: List<Product>?,
    @SerializedName("pickups") val pickups: List<Product>?
)