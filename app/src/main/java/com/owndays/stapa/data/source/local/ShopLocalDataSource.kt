package com.owndays.stapa.data.source.local

import com.owndays.stapa.data.Result
import com.owndays.stapa.data.Result.Error
import com.owndays.stapa.data.Result.Success
import com.owndays.stapa.data.dao.ShopDao
import com.owndays.stapa.data.model.Product
import com.owndays.stapa.data.model.Shop
import com.owndays.stapa.data.model.ShopDetail
import com.owndays.stapa.data.source.ShopDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ShopLocalDataSource(
    private val shopDao : ShopDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ShopDataSource {

    override suspend fun getShopList(accessToken: String, lang: String,
                                     successHandler: (List<Shop>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {
        //Do nothing, let remote handle it
    }

    override suspend fun getShopDetail(accessToken: String, lang: String, shopCode: String,
                                       successHandler: (ShopDetail) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {
        //Do nothing, let remote handle it
    }

    override suspend fun insertCart(productList: List<Product>) {
        productList.forEach { _product ->
            shopDao.insertOrUpdateCart(_product)
        }
    }


    override suspend fun getShopCart(): Result<List<Product>> {
        return withContext(ioDispatcher) {
            try {
                Success(shopDao.getCart())
            } catch (e: Exception) {
                Error(e)
            }
        }
    }

    override suspend fun clearCart() {
        shopDao.deleteCart()
    }
}