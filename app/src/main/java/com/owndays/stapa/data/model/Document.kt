package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Document(
    @SerializedName("id") val id: Int?,
    @SerializedName("url") val url: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("extension") val extension: String?,
    @SerializedName("base64") val base64: String?
)