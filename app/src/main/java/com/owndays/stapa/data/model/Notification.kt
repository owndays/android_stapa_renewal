package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Notification(
    @SerializedName("id") val id: Int?,
    @SerializedName("date") var date: String?,
    @SerializedName("read_flag") var readFlag: Int?,
    @SerializedName("type") val type: Int?,
    @SerializedName("link") val link: String?,
    @SerializedName("title") val title: String?,
    @SerializedName("order_code") val orderCode: String?,
    @SerializedName("body") val body: String?
)

/*
* Type
* 1 : send point by admin (plane)
* 2 : send point by user (heart) >> WAITING MILE
*
* 3 : send notification by admin (info) >> NotificationDetail API >> NotificationDetail response
* 4 : information (info) >> NotificationDetail API  >> NotificationDetail HTML
* 5 : new release (star) >> NotificationDetail API >> NotificationDetail HTML
*
* 6 : cancel order (info) >> ORDER HISTORY
* 7 : complete order (info) >> ORDER HISTORY
* */