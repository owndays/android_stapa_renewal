package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName

data class LanguageRequest(
    @SerializedName("language_id") val languageId: Int?
)