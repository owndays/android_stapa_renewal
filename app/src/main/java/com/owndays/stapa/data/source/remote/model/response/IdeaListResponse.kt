package com.owndays.stapa.data.source.remote.model.response

import com.google.gson.annotations.SerializedName
import com.owndays.stapa.data.model.Idea
import com.owndays.stapa.data.model.Paginate

data class IdeaListResponse(
    @SerializedName("paginate") val paginate: Paginate,
    @SerializedName("ideas") val ideas: List<Idea>?
)