package com.owndays.stapa.data.source.repository

import com.owndays.stapa.data.model.User
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface StaffRepository {

    //remote
    suspend fun getStaffList(accessToken: String, lang: String, successHandler: (List<User>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)
}