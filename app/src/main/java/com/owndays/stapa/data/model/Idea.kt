package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class Idea(
    @SerializedName("id") val id: Int?,
    @SerializedName("staff") val staff: User?,
    @SerializedName("created_date") val createdDate: String?,
    @SerializedName("category_name") val categoryName: String?,
    @SerializedName("rank") val rank: String?,
    @SerializedName("mile") val mile: Int?,
    @SerializedName("is_approve") val isApprove: Boolean?,
    @SerializedName("title") val title: String?,
    @SerializedName("how") val how: String?,
    @SerializedName("why") val why: String?,
    @SerializedName("what") val what: String?,
    @SerializedName("documents") val documents: List<Document>?,
    @SerializedName("comments") var comments: List<Comment>?,
    @SerializedName("is_like") var isLike: Boolean?,
    @SerializedName("likes") var like: List<Like>?,
    var isExpanded: Boolean = false
)
/*
* normal
* bronze
* silver
* gold
* */
