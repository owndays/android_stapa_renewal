package com.owndays.stapa.data.source

import com.owndays.stapa.data.model.CheckInLog
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.request.CheckInRequest
import com.owndays.stapa.data.source.remote.model.response.CheckInResponse

interface CheckInDataSource {

    //remote
    suspend fun getUserCheckInLogList(accessToken: String, lang: String,
                                      successHandler: (List<CheckInLog>) -> Unit,
                                      errorHandler: (ErrorResponse?) -> Unit,
                                      languageCallBack: (String) -> Unit)

    //remote
    suspend fun getUserCheckInMile(accessToken: String, lang: String,
                                   checkInRequest: CheckInRequest,
                                   successHandler: (CheckInResponse) -> Unit,
                                   errorHandler: (ErrorResponse?) -> Unit,
                                   languageCallBack: (String) -> Unit)
}