package com.owndays.stapa.data.model

import com.google.gson.annotations.SerializedName

data class ShopDetail(
    @SerializedName("name") val name: String?,
    @SerializedName("address") val address: String?,
    @SerializedName("phone") val phone: String?,
    @SerializedName("holiday") val holiday: String?,
    @SerializedName("open_time") val openTime: String?,
    @SerializedName("remarks") val remarks: String?,
    @SerializedName("image_urls") val imageUrls: String?,
    @SerializedName("already_check_in") val alreadyCheckIn: Boolean?,
    @SerializedName("staff_check_ins") val staffCheckIns: List<StaffCheckIn>?
)