package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.model.User
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.StaffDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class StaffRemoteDataSource(private val owndaysService: OwndaysService, private val gson: Gson) : StaffDataSource, BaseRemoteDataSource(){

    override suspend fun getStaffList(accessToken: String, lang: String,
                                      successHandler: (List<User>) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getStaffList(accessToken, lang),
            { _userList ->
                val userList = gson.fromJson(gson.toJsonTree(_userList), Array<User>::class.java)
                successHandler(userList.toList())
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }
}