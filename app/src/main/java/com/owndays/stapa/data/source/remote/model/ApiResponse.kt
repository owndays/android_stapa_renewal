package com.owndays.stapa.data.source.remote.model

import com.google.gson.annotations.SerializedName

data class ApiResponse(
    @SerializedName("response_status") val responseStatus: ApiStatus,
    @SerializedName("response_data") val responseData: Any?
)