package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName

data class LoginRequest(
    @SerializedName("staff_id") val staffId: Int?,
    @SerializedName("password") val password: Int?
)