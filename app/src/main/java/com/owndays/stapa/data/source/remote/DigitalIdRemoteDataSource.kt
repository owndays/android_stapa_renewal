package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.Result
import com.owndays.stapa.data.Result.Error
import com.owndays.stapa.data.model.DigitalId
import com.owndays.stapa.data.source.DigitalIdDataSource
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.remote.model.ErrorResponse

class DigitalIdRemoteDataSource(private val owndaysService: OwndaysService, private val gson: Gson) : DigitalIdDataSource, BaseRemoteDataSource(){

    override suspend fun getUserDigitalIdCard(accessToken: String, lang: String,
                                              successHandler: (DigitalId) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getUserDigitalIdCard(accessToken, lang),
            { _digitalId ->
                val digitalId = gson.fromJson(gson.toJsonTree(_digitalId), DigitalId::class.java)
                successHandler(digitalId)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }

    override suspend fun saveDigitalId(digitalId: DigitalId) {
        //Do nothing, let local handle it
    }

    override suspend fun loadDigitalId(): Result<DigitalId> {
        return Error(Exception())
    }

    override suspend fun clearDigitalId() {
        //Do nothing, let local handle it
    }

}