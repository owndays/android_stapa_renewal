package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.source.remote.model.ApiResponse
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.ErrorResponse.ResponseStatus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber


open class BaseRemoteDataSource {

    protected fun makeCall(requestCall: Call<ApiResponse>, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit): Call<ApiResponse> {
        requestCall.enqueue(object : Callback<ApiResponse> {
            override fun onResponse(call: Call<ApiResponse>?, response: Response<ApiResponse>?) {
                response?.let { callResponse ->
                    when (callResponse.isSuccessful) {
                        true -> {
                            if (callResponse.body() != null) {
                                callResponse.body().let { apiResponse ->
                                    when (apiResponse?.responseStatus?.status) {
                                        true -> {
                                            if (apiResponse.responseData != null) {
                                                successHandler(apiResponse.responseData)
                                            } else {
                                                errorHandler(ErrorResponse(ResponseStatus(true, null, "",null,null)))
                                            }
                                        }
                                        false -> {
                                            errorHandler(
                                                ErrorResponse(
                                                    ResponseStatus(
                                                    false,
                                                    apiResponse.responseStatus.errorCode,
                                                    apiResponse.responseStatus.message,
                                                    null,null
                                                    )
                                                )
                                            )
                                        }
                                        else -> {
                                            errorHandler(
                                                ErrorResponse(
                                                    ResponseStatus(
                                                        false,
                                                        apiResponse?.responseStatus?.errorCode,
                                                        apiResponse?.responseStatus?.message,
                                                        null,null
                                                    )
                                                )
                                            )
                                        }
                                    }
                                    if(!apiResponse?.responseStatus?.languageCode.isNullOrEmpty())
                                        languageCallBack(apiResponse?.responseStatus?.languageCode?:"")
                                }
                            } else {
                                errorHandler(ErrorResponse(ResponseStatus(false, null, "body is null",null,null)))
                            }
                        }
                        false -> {
                            Timber.d("notSuccess-errorResponse $callResponse")
//                            val gson = Gson()
//                            val errorResponse : ErrorResponse? = gson.fromJson(callResponse.errorBody()?.charStream(), ErrorResponse::class.java)
                            val errorResponse = ErrorResponse(ResponseStatus(false, null, callResponse.message(),null,callResponse.code().toString()))
                            errorHandler(errorResponse)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ApiResponse>?, t: Throwable?) {
                t?.let {
                    if (t is Exception) {
                        if (call != null && !call.isCanceled) {
                            Timber.d("makeRequest-http failed ${t.message}")
                            errorHandler(ErrorResponse(ResponseStatus(false, null, "on failure",null,null)))
                        }
                    }
                }
            }
        })
        return requestCall
    }
}