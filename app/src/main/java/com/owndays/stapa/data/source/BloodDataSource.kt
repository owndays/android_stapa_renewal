package com.owndays.stapa.data.source

import com.owndays.stapa.data.model.Blood
import com.owndays.stapa.data.source.remote.model.ErrorResponse

interface BloodDataSource {

    //remote
    suspend fun getBloodList(accessToken: String, lang: String,
                             queryLang: String,
                             successHandler: (List<Blood>) -> Unit,
                             errorHandler: (ErrorResponse?) -> Unit,
                             languageCallBack: (String) -> Unit)
}