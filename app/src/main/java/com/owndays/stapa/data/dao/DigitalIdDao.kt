package com.owndays.stapa.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.owndays.stapa.data.model.DigitalId

@Dao
interface DigitalIdDao{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdateDigitalId(digitalId: DigitalId)

    @Query("SELECT * FROM digitalId LIMIT 1")
    fun getDigitalId(): DigitalId

    @Query("DELETE FROM digitalId")
    fun deleteDigitalId()
}