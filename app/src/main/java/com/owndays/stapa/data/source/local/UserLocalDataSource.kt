package com.owndays.stapa.data.source.local

import com.owndays.stapa.data.Result
import com.owndays.stapa.data.Result.Error
import com.owndays.stapa.data.Result.Success
import com.owndays.stapa.data.dao.UserDao
import com.owndays.stapa.data.model.Comment
import com.owndays.stapa.data.source.UserDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.request.*
import com.owndays.stapa.data.source.remote.model.response.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserLocalDataSource(
    private val userDao : UserDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : UserDataSource {

    override suspend fun addUserAddress(accessToken: String, lang: String,
                                        staffAddressRequest: StaffAddressRequest,
                                        successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun editUserAddress(accessToken: String, lang: String,
                                         addressId: String,
                                         staffAddressRequest: StaffAddressRequest,
                                         successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun deleteUserAddress(accessToken: String, lang: String,
                                           addressId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun updateDigitalIdCardImage(accessToken: String, lang: String,
                                                  digitalIdCardRequest: DigitalIdCardRequest,
                                                  successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun postIdea(accessToken: String, lang: String,
                                  ideaRequest: IdeaRequest,
                                  successHandler: (IdeaPostResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun likeIdea(accessToken: String, lang: String,
                                  ideaId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun postComment(accessToken: String, lang: String,
                                     ideaId: String,
                                     commentRequest: CommentRequest, successHandler: (Comment) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun editComment(accessToken: String, lang: String,
                                     ideaId: String,
                                     commentId: String,
                                     commentRequest: CommentRequest, successHandler: (Comment) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun deleteComment(accessToken: String, lang: String,
                                       ideaId: String,
                                       commentId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun changeLanguage(accessToken: String, lang: String,
                                        languageRequest: LanguageRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun giveMile(accessToken: String, lang: String,
                                  giveMileRequest: GiveMileRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun acceptMile(accessToken: String, lang: String,
                                    mileId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun rejectMile(accessToken: String, lang: String,
                                    mileId: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun readNotification(accessToken: String, lang: String, notificationId: String,
                                          successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun readAllNotification(accessToken: String, lang: String,
                                             successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {
        //Do nothing, let remote handle it
    }

    override suspend fun updateFCMToken(accessToken: String, lang: String,
                                        fcmTokenRequest: FCMTokenRequest,
                                        successHandler: (FCMTokenResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun clearFCMToken(accessToken: String, lang: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun purchaseProduct(accessToken: String, lang: String,
                                         purchaseRequest: PurchaseRequest,
                                         successHandler: (PurchaseResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun refreshToken(accessToken: String, lang: String,
                                      successHandler: (RefreshTokenResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun register(accessToken: String, lang: String,
                                  staffRegisterRequest: StaffRegisterRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun logIn(lang: String,
                               loginRequest: LoginRequest, successHandler: (LoginResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun checkIn(accessToken: String, lang: String,
                                 checkInRequest: CheckInRequest,
                                 successHandler: (ConfirmCheckInResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun getUser(accessToken: String, lang: String,
                                 successHandler: (UserInfoResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun updateUserProfile(accessToken: String, lang: String,
                                           updateRequest: ProfileUpdateRequest, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallback: (String) -> Unit) {

        //Do nothing, let remote handle it
    }

    override suspend fun saveUserToken(loginResponse: LoginResponse) {
        userDao.insertOrUpdateUser(loginResponse)
    }

    override suspend fun loadUserToken(): Result<String> {
        return withContext(ioDispatcher) {
            try {
                Success(userDao.getUserToken().token?:"")
            } catch (e: Exception) {
                Error(e)
            }
        }
    }

    override suspend fun clearUser() {
        userDao.deleteUserToken()
    }
}