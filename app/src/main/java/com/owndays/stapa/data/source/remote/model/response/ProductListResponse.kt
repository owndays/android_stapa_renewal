package com.owndays.stapa.data.source.remote.model.response

import com.google.gson.annotations.SerializedName
import com.owndays.stapa.data.model.Category
import com.owndays.stapa.data.model.Product
import com.owndays.stapa.data.model.Refine
import com.owndays.stapa.data.model.Sort

data class ProductListResponse(
    @SerializedName("categories") val categories: List<Category>?,
    @SerializedName("refines") val refines: List<Refine>?,
    @SerializedName("sorts") val sorts: List<Sort>?,
    @SerializedName("products") val products: List<Product>?
)