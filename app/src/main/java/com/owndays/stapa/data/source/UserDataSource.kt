package com.owndays.stapa.data.source

import com.owndays.stapa.data.Result
import com.owndays.stapa.data.model.Comment
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.request.*
import com.owndays.stapa.data.source.remote.model.response.*

interface UserDataSource {

    //------------------------------------------------Address----------------------------------------------------//
    //remote
    suspend fun addUserAddress(accessToken: String, lang: String,
                               staffAddressRequest: StaffAddressRequest,
                               successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun editUserAddress(accessToken: String, lang: String,
                                addressId: String,
                                staffAddressRequest: StaffAddressRequest,
                                successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun deleteUserAddress(accessToken: String, lang: String,
                                  addressId: String,
                                  successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //------------------------------------------------DigitalId----------------------------------------------------//
    //remote
    suspend fun updateDigitalIdCardImage(accessToken: String, lang: String,
                                         digitalIdCardRequest: DigitalIdCardRequest,
                                         successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //------------------------------------------------Idea----------------------------------------------------//
    //remote
    suspend fun postIdea(accessToken: String, lang: String,
                         ideaRequest: IdeaRequest,
                         successHandler: (IdeaPostResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun likeIdea(accessToken: String, lang: String,
                         ideaId: String,
                         successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun postComment(accessToken: String, lang: String,
                            ideaId: String,
                            commentRequest: CommentRequest,
                            successHandler: (Comment) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun editComment(accessToken: String, lang: String,
                            ideaId: String,
                            commentId: String,
                            commentRequest: CommentRequest,
                            successHandler: (Comment) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun deleteComment(accessToken: String, lang: String,
                              ideaId: String,
                              commentId: String,
                              successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //------------------------------------------------Language----------------------------------------------------//
    //remote
    suspend fun changeLanguage(accessToken: String, lang: String,
                               languageRequest: LanguageRequest,
                               successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //------------------------------------------------Mile----------------------------------------------------//
    //remote
    suspend fun giveMile(accessToken: String, lang: String,
                         giveMileRequest: GiveMileRequest,
                         successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun acceptMile(accessToken: String, lang: String,
                           mileId: String,
                           successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun rejectMile(accessToken: String, lang: String,
                           mileId: String,
                           successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //------------------------------------------------Notification----------------------------------------------------//
    //remote
    suspend fun readNotification(accessToken: String, lang: String, notificationId: String,
                                 successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun readAllNotification(accessToken: String, lang: String, successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun updateFCMToken(accessToken: String, lang: String,
                               fcmTokenRequest: FCMTokenRequest,
                               successHandler: (FCMTokenResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun clearFCMToken(accessToken: String, lang: String,
                               successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //------------------------------------------------Product----------------------------------------------------//
    //remote
    suspend fun purchaseProduct(accessToken: String, lang: String,
                                purchaseRequest: PurchaseRequest,
                                successHandler: (PurchaseResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //------------------------------------------------Refresh Token----------------------------------------------------//
    //remote
    suspend fun refreshToken(accessToken: String, lang: String,
                             successHandler: (RefreshTokenResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //------------------------------------------------Register----------------------------------------------------//
    //remote
    suspend fun register(accessToken: String, lang: String,
                         staffRegisterRequest: StaffRegisterRequest,
                         successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //------------------------------------------------User----------------------------------------------------//
    //remote
    suspend fun logIn(lang: String,
                      loginRequest: LoginRequest,
                      successHandler: (LoginResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun checkIn(accessToken: String, lang: String,
                        checkInRequest: CheckInRequest,
                        successHandler: (ConfirmCheckInResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun getUser(accessToken: String, lang: String,
                        successHandler: (UserInfoResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //remote
    suspend fun updateUserProfile(accessToken: String, lang: String,
                                  updateRequest: ProfileUpdateRequest,
                                  successHandler: (Any?) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit)

    //local
    suspend fun saveUserToken (loginResponse: LoginResponse)

    //local
    suspend fun loadUserToken(): Result<String>

    //local
    suspend fun clearUser()
}