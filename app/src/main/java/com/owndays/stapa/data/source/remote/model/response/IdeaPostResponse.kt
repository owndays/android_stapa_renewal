package com.owndays.stapa.data.source.remote.model.response

import com.google.gson.annotations.SerializedName

data class IdeaPostResponse(
    @SerializedName("mile") val mile: Int?
)