package com.owndays.stapa.data.source.remote.model.request

import com.google.gson.annotations.SerializedName

data class ProductCartRequest(
    @SerializedName("product_id") val productId: Int?,
    @SerializedName("qty") val qty: Int?
)