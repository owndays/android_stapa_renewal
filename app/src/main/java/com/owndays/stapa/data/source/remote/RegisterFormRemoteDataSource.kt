package com.owndays.stapa.data.source.remote

import com.google.gson.Gson
import com.owndays.stapa.data.source.OwndaysService
import com.owndays.stapa.data.source.RegisterFormDataSource
import com.owndays.stapa.data.source.remote.model.ErrorResponse
import com.owndays.stapa.data.source.remote.model.response.RegisterFormResponse

class RegisterFormRemoteDataSource(private val owndaysService: OwndaysService, private val gson: Gson) : RegisterFormDataSource, BaseRemoteDataSource(){

    override suspend fun getRegisterForm(accessToken: String, lang: String,
        queryLang: String, successHandler: (RegisterFormResponse) -> Unit, errorHandler: (ErrorResponse?) -> Unit, languageCallBack: (String) -> Unit) {

        makeCall(owndaysService.getRegisterForm(accessToken, lang, queryLang),
            { _registerFormResponse ->
                val registerFormResponse = gson.fromJson(gson.toJsonTree(_registerFormResponse), RegisterFormResponse::class.java)
                successHandler(registerFormResponse)
            },
            { errorHandler(it) },
            { languageCallBack(it) }
        )
    }
}