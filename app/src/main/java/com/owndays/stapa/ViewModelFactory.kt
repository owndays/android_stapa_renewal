package com.owndays.stapa

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.owndays.stapa.data.source.repository.*
import com.owndays.stapa.dialog.viewmodel.BloodViewModel
import com.owndays.stapa.dialog.viewmodel.ProvinceViewModel
import com.owndays.stapa.main.adapters.viewmodel.CategoryViewModel
import com.owndays.stapa.main.adapters.viewmodel.DurationViewModel
import com.owndays.stapa.main.checkin.viewmodel.CheckInViewModel
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.main.mile.viewmodel.MileViewModel
import com.owndays.stapa.main.setting.viewmodel.AddressViewModel
import com.owndays.stapa.main.store.viewmodel.StoreViewModel
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.register.viewmodel.RegisterViewModel
import com.owndays.stapa.welcome.viewmodel.WelcomeViewModel

@Suppress("UNCHECKED_CAST")
class ViewModelFactory constructor(
    private val bloodRepository: BloodRepository,
    private val ideaRepository: IdeaRepository,
    private val languageRepository: LanguageRepository,
    private val notificationRepository: NotificationRepository,
    private val orderRepository: OrderRepository,
    private val registerFormRepository: RegisterFormRepository,
    private val shopRepository: ShopRepository,
    private val staffRepository: StaffRepository,
    private val userRepository: UserRepository,
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        return with(modelClass) {
            when {
                isAssignableFrom(WelcomeViewModel::class.java) -> WelcomeViewModel(userRepository)
                isAssignableFrom(RegisterViewModel::class.java) -> RegisterViewModel(registerFormRepository, userRepository)
                isAssignableFrom(MainViewModel::class.java) -> MainViewModel(userRepository, languageRepository, bloodRepository, notificationRepository)
                isAssignableFrom(MileViewModel::class.java) -> MileViewModel(userRepository, staffRepository)
                isAssignableFrom(IdeaViewModel::class.java) -> IdeaViewModel(ideaRepository, userRepository)
                isAssignableFrom(CheckInViewModel::class.java) -> CheckInViewModel(shopRepository, userRepository)
                isAssignableFrom(DurationViewModel::class.java) -> DurationViewModel()
                isAssignableFrom(BloodViewModel::class.java) -> BloodViewModel()
                isAssignableFrom(AddressViewModel::class.java) -> AddressViewModel(userRepository)
                isAssignableFrom(ProvinceViewModel::class.java) -> ProvinceViewModel()
                isAssignableFrom(StoreViewModel::class.java) -> StoreViewModel(orderRepository, shopRepository)
                isAssignableFrom(CategoryViewModel::class.java) -> CategoryViewModel()
                else -> throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            }
        } as T
    }
}