package com.owndays.stapa.register.view

import android.content.Context
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.owndays.stapa.BaseActivity
import com.owndays.stapa.R
import com.owndays.stapa.databinding.RegisterActivityBinding
import com.owndays.stapa.utils.LocaleManager

class RegisterActivity : BaseActivity(){

    private lateinit var viewBinding : RegisterActivityBinding
    private lateinit var navController : NavController

    //Language re-config
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase?:baseContext))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = RegisterActivityBinding.inflate(layoutInflater).apply {
            setContentView(root)

            navController = findNavController(R.id.fragmentContainer)
        }
    }

    override fun onBackPressed() {

    }
}