package com.owndays.stapa.register.tutorial.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.databinding.TutorialFragmentBinding
import com.owndays.stapa.register.viewmodel.RegisterViewModel
import com.owndays.stapa.utils.getViewModelFactory

class TutorialFragment : Fragment(){

    private lateinit var viewBinding : TutorialFragmentBinding
    private val registerViewModel by activityViewModels<RegisterViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = TutorialFragmentBinding.inflate(layoutInflater).apply {
            registerviewmodel = registerViewModel
            oncompletetutorialclick = onCompleteTutorialClick
        }
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
    }

    private fun setupView(){

    }

    private fun setupEvent(){

    }

    private val onCompleteTutorialClick = View.OnClickListener{

    }
}