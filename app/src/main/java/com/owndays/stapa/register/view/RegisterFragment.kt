package com.owndays.stapa.register.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.RegisterFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.dialog.view.DialogPicker
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.register.viewmodel.RegisterViewModel
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory

class RegisterFragment : Fragment(){

    private lateinit var viewDataBinding : RegisterFragmentBinding
    private val registerViewModel by activityViewModels<RegisterViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = RegisterFragmentBinding.inflate(layoutInflater).apply {
            registerviewmodel = registerViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
    }

    private fun setupView(){
        viewDataBinding.apply {
            textBlood.setOnClickListener {
                val pickerDialog = DialogPicker(0)
                pickerDialog.show(childFragmentManager, "DialogPicker : BloodPicker")
            }
            textLanguage.setOnClickListener {
                val pickerDialog = DialogPicker(1)
                pickerDialog.show(childFragmentManager, "DialogPicker : LanguagePicker")
            }
            textDeliveryProvince.setOnClickListener {
                val pickerDialog = DialogPicker(2)
                pickerDialog.show(childFragmentManager, "DialogPicker : ProvincePicker")
            }
        }
    }

    private fun setupEvent(){
        registerViewModel.apply {
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onLoadProfileComplete.observe(viewLifecycleOwner, EventObserver { _name ->
                viewDataBinding.textGreeting.text = String.format(getString(R.string.profile_header_msg), _name)
            })
            onRegisterComplete.observe(viewLifecycleOwner, EventObserver {
                val intent = Intent(requireActivity(), MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }
}