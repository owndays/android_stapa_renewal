package com.owndays.stapa.register.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.owndays.stapa.BaseViewModel
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.Event
import com.owndays.stapa.data.model.*
import com.owndays.stapa.data.source.remote.model.request.StaffRegisterRequest
import com.owndays.stapa.data.source.remote.model.response.RegisterFormResponse
import com.owndays.stapa.data.source.repository.RegisterFormRepository
import com.owndays.stapa.data.source.repository.UserRepository
import kotlinx.coroutines.launch

class RegisterViewModel(
    private val registerFormRepository: RegisterFormRepository,
    private val userRepository: UserRepository
) : BaseViewModel(){

    private val _dataLoading = MutableLiveData<Boolean>().apply { value = false }
    val dataLoading : LiveData<Boolean> = _dataLoading

    private val _userItem = MutableLiveData<User>()
    val userItem : LiveData<User> = _userItem

    private val _registerItem = MutableLiveData<RegisterFormResponse>()
    val registerItem : LiveData<RegisterFormResponse> = _registerItem

    private val _bloodItems = MutableLiveData<List<Blood>>().apply { value = emptyList()}
    private val _languageItems = MutableLiveData<List<Language>>().apply { value = emptyList()}
    private val _provinceItems = MutableLiveData<List<Province>>().apply { value = emptyList()}

    private val _digitalItem = MutableLiveData<DigitalId>()
    val digitalItem : LiveData<DigitalId> = _digitalItem

    // Two-way databinding, exposing MutableLiveData
    val userLangName = MutableLiveData<String>().apply { value = "" }
    val userBloodName = MutableLiveData<String>().apply { value = "" }
    val userPhone = MutableLiveData<String>().apply { value = "" }
    val userHobby = MutableLiveData<String>().apply { value = "" }

    val addressName = MutableLiveData<String>().apply { value = "" }
    val addressZip = MutableLiveData<String>().apply { value = "" }
    val addressProvinceName = MutableLiveData<String>().apply { value = "" }
    val addressCity = MutableLiveData<String>().apply { value = "" }
    val addressTown = MutableLiveData<String>().apply { value = "" }
    val addressOther = MutableLiveData<String>().apply { value = "" }
    val addressTel = MutableLiveData<String>().apply { value = "" }

    private val _headerMessage = MutableLiveData<String>().apply { value = "" }
    val headerMessage : LiveData<String> = _headerMessage

    private val _countTutorial = MutableLiveData<Int>().apply { value = 1 }
    val countTutorial : LiveData<Int> = _countTutorial

    //Event
    private val _onLoadProfileComplete = MutableLiveData<Event<String>>()
    val onLoadProfileComplete: LiveData<Event<String>> = _onLoadProfileComplete

    private val _onOpenPicker = MutableLiveData<Event<List<String>>>()
    val onOpenPicker: LiveData<Event<List<String>>> = _onOpenPicker

    private val _onRegisterComplete = MutableLiveData<Event<Unit>>()
    val onRegisterComplete: LiveData<Event<Unit>> = _onRegisterComplete

    private val _onCheckLanguage = MutableLiveData<Event<String>>()
    val onCheckLanguage: LiveData<Event<String>> = _onCheckLanguage

    private val _onPopupApiError = MutableLiveData<Event<String>>()
    val onPopupApiError: LiveData<Event<String>> = _onPopupApiError

    private val _onPopupError = MutableLiveData<Event<Int>>()
    val onPopupError: LiveData<Event<Int>> = _onPopupError

    private var accessToken = ""

    init {
        accessToken = ServiceLocator.getUserToken()

        if(_registerItem.value == null) getRegisterFormProfile()
    }

    fun getBloodNameList() : List<String>{
        var bloodNameList = ArrayList<String>()
        _bloodItems.value?.forEach { _blood ->
            bloodNameList.add(_blood.name?:"")
        }
        return bloodNameList
    }

    fun getLanguageNameList() : List<String>{
        var languageNameList = ArrayList<String>()
        _languageItems.value?.forEach { _language ->
            languageNameList.add(_language.name?:"")
        }
        return languageNameList
    }

    fun getProvinceNameList() : List<String>{
        var provinceNameList = ArrayList<String>()
        _provinceItems.value?.forEach { _province ->
            provinceNameList.add(_province.name?:"")
        }
        return provinceNameList
    }

    fun validateRegister(){
//        if(!userPhone.value.isNullOrEmpty()){
//            if(!userHobby.value.isNullOrEmpty()){
//                if(!addressName.value.isNullOrEmpty()){
//                    if(!addressZip.value.isNullOrEmpty()){
//                        lateinit var province : Province
//                        _provinceItems.value?.forEach { _province ->
//                            if(addressProvinceName.value == _province.name) province = _province
//                        }
//                        if(province != null){
//                            if(!addressCity.value.isNullOrEmpty()){
//                                if(!addressTown.value.isNullOrEmpty()){
//                                    if(!addressTel.value.isNullOrEmpty()){
//                                        if(isValidateTel(province, addressTel.value?:"")){
//                                            lateinit var blood: Blood
//                                            _bloodItems.value?.forEach { _blood ->
//                                                if(userBloodName.value == _blood.name) blood = _blood
//                                            }
//                                            lateinit var language : Language
//                                            _languageItems.value?.forEach { _language ->
//                                                if(userLangName.value == _language.name) language = _language
//                                            }
//                                            val user = User(
//                                                _userItem.value?.id,
//                                                _userItem.value?.staffId,
//                                                _userItem.value?.name,
//                                                _userItem.value?.imageUrl,
//                                                blood.id,
//                                                userHobby.value,
//                                                province.id,
//                                                language.id,
//                                                _userItem.value?.departmentName?:_registerItem.value?.departmentName,
//                                                _userItem.value?.imageBase64,
//                                                addressTel.value,
//                                                _userItem.value?.badges,
//                                                language,
//                                                blood
//                                            )
//                                            val digitalId = DigitalId(
//                                                _digitalItem.value?.id,
//                                                _digitalItem.value?.status,
//                                                _digitalItem.value?.departmentName?:_registerItem.value?.departmentName,
//                                                _digitalItem.value?.name?:_userItem.value?.name,
//                                                _digitalItem.value?.imageBase64,
//                                                _digitalItem.value?.imageUrl
//                                            )
//                                            val address = Address(
//                                            null,
//                                                null,
//                                                addressName.value,
//                                                addressZip.value,
//                                                province,
//                                                province.id,
//                                                addressTown.value,
//                                                addressCity.value,
//                                                null,
//                                                addressOther.value,
//                                                addressTel.value
//                                            )
//                                            register(user, digitalId, address)
//                                        }//No popup, in validate function already
//                                    }else _onPopupError.value = Event(R.string.profile_delivery_tel_required)
//                                }else _onPopupError.value = Event(R.string.profile_delivery_town_required)
//                            }else _onPopupError.value = Event(R.string.profile_delivery_city_required)
//                        }else _onPopupError.value = Event(R.string.profile_delivery_province_required)
//                    }else _onPopupError.value = Event(R.string.profile_delivery_yubin_required)
//                }else _onPopupError.value = Event(R.string.profile_delivery_name_required)
//            }else _onPopupError.value = Event(R.string.profile_hobby_required)
//        }else _onPopupError.value = Event(R.string.profile_tel_required)
        _onRegisterComplete.value = Event(Unit)
    }

    private fun isValidateTel(province: Province, telNo: String):Boolean{
        when(province.id){
            1 -> {
                return if(telNo.length in 10..11) true
                else {
                    _onPopupError.value = Event(R.string.profile_delivery_tel_required)
                    false
                }
            }
            2,4 -> {
                return if(telNo.length in 9..10) true
                else {
                    _onPopupError.value = Event(R.string.profile_delivery_tel_required)
                    false
                }
            }
            3 -> {
                return if(telNo.length == 8) true
                else {
                    _onPopupError.value = Event(R.string.profile_delivery_tel_required)
                    false
                }
            }
            else -> { telNo.length in 8..12 }
        }
        return false
    }

    fun toNextPage(){
        _countTutorial.value = (_countTutorial.value?:1) + 1
    }

    private fun getRegisterFormProfile(){
        _dataLoading.value = true
        viewModelScope.launch {
            val lang = ServiceLocator.getLanguage()
            registerFormRepository.getRegisterForm(accessToken, lang, lang,
                { _registerFormResponse ->
                    _registerItem.value = _registerFormResponse
                    _bloodItems.value = _registerFormResponse.bloods?: emptyList()
                    _languageItems.value = _registerFormResponse.languages?: emptyList()
                    _provinceItems.value = _registerFormResponse.provinces?: emptyList()
                    getUserProfile()
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message?:"")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getUserProfile(){
        viewModelScope.launch {
            userRepository.getUser(accessToken, ServiceLocator.getLanguage(),
                { _userInfoResponse ->
                    _userItem.value = _userInfoResponse.user
                    _bloodItems.value?.forEach { _blood ->
                        if(_userInfoResponse.user?.bloodId?:0 == _blood.id)
                            userBloodName.value = _blood.name
                    }
                    _languageItems.value?.forEach { _language ->
                        if(_userInfoResponse.user?.languageId?:0 == _language.id)
                            userLangName.value = _language.name
                    }
                    _onLoadProfileComplete.value = Event(_userInfoResponse.user?.name ?: "")
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun register(user: User, digitalId: DigitalId, address: Address){
        val staffRegisterRequest = StaffRegisterRequest(user, digitalId, address)
        viewModelScope.launch {
            userRepository.register(accessToken, ServiceLocator.getLanguage(), staffRegisterRequest,
                {

                },
                {

                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }
}