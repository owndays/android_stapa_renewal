package com.owndays.stapa

import android.content.Context
import androidx.annotation.VisibleForTesting
import androidx.room.Room
import com.google.gson.Gson
import com.owndays.stapa.data.StapaDatabase
import com.owndays.stapa.data.source.*
import com.owndays.stapa.data.source.local.DigitalIdLocalDataSource
import com.owndays.stapa.data.source.local.ShopLocalDataSource
import com.owndays.stapa.data.source.local.UserLocalDataSource
import com.owndays.stapa.data.source.remote.*
import com.owndays.stapa.data.source.repository.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

const val DB_NAME = "stapa_database"

object ServiceLocator {

    private var database: StapaDatabase? = null
    private var apiService: OwndaysService? = null
    private var gson: Gson? = null
    private var userToken: String = ""
    private var lang: String = ""

    @Volatile
    var bloodRepository: BloodRepository? = null
        @VisibleForTesting set
    var ideaRepository: IdeaRepository? = null
        @VisibleForTesting set
    var languageRepository: LanguageRepository? = null
        @VisibleForTesting set
    var notificationRepository: NotificationRepository? = null
        @VisibleForTesting set
    var orderRepository: OrderRepository? = null
        @VisibleForTesting set
    var registerFormRepository: RegisterFormRepository? = null
        @VisibleForTesting set
    var shopRepository: ShopRepository? = null
        @VisibleForTesting set
    var staffRepository: StaffRepository? = null
        @VisibleForTesting set
    var userRepository: UserRepository? = null
        @VisibleForTesting set

    fun provideBloodRepository(): BloodRepository {
        synchronized(this) {
            return bloodRepository ?: bloodRepository ?: createBloodRepository()
        }
    }

    fun provideIdeaRepository(): IdeaRepository {
        synchronized(this) {
            return ideaRepository ?: ideaRepository ?: createIdeaRepository()
        }
    }

    fun provideLanguageRepository(): LanguageRepository {
        synchronized(this) {
            return languageRepository ?: languageRepository ?: createLanguageRepository()
        }
    }

    fun provideNotificationRepository(): NotificationRepository {
        synchronized(this) {
            return notificationRepository ?: notificationRepository ?: createNotificationRepository()
        }
    }

    fun provideOrderRepository(): OrderRepository {
        synchronized(this) {
            return orderRepository ?: orderRepository ?: createOrderRepository()
        }
    }

    fun provideRegisterFormRepository(): RegisterFormRepository {
        synchronized(this) {
            return registerFormRepository ?: registerFormRepository ?: createRegisterFormRepository()
        }
    }

    fun provideShopRepository(context: Context): ShopRepository {
        synchronized(this) {
            return shopRepository ?: shopRepository ?: createShopRepository(context)
        }
    }

    fun provideStaffRepository(): StaffRepository {
        synchronized(this) {
            return staffRepository ?: staffRepository ?: createStaffRepository()
        }
    }

    fun provideUserRepository(context: Context): UserRepository {
        synchronized(this) {
            return userRepository ?: userRepository ?: createUserRepository(context)
        }
    }

    private fun createBloodRepository(): BloodRepository {
        val newRepo =
            DefaultBloodRepository(
                createBloodRemoteDataSource()
            )
        bloodRepository = newRepo
        return newRepo
    }

    private fun createIdeaRepository(): IdeaRepository {
        val newRepo =
            DefaultIdeaRepository(
                createIdeaRemoteDataSource()
            )
        ideaRepository = newRepo
        return newRepo
    }

    private fun createLanguageRepository(): LanguageRepository {
        val newRepo =
            DefaultLanguageRepository(
                createLanguageRemoteDataSource()
            )
        languageRepository = newRepo
        return newRepo
    }

    private fun createNotificationRepository(): NotificationRepository {
        val newRepo =
            DefaultNotificationRepository(
                createNotificationRemoteDataSource()
            )
        notificationRepository = newRepo
        return newRepo
    }

    private fun createOrderRepository(): OrderRepository {
        val newRepo =
            DefaultOrderRepository(
                createOrderRemoteDataSource()
            )
        orderRepository = newRepo
        return newRepo
    }

    private fun createRegisterFormRepository(): RegisterFormRepository {
        val newRepo =
            DefaultRegisterFormRepository(
                createRegisterFormDataSource()
            )
        registerFormRepository = newRepo
        return newRepo
    }

    private fun createShopRepository(context: Context): ShopRepository {
        val newRepo =
            DefaultShopRepository(
                createShopRemoteDataSource(),
                createShopLocalDataSource(context)
            )
        shopRepository = newRepo
        return newRepo
    }

    private fun createStaffRepository(): StaffRepository {
        val newRepo =
            DefaultStaffRepository(
                createStaffRemoteDataSource()
            )
        staffRepository = newRepo
        return newRepo
    }

    private fun createUserRepository(context: Context): UserRepository {
        val newRepo =
            DefaultUserRepository(
                createAddressRemoteDataSource(),
                createCheckInRemoteDataSource(),
                createDigitalIdRemoteDataSource(),
                createDigitalIdLocalDataSource(context),
                createGachaRemoteDataSource(),
                createMileRemoteDataSource(),
                createNotificationRemoteDataSource(),
                createOrderRemoteDataSource(),
                createProvinceDataSource(),
                createShiftRemoteDataSource(),
                createUserRemoteDataSource(),
                createUserLocalDataSource(context)
            )
        userRepository = newRepo
        return newRepo
    }

    private fun createAddressRemoteDataSource(): AddressDataSource {
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return AddressRemoteDataSource(apiService, gson)
    }

    private fun createBloodRemoteDataSource(): BloodDataSource {
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return BloodRemoteDataSource(apiService, gson)
    }

    private fun createCheckInRemoteDataSource(): CheckInDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return CheckInRemoteDataSource(apiService, gson)
    }

    private fun createDigitalIdRemoteDataSource(): DigitalIdDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return DigitalIdRemoteDataSource(apiService, gson)
    }

    private fun createDigitalIdLocalDataSource(context: Context): DigitalIdDataSource{
        val database = database ?: createDataBase(context)
        return DigitalIdLocalDataSource(database.digitalIdDao())
    }

    private fun createGachaRemoteDataSource(): GachaDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return GachaRemoteDataSource(apiService, gson)
    }

    private fun createIdeaRemoteDataSource(): IdeaDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return IdeaRemoteDataSource(apiService, gson)
    }

    private fun createLanguageRemoteDataSource(): LanguageDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return LanguageRemoteDataSource(apiService, gson)
    }

    private fun createMileRemoteDataSource(): MileDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return MileRemoteDataSource(apiService, gson)
    }

    private fun createNotificationRemoteDataSource(): NotificationDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return NotificationRemoteDataSource(apiService, gson)
    }

    private fun createOrderRemoteDataSource(): OrderDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return OrderRemoteDataSource(apiService, gson)
    }

    private fun createProvinceDataSource(): ProvinceDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return ProvinceRemoteDataSource(apiService, gson)
    }

    private fun createRegisterFormDataSource(): RegisterFormDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return RegisterFormRemoteDataSource(apiService, gson)
    }

    private fun createShiftRemoteDataSource(): ShiftDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return ShiftRemoteDataSource(apiService, gson)
    }

    private fun createShopRemoteDataSource(): ShopDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return ShopRemoteDataSource(apiService, gson)
    }

    private fun createShopLocalDataSource(context: Context): ShopDataSource {
        val database = database ?: createDataBase(context)
        return ShopLocalDataSource(database.shopDao())
    }

    private fun createStaffRemoteDataSource(): StaffDataSource{
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return StaffRemoteDataSource(apiService, gson)
    }

    private fun createUserRemoteDataSource(): UserDataSource {
        val apiService = apiService ?: createApiService()
        val gson = gson ?: createGson()
        return UserRemoteDataSource(apiService, gson)
    }

    private fun createUserLocalDataSource(context: Context): UserDataSource {
        val database = database ?: createDataBase(context)
        return UserLocalDataSource(database.userDao())
    }

    private fun createDataBase(context: Context): StapaDatabase {
        val result = Room.databaseBuilder(
            context.applicationContext,
            StapaDatabase::class.java, DB_NAME
        ).build()
        database = result
        return result
    }

    fun getUserToken() = "Bearer $userToken"

    fun setUserToken(token: String){
        userToken = token
    }

    fun clearUserToken(){
        userToken = ""
    }

    fun getLanguage(): String{
        return if(lang.isNullOrEmpty())
            "ja"
        else lang
    }

    fun setLanguage(_lang: String){
        lang = _lang
    }

    private fun createApiService(): OwndaysService {
        val interceptor = HttpLoggingInterceptor().apply {
            level = when (BuildConfig.DEBUG) {
                true -> HttpLoggingInterceptor.Level.BODY
                else -> HttpLoggingInterceptor.Level.HEADERS
            }
        }

        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
            .baseUrl(BuildConfig.DOMAIN + BuildConfig.API_PATH)
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(OwndaysService::class.java)
    }

    private fun createGson() = Gson()
}