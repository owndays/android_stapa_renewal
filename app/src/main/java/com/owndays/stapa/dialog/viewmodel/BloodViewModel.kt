package com.owndays.stapa.dialog.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.owndays.stapa.data.Event
import com.owndays.stapa.data.model.Blood

class BloodViewModel : ViewModel(){

    private val _dataLoading = MutableLiveData<Boolean>().apply { value = false }
    val dataLoading : LiveData<Boolean> = _dataLoading

    private val _bloodItems = MutableLiveData<List<Blood>>().apply { value = emptyList() }
    val bloodItems : LiveData<List<Blood>> = _bloodItems

    private val _selectedBloodItem = MutableLiveData<Blood>()
    val selectedBloodItem : LiveData<Blood> = _selectedBloodItem

    private val _onBloodSelected = MutableLiveData<Event<Blood>>()
    val onBloodSelected : LiveData<Event<Blood>> = _onBloodSelected

    fun setBloodList(bloodList: List<Blood>){
        _bloodItems.value = bloodList
    }

    fun setBloodItem(blood: Blood?){
        if(blood != null) _selectedBloodItem.value = blood
    }

    fun selectBlood(blood: Blood){
        _selectedBloodItem.value = blood
        _onBloodSelected.value = Event(blood)
    }
}