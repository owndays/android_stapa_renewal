package com.owndays.stapa.dialog.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.data.model.Province
import com.owndays.stapa.databinding.ProvinceDialogFragmentBinding
import com.owndays.stapa.dialog.viewmodel.ProvinceViewModel
import com.owndays.stapa.main.adapters.ProvinceItemAdapter
import com.owndays.stapa.utils.getViewModelFactory

const val PROVINCE_DIALOG_FRAGMENT = "ProvinceDialogFragment"

class ProvinceDialogFragment(private val provinceSelected: (province: Province) -> Unit) : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: ProvinceDialogFragmentBinding
    private lateinit var listAdapter: ProvinceItemAdapter
    private val provinceViewModel by activityViewModels<ProvinceViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = ProvinceDialogFragmentBinding.inflate(layoutInflater).apply {
            provinceviewmodel = provinceViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            btnBack.setOnClickListener { dismiss() }
        }
    }

    private fun setupEvent(){
        provinceViewModel.apply {
            onProvinceSelected.observe(viewLifecycleOwner, EventObserver { _blood ->
                listAdapter.notifyDataSetChanged()
                provinceSelected.invoke(_blood)
                dismiss()
            })
        }
    }

    private fun setupAdapter(){
        listAdapter = ProvinceItemAdapter(provinceViewModel)
        viewDataBinding.provinceListRecyclerView.adapter = listAdapter
    }
}