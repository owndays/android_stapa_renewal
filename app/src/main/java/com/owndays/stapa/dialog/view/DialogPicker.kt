package com.owndays.stapa.dialog.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.databinding.DialogPickerBinding
import com.owndays.stapa.register.viewmodel.RegisterViewModel
import com.owndays.stapa.utils.getViewModelFactory

//0 : Blood
//1 : Language
//2 : Province
class DialogPicker(
    private val mode: Int
) : AppCompatDialogFragment(){

    private lateinit var viewBinding: DialogPickerBinding
    private val registerViewModel by activityViewModels<RegisterViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DialogPickerBinding.inflate(layoutInflater)
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewBinding.apply {
            picker.apply {
               when(mode){
                   0 -> {
                       data = registerViewModel.getBloodNameList()
                       for(i in 0 until data.size){
                           if(registerViewModel.userBloodName.value == data[i]) setSelectedItemPosition(i, false)
                       }
                   }
                   1 -> {
                       data = registerViewModel.getLanguageNameList()
                       for(i in 0 until data.size){
                           if(registerViewModel.userLangName.value == data[i]) setSelectedItemPosition(i, false)
                       }
                   }
                   2 -> {
                       data = registerViewModel.getProvinceNameList()
                       for(i in 0 until data.size){
                           if(registerViewModel.addressProvinceName.value == data[i]) setSelectedItemPosition(i, false)
                       }
                   }
               }
            }
            btnCancel.setOnClickListener { dismiss() }
            btnDone.setOnClickListener {
                when(mode){
                    0 -> {
                        registerViewModel.userBloodName.value = "${picker.data[picker.currentItemPosition]}"
                    }
                    1 -> {
                        registerViewModel.userLangName.value = "${picker.data[picker.currentItemPosition]}"
                    }
                    2 -> {
                        registerViewModel.addressProvinceName.value = "${picker.data[picker.currentItemPosition]}"
                    }
                }
                dismiss()
            }
        }
    }
}