package com.owndays.stapa.dialog.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import com.owndays.stapa.databinding.DefaultCancelOkDialogBinding

const val DEFAULT_CANCEL_OK_DIALOG = "DefaultCancelOkDialog"

class DefaultCancelOkDialog(
    private val headerString: String? = "",
    private val messageString: String,
    private val btnCancelString: String,
    private val btnOkString: String,
    private val cancelCallback: () -> Unit,
    private val okCallBack: () -> Unit
) : AppCompatDialogFragment(){

    lateinit var viewBinding : DefaultCancelOkDialogBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DefaultCancelOkDialogBinding.inflate(layoutInflater).apply {
            textHeader.text = headerString
            textMessage.text = messageString
            btnCancel.text = btnCancelString
            btnOk.text = btnOkString
        }
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    private fun setupView(){
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        viewBinding.apply {
            if(headerString == "") textHeader.visibility = View.GONE

            btnCancel.setOnClickListener{
                cancelCallback.invoke()
                dismiss()
            }

            btnOk.setOnClickListener{
                okCallBack.invoke()
                dismiss()
            }
        }
    }
}