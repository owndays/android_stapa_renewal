package com.owndays.stapa.dialog.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import com.owndays.stapa.databinding.DefaultOkDialogBinding

const val DEFAULT_OK_DIALOG = "DefaultOkDialog"

class DefaultOkDialog(
    private val headerString: String? = "",
    private val messageString: String,
    private val okCallBack: () -> Unit
) : AppCompatDialogFragment(){

    lateinit var viewBinding : DefaultOkDialogBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DefaultOkDialogBinding.inflate(layoutInflater).apply {
            textHeader.text = headerString
            textMessage.text = messageString
        }
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    private fun setupView(){
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        viewBinding.apply {
            if(headerString == "") textHeader.visibility = View.GONE

            btnOk.setOnClickListener{
                okCallBack.invoke()
                dismiss()
            }
        }
    }
}