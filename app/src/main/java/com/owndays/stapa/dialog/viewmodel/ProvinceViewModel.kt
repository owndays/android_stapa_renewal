package com.owndays.stapa.dialog.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.owndays.stapa.data.Event
import com.owndays.stapa.data.model.Province

class ProvinceViewModel : ViewModel(){

    private val _dataLoading = MutableLiveData<Boolean>().apply { value = false }
    val dataLoading : LiveData<Boolean> = _dataLoading

    private val _provinceItems = MutableLiveData<List<Province>>().apply { value = emptyList() }
    val provinceItems : LiveData<List<Province>> = _provinceItems

    private val _selectedProvinceItem = MutableLiveData<Province>()
    val selectedProvinceItem : LiveData<Province> = _selectedProvinceItem

    private val _onProvinceSelected = MutableLiveData<Event<Province>>()
    val onProvinceSelected : LiveData<Event<Province>> = _onProvinceSelected

    fun setProvinceList(provinceList: List<Province>){
        _provinceItems.value = provinceList
    }

    fun setProvinceItem(province: Province?){
        if(province != null) _selectedProvinceItem.value = province
    }

    fun selectProvince(province: Province){
        _selectedProvinceItem.value = province
        _onProvinceSelected.value = Event(province)
    }
}