package com.owndays.stapa.dialog.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.data.model.Blood
import com.owndays.stapa.databinding.BloodDialogFragmentBinding
import com.owndays.stapa.dialog.viewmodel.BloodViewModel
import com.owndays.stapa.main.adapters.BloodItemAdapter
import com.owndays.stapa.utils.getViewModelFactory

const val BLOOD_DIALOG_FRAGMENT = "BloodDialogFragment"

class BloodDialogFragment(private val bloodSelected: (blood: Blood) -> Unit) : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: BloodDialogFragmentBinding
    private lateinit var listAdapter: BloodItemAdapter
    private val bloodViewModel by activityViewModels<BloodViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = BloodDialogFragmentBinding.inflate(layoutInflater).apply {
            bloodviewmodel = bloodViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            btnBack.setOnClickListener { dismiss() }
        }
    }

    private fun setupEvent(){
        bloodViewModel.apply {
            onBloodSelected.observe(viewLifecycleOwner, EventObserver { _blood ->
                listAdapter.notifyDataSetChanged()
                bloodSelected.invoke(_blood)
                dismiss()
            })
        }
    }

    private fun setupAdapter(){
        listAdapter = BloodItemAdapter(bloodViewModel)
        viewDataBinding.bloodListRecyclerView.adapter = listAdapter
    }
}