package com.owndays.stapa.dialog.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import com.owndays.stapa.databinding.NumberPickerDialogBinding

const val NUMBER_PICKER_DIALOG = "NumberPickerDialog"

class NumberPickerDialog(private val numberStartPicker: Int,
                         private val numberEndPicker: Int,
                         private val currentNumber: Int,
                         private val okCallback: (chosenNo : Int) -> Unit) : AppCompatDialogFragment(){

    lateinit var viewBinding: NumberPickerDialogBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = NumberPickerDialogBinding.inflate(layoutInflater)
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewBinding.apply {
            var pickerList = ArrayList<String>()
            for (i in numberStartPicker..numberEndPicker){
                pickerList.add("$i")
            }
            picker.data = pickerList
            for(i in 0 until picker.data.size-1){
                if(picker.data[i].toString() == "$currentNumber")
                    picker.setSelectedItemPosition(i, false)
            }

            btnOk.setOnClickListener{
                okCallback((picker.data[picker.currentItemPosition].toString().toInt()))
                dismiss()
            }
        }
    }
}