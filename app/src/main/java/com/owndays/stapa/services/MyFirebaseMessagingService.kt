package com.owndays.stapa.services

import android.content.Intent
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber


const val NOTI_TITLE_KEY = "notiTitleKey"
const val NOTI_MESSAGE_KEY = "notiMessageKey"
const val NOTI_DATA_KEY = "notiDataKey"
const val MESSAGE_RECEIVED_ACTION = "receivedAction"

class MyFirebaseMessagingService : FirebaseMessagingService(){

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        Timber.d("FirebaseNoti, Title : ${p0.notification?.title}")
        Timber.d("FirebaseNoti, Message : ${p0.notification?.body}")
        Timber.d("FirebaseNoti, Data : ${p0.data}")
        val leaveId = p0.data?.get("leave_request_id")?:"0"
        val badge = p0.data?.get("badge")?:"0"

        val intentNoti = Intent().apply {
            putExtra(NOTI_TITLE_KEY, p0.notification?.title?:"")
            putExtra(NOTI_MESSAGE_KEY, p0.notification?.body?:"")
            putExtra(NOTI_DATA_KEY, leaveId)
            action = MESSAGE_RECEIVED_ACTION
        }
        sendBroadcast(intentNoti)
    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Timber.d("New FcmToken : $p0")
    }
}