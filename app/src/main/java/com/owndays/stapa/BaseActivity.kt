package com.owndays.stapa

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.owndays.stapa.utils.LocaleManager

open class BaseActivity : AppCompatActivity(){

    //Language re-config
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase?:baseContext))
    }
}