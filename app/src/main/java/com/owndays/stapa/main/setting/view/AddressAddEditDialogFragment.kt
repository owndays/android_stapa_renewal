package com.owndays.stapa.main.setting.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.AddressAddeditDialogFragmentBinding
import com.owndays.stapa.dialog.view.*
import com.owndays.stapa.dialog.viewmodel.ProvinceViewModel
import com.owndays.stapa.main.setting.viewmodel.AddressViewModel
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory

const val ADDRESS_ADD_MODE = "AddressAddMode"
const val ADDRESS_EDIT_MODE = "AddressEditMode"

class AddressAddEditDialogFragment(private val mode: String) : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: AddressAddeditDialogFragmentBinding
    private val addressViewModel by activityViewModels<AddressViewModel> { getViewModelFactory() }
    private val provinceViewModel by activityViewModels<ProvinceViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = AddressAddeditDialogFragmentBinding.inflate(layoutInflater).apply {
            addressviewmodel = addressViewModel
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            if(mode == ADDRESS_ADD_MODE){
                textTitle.text = getString(R.string.setting_add_address_title)
                btnBack.visibility = View.VISIBLE
                btnCancel.visibility = View.GONE
                btnDelete.visibility = View.GONE
                textSubmit.text = getString(R.string.setting_add_address_btn)
                addressViewModel.clearAddress()
            }else if(mode == ADDRESS_EDIT_MODE){
                textTitle.text = getString(R.string.setting_address_detail_title)
                btnBack.visibility = View.GONE
                btnCancel.visibility = View.VISIBLE
                btnDelete.visibility = View.VISIBLE
                textSubmit.text = getString(R.string.setting_address_detail_save)
                addressViewModel.validateAddress()
            }

            edtName.addTextChangedListener { addressViewModel.validateAddress() }
            edtZip.addTextChangedListener { addressViewModel.validateAddress() }
            edtCity.addTextChangedListener { addressViewModel.validateAddress() }
            edtTown.addTextChangedListener { addressViewModel.validateAddress() }
            edtPhone.addTextChangedListener {  addressViewModel.validateAddress() }
        }
    }

    private fun setupEvent(){
        addressViewModel.apply {
            onCompleteAddEdit.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                addressViewModel.refreshAddressList()
                dismiss()
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private val onClickListener = View.OnClickListener {
        viewDataBinding.apply {
            when(it.id){
                R.id.btnBack -> { dismiss() }
                R.id.btnCancel -> { dismiss() }
                R.id.btnSubmit -> {
                    if(addressViewModel.isAddressValid.value == true){
                        if(mode == ADDRESS_ADD_MODE) addressViewModel.addAddress()
                        else if(mode == ADDRESS_EDIT_MODE) addressViewModel.editAddress()
                    }
                }
                R.id.btnProvince -> {
                    (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                        .hideSoftInputFromWindow(view?.windowToken,0)
                    provinceViewModel.setProvinceList(addressViewModel.provinceItems.value?: emptyList())
                    provinceViewModel.setProvinceItem(addressViewModel.addressProvince.value)
                    val provinceDialogFragment = ProvinceDialogFragment { _province ->
                        addressViewModel.setProvince(_province)
                    }
                    provinceDialogFragment.show(childFragmentManager, PROVINCE_DIALOG_FRAGMENT)
                }
                R.id.btnDelete -> {
                    val defaultCancelOkDialog = DefaultCancelOkDialog(
                        "",
                        getString(R.string.confirm_msg_001),
                        getString(R.string.dialog_cancel),
                        getString(R.string.dialog_del),
                        {},{
                            addressViewModel.confirmDeleteAddress()
                            dismiss()
                        }
                    )
                    defaultCancelOkDialog.show(childFragmentManager, DEFAULT_CANCEL_OK_DIALOG)
                }
            }
        }
    }
}