package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Document
import com.owndays.stapa.databinding.DocumentItemBinding
import com.owndays.stapa.main.adapters.DocumentItemAdapter.ViewHolder
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel

class DocumentItemAdapter(private val ideaViewModel: IdeaViewModel, private val onViewDocument: (url: String) -> Unit) : ListAdapter<Document, ViewHolder>(DocumentDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, ideaViewModel, onViewDocument)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: DocumentItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(document: Document, ideaViewModel: IdeaViewModel, onViewDocument: (url: String) -> Unit) {
            binding.apply {
                documentitem = document
                ideaviewmodel = ideaViewModel

                if(document.extension.isNullOrEmpty())
                    textName.text = document.name
                else textName.text = "${document.name}.${document.extension}"

                layoutBg.setOnClickListener {
                    if(!document.url.isNullOrEmpty()) onViewDocument.invoke(document.url)
                }
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = DocumentItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class DocumentDiffCallback : DiffUtil.ItemCallback<Document>() {
    override fun areItemsTheSame(oldItem: Document, newItem: Document): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Document, newItem: Document): Boolean {
        return oldItem == newItem
    }
}