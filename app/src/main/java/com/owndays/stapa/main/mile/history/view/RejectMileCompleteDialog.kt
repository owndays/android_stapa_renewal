package com.owndays.stapa.main.mile.history.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.databinding.RejectmileCompleteDialogBinding
import com.owndays.stapa.main.mile.viewmodel.MileViewModel
import com.owndays.stapa.utils.getViewModelFactory

const val REJECTMILE_COMPLETE_DIALOG = "RejectMileCompleteDialog"

class RejectMileCompleteDialog : AppCompatDialogFragment(){

    lateinit var viewBinding : RejectmileCompleteDialogBinding
    private val mileViewModel by activityViewModels<MileViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = RejectmileCompleteDialogBinding.inflate(layoutInflater)
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        viewBinding.apply {
            btnOk.setOnClickListener{
                dismiss()
                mileViewModel.refreshMileHistory()
            }
        }
    }
}