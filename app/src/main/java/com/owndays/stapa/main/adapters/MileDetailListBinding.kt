package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.MileDetail

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<MileDetail>?) {
    items?.let {
        (listView.adapter as MileDetailItemAdapter).submitList(items)
    }
}