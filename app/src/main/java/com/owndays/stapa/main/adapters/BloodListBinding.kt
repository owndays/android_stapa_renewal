package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Blood

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Blood>?) {
    items?.let {
        (listView.adapter as BloodItemAdapter).submitList(items)
    }
}