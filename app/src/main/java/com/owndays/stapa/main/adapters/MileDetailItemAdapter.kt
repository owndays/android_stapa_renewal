package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.owndays.stapa.R
import com.owndays.stapa.data.model.MileDetail
import com.owndays.stapa.databinding.MilehistorydetailItemBinding
import com.owndays.stapa.databinding.MilewaitItemBinding
import com.owndays.stapa.main.adapters.MileDetailItemAdapter.ViewHolder
import com.owndays.stapa.main.mile.viewmodel.MileViewModel
import com.owndays.stapa.utils.toNewPatternDateString
import java.text.DecimalFormat

const val MILEWAIT_ITEM = "MileWaitItem"
const val MILEDETAIL_ITEM = "MileDetailItem"

class MileDetailItemAdapter(private val mode: String, private val mileViewModel: MileViewModel) : ListAdapter<MileDetail, ViewHolder>(MileWaitDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, mileViewModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if(mode == MILEWAIT_ITEM){
            return ViewHolder.fromMileWait(parent, mode)
        }else{
            return ViewHolder.fromMileDetail(parent, mode)
        }
    }

    class ViewHolder : RecyclerView.ViewHolder {

        private lateinit var mileWaitItemBinding: MilewaitItemBinding
        private lateinit var mileDetailItemBinding: MilehistorydetailItemBinding
        private var mode: String

        constructor(binding: MilewaitItemBinding, _mode: String) : super(binding.root){
            mode = _mode
            mileWaitItemBinding = binding
        }
        constructor(binding: MilehistorydetailItemBinding, _mode: String) : super(binding.root){
            mode = _mode
            mileDetailItemBinding = binding
        }

        fun bind(mileDetail: MileDetail, mileViewModel: MileViewModel) {

            if(mode == MILEWAIT_ITEM){
                mileWaitItemBinding.apply {
                    miledetailitem = mileDetail

                    textDate.text = if(!mileDetail.date.isNullOrEmpty()) mileDetail.date.toNewPatternDateString("yyyy-MM-dd","yyyy.MM.dd") else ""
                    if(mileDetail.comment.isNullOrEmpty()){
                        line.visibility = View.GONE
                        textComment.visibility = View.GONE
                    } else {
                        line.visibility = View.VISIBLE
                        textComment.visibility = View.VISIBLE
                    }
                    btnAccept.setOnClickListener {
                        mileViewModel.acceptMile(mileDetail.id.toString(), mileDetail.mile?:0)
                    }
                    btnDeny.setOnClickListener {
                        mileViewModel.rejectMile(mileDetail.id.toString())
                    }

                    Glide.with(root)
                        .load(mileDetail.imageUrl)
                        .circleCrop()
                        .placeholder(R.drawable.img_placeholder_profile)
                        .into(imgProfile)
                }
                mileWaitItemBinding.executePendingBindings()
            }else if(mode == MILEDETAIL_ITEM){
                mileDetailItemBinding.apply {
                    miledetailitem = mileDetail

                    val formatter = DecimalFormat("#,###")
                    textDate.text = if(!mileDetail.date.isNullOrEmpty()) mileDetail.date.toNewPatternDateString("yyyy-MM-dd","dd") else ""
                    textMileAmount.text = if(mileDetail.mile?:0 > 0) formatter.format(mileDetail.mile?:0) else "0"
                    if(mileDetail.comment.isNullOrEmpty()){
                        line.visibility = View.GONE
                        textComment.visibility = View.GONE
                    } else {
                        line.visibility = View.VISIBLE
                        textComment.visibility = View.VISIBLE
                    }
                    if(mileDetail.status.isNullOrEmpty()){
                        textStatus.visibility = View.GONE

                        when(mileDetail.kind){
                            1 -> { iconMarker.setBackgroundResource(R.color.colorProgressSystem) }
                            3 -> { iconMarker.setBackgroundResource(R.color.colorProgressUser) }
                            else -> { iconMarker.setBackgroundResource(R.color.colorProgressContent) }
                        }
                    }
                    else textStatus.visibility = View.VISIBLE
                }
                mileDetailItemBinding.executePendingBindings()
            }
        }

        companion object {
            fun fromMileWait(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val mileWaitItemBinding = MilewaitItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(mileWaitItemBinding, mode)
            }

            fun fromMileDetail(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val mileDetailItemBinding = MilehistorydetailItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(mileDetailItemBinding, mode)
            }
        }
    }
}

class MileWaitDiffCallback : DiffUtil.ItemCallback<MileDetail>() {
    override fun areItemsTheSame(oldItem: MileDetail, newItem: MileDetail): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: MileDetail, newItem: MileDetail): Boolean {
        return oldItem == newItem
    }
}