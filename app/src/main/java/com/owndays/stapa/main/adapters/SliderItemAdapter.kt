package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.owndays.stapa.R
import com.owndays.stapa.data.model.Slider
import com.owndays.stapa.databinding.SliderItemBinding
import com.owndays.stapa.main.adapters.SliderItemAdapter.ViewHolder

class SliderItemAdapter : RecyclerView.Adapter<ViewHolder>(){

    var dataItems: MutableList<Slider> = mutableListOf()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataItems[position]
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount(): Int = dataItems.size

    fun updateDataItems(list: List<Slider>) {
        when (dataItems.size) {
            0 -> {
                dataItems.clear()
                dataItems.addAll(list)
                notifyDataSetChanged()
            }
            else -> {
                val diffCallback = SliderDiffCallback(dataItems, list)
                DiffUtil.calculateDiff(diffCallback, true)
                    .dispatchUpdatesTo(this)
                dataItems.clear()
                dataItems.addAll(list)
            }
        }
    }

    class ViewHolder private constructor(val binding: SliderItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(slider: Slider) {
            binding.apply {
                Glide.with(root)
                    .load(slider.imageUrl)
                    .placeholder(R.drawable.noimage)
                    .into(imgSlider)
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = SliderItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }

    class SliderDiffCallback (private val oldList: List<Slider>, private val newList: List<Slider>): DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size
        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldList[oldItemPosition]
            val newItem = newList[newItemPosition]
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldList[oldItemPosition]
            val newItem = newList[newItemPosition]
            return oldItem == newItem
        }
    }
}