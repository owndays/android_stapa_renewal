package com.owndays.stapa.main.adapters.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.owndays.stapa.data.model.Duration
import com.owndays.stapa.utils.toNewPatternDateString
import java.util.*
import kotlin.collections.ArrayList

class DurationViewModel : ViewModel(){

    private val _dateListItems = MutableLiveData<List<Int>>().apply { value = emptyList() }
    val dateListItems : LiveData<List<Int>> = _dateListItems

    private val _durationItems = MutableLiveData<List<Duration>>().apply { value = emptyList() }
    val durationItems : LiveData<List<Duration>> = _durationItems

    private val _monthShow = MutableLiveData<String>()
    val monthShow : LiveData<String> = _monthShow

    private val _yearShow = MutableLiveData<String>()
    val yearShow : LiveData<String> = _yearShow

    private var calInstance = Calendar.getInstance()
    private var yearFormat = ""

    fun setDuration(dateList: List<Int>){
        _dateListItems.value = dateList
        generateDuration()
    }

    fun getPrevMonth(){
        calInstance.add(Calendar.MONTH,-1)
        generateDuration()
    }

    fun getNextMonth(){
        calInstance.add(Calendar.MONTH,1)
        generateDuration()
    }

    private fun generateDuration(){
        //Reset Calendar Date
        calInstance.set(Calendar.DATE, 1)

        val dayCount = calInstance.getActualMaximum(Calendar.DAY_OF_MONTH)

        //To show in monthView
        var tempMonthDurations = ArrayList<Duration>()

        _monthShow.value = calInstance.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
        _yearShow.value = calInstance.get(Calendar.YEAR).toString().toNewPatternDateString("yyyy",yearFormat)

        //add day of week ahead
        for(i in 1 until calInstance.get(Calendar.DAY_OF_WEEK)){
            tempMonthDurations.add(Duration(
                0,
                i,
                false)
            )
        }
        //add real day
        calInstance.set(Calendar.DATE, 1)
        for(i in 1..dayCount){
            var durationToAdd = Duration(i, calInstance.get(Calendar.DAY_OF_WEEK))
            run loop@{
                _dateListItems.value?.forEach { _date ->
                    if(_date == i) {
                        durationToAdd.isChecked = true
                        return@loop
                    }
                }
            }
            tempMonthDurations.add(durationToAdd)

            //prevent from going to next month
            if(i != dayCount) calInstance.add(Calendar.DATE,1)
        }
        _durationItems.value = tempMonthDurations
    }

    fun clearDuration() {
        _durationItems.value = emptyList()
    }
}