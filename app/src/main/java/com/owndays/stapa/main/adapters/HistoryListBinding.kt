package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.History

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<History>?) {
    items?.let {
        (listView.adapter as HistoryItemAdapter).submitList(items)
    }
}