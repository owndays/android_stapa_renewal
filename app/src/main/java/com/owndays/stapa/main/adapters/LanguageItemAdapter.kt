package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.R
import com.owndays.stapa.data.model.Language
import com.owndays.stapa.databinding.LanguageItemBinding
import com.owndays.stapa.main.adapters.LanguageItemAdapter.ViewHolder
import com.owndays.stapa.main.viewmodel.MainViewModel

class LanguageItemAdapter(private val mainViewModel: MainViewModel) : ListAdapter<Language, ViewHolder>(LanguageDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, mainViewModel, position, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: LanguageItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(language: Language, mainViewModel: MainViewModel, position: Int, itemCount: Int) {
            binding.apply {
                languageitem = language
                mainviewmodel = mainViewModel

                if(position == itemCount - 1)
                    layoutBg.setBackgroundResource(R.color.colorWhite)
                else layoutBg.setBackgroundResource(R.drawable.bg_white_grey_border_bottomonly)
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = LanguageItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class LanguageDiffCallback : DiffUtil.ItemCallback<Language>() {
    override fun areItemsTheSame(oldItem: Language, newItem: Language): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Language, newItem: Language): Boolean {
        return oldItem == newItem
    }
}