package com.owndays.stapa.main.mile.history.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.data.model.MileHistory
import com.owndays.stapa.databinding.MilehistoryDetailDialogFragmentBinding
import com.owndays.stapa.main.adapters.EARN_HISTORY
import com.owndays.stapa.main.adapters.MILEDETAIL_ITEM
import com.owndays.stapa.main.adapters.MileDetailItemAdapter
import com.owndays.stapa.main.adapters.SPEND_HISTORY
import com.owndays.stapa.main.mile.viewmodel.MileViewModel
import com.owndays.stapa.utils.getViewModelFactory
import java.text.DecimalFormat

const val MILEHISTORY_DETAIL_DIALOG_FRAGMENT = "MileHistoryDetailDialogFragment"

class MileHistoryDetailDialogFragment(private val year: String, private val mileHistory: MileHistory, private val mode: String) : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: MilehistoryDetailDialogFragmentBinding
    private lateinit var listAdapter: MileDetailItemAdapter
    private val mileViewModel by activityViewModels<MileViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = MilehistoryDetailDialogFragmentBinding.inflate(layoutInflater).apply {
            mileviewmodel = mileViewModel
            monthstring = mileHistory.month
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupAdapter()

        mileViewModel.loadMileDetail(mode, year, mileHistory.month?:"1")
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val formatter = DecimalFormat("#,###")
        viewDataBinding.apply {
            if(mode == EARN_HISTORY)
                textTitle.text = getString(R.string.mile_arrival_detail_title)
            else if(mode == SPEND_HISTORY)
                textTitle.text = getString(R.string.mile_used_detail_title)

            textMileAmount.text = formatter.format(mileHistory.total)
            btnClose.setOnClickListener {
                dismiss()
            }
        }
    }

    private fun setupAdapter(){
        listAdapter = MileDetailItemAdapter(MILEDETAIL_ITEM, mileViewModel)
        viewDataBinding.mileDetailRecyclerView.adapter = listAdapter
    }
}