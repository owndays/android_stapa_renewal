package com.owndays.stapa.main.adapters.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.owndays.stapa.data.Event
import com.owndays.stapa.data.model.Category

class CategoryViewModel : ViewModel(){

    private val _dataLoading = MutableLiveData<Boolean>().apply { value = false }
    val dataLoading : LiveData<Boolean> = _dataLoading

    private val _categoryItems = MutableLiveData<List<Category>>().apply { value = emptyList() }
    val categoryItems : LiveData<List<Category>> = _categoryItems

    private val _selectedCategoryItem = MutableLiveData<Category>()
    val selectedCategoryItem : LiveData<Category> = _selectedCategoryItem

    private val _onCategorySelected = MutableLiveData<Event<Category>>()
    val onCategorySelected : LiveData<Event<Category>> = _onCategorySelected

    fun clearCategory(){
        _selectedCategoryItem.value = null
    }

    fun setCategoryList(categoryList: List<Category>){
        _categoryItems.value = categoryList
    }

    fun setCategoryItem(category: Category?){
        if(category != null) _selectedCategoryItem.value = category
    }

    fun selectCategory(category: Category){
        _selectedCategoryItem.value = category
        _onCategorySelected.value = Event(category)
    }
}