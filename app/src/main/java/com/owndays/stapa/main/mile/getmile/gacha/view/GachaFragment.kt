package com.owndays.stapa.main.mile.getmile.gacha.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.BuildConfig
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.GachaFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.DurationItemAdapter
import com.owndays.stapa.main.adapters.GACHA_MODE
import com.owndays.stapa.main.adapters.viewmodel.DurationViewModel
import com.owndays.stapa.main.view.MILEHISTORY_FRAGMENT
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.GachaWebViewJSHandler
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory

const val JS_HANDLER_NAME = "Android"

class GachaFragment : Fragment(){

    private lateinit var viewDataBinding: GachaFragmentBinding
    private lateinit var listAdapter: DurationItemAdapter
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }
    private val durationViewModel by activityViewModels<DurationViewModel> { getViewModelFactory() }

    private val GACHA_URL = "gacha/view"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = GachaFragmentBinding.inflate(layoutInflater).apply {
            mainviewmodel = mainViewModel
            durationviewmodel = durationViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupEvent()
        setupAdapter()

        durationViewModel.clearDuration()
        mainViewModel.refreshGacha()
    }

    private fun setupView(){
        viewDataBinding.apply {
            var header = HashMap<String, String>().also { _header ->
                _header["Authorization"] = ServiceLocator.getUserToken()
            }
            webView.settings.javaScriptEnabled = true
            webView.addJavascriptInterface(GachaWebViewJSHandler {
                mainViewModel.refreshGacha()
            }, JS_HANDLER_NAME)
            webView.loadUrl(BuildConfig.DOMAIN + BuildConfig.API_PATH + GACHA_URL, header)
            btnMileHistory.setOnClickListener {
                (activity as MainActivity)?.navigate(MILEHISTORY_FRAGMENT)
            }
        }
    }

    private fun setupEvent(){
        mainViewModel.apply {
            onLoadGachaHistoryComplete.observe(viewLifecycleOwner, EventObserver { _dateList ->
                durationViewModel.setDuration(_dateList)
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun setupAdapter(){
        listAdapter = DurationItemAdapter(GACHA_MODE, requireContext())
        viewDataBinding.durationGridRecyclerView.adapter = listAdapter
    }
}