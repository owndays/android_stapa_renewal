package com.owndays.stapa.main.mile.getmile.idea.view

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.IdeaPostHistoryFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.IdeaItemAdapter
import com.owndays.stapa.main.adapters.USER_IDEA
import com.owndays.stapa.main.adapters.viewmodel.CategoryViewModel
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory

class IdeaPostHistoryFragment : Fragment(){

    private lateinit var viewDataBinding : IdeaPostHistoryFragmentBinding
    private lateinit var listAdapter: IdeaItemAdapter
    private val ideaViewModel by activityViewModels<IdeaViewModel> { getViewModelFactory() }
    private val categoryViewModel by activityViewModels<CategoryViewModel> { getViewModelFactory() }
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if(intent?.action == REFRESH_LIST_ADAPTER) listAdapter.notifyDataSetChanged()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = IdeaPostHistoryFragmentBinding.inflate(layoutInflater).apply {
            ideaviewmodel = ideaViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()

        ideaViewModel.refreshIdeaPostHistory()
    }

    private fun setupView(){
        viewDataBinding.apply {
            refreshLayout.apply {
                setOnRefreshListener {
                    isRefreshing = false
                    hideKeyboard()
                    ideaViewModel.refreshIdeaPostHistory()
                }
            }
//            nestScrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, _, scrollY, _, _ ->
//                if (scrollY == (v!!.getChildAt(0)!!.measuredHeight) - v!!.height) {
//                    ideaViewModel.loadNextHistoryPaging()
//                }
//            })
            edtSearch.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    ideaViewModel.setTextKeyword(USER_IDEA)
                    ideaViewModel.filterKeyword(USER_IDEA)
                    hideKeyboard()
                    return@setOnEditorActionListener true
                }
                return@setOnEditorActionListener false
            }
            btnCategoryFilter.setOnClickListener {
                hideKeyboard()
                categoryViewModel.setCategoryItem(ideaViewModel.filterCategoryItem.value)
                val ideaCategoryFragment = IdeaFilterCategoryDialogFragment(USER_IDEA)
                ideaCategoryFragment.show(
                    childFragmentManager,
                    IDEA_FILTER_CATEGORY_DIALOG_FRAGMENT
                )
            }
        }
    }

    private fun setupEvent(){
        ideaViewModel.apply {
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun setupAdapter(){
        listAdapter = IdeaItemAdapter(USER_IDEA, ideaViewModel, requireContext(),
            { _idea ->
                LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(
                    broadCastReceiver, IntentFilter(
                        REFRESH_LIST_ADAPTER
                    )
                )
                ideaViewModel.setIdeaItemDetail(_idea)
                val ideaDetailDialogFragment = IdeaDetailDialogFragment()
                ideaDetailDialogFragment.show(childFragmentManager, IDEA_DETAIL_DIALOG_FRAGMENT)
            }, { _idea ->
                activity?.intent?.putExtra(ON_COMMENT, 1)
                ideaViewModel.setIdeaItemDetail(_idea)
                LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(
                    broadCastReceiver, IntentFilter(
                        REFRESH_LIST_ADAPTER
                    )
                )
                val ideaDetailDialogFragment = IdeaDetailDialogFragment()
                ideaDetailDialogFragment.show(childFragmentManager, IDEA_DETAIL_DIALOG_FRAGMENT)
            }, { _url ->
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(_url))
                startActivity(browserIntent)
            })
        viewDataBinding.ideaListRecyclerView.adapter = listAdapter
    }

    private fun hideKeyboard(){
        (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(view?.windowToken, 0)
    }
}