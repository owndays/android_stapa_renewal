package com.owndays.stapa.main.store.view

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.RefineBottomDialogFragmentBinding
import com.owndays.stapa.main.adapters.RefineItemAdapter
import com.owndays.stapa.main.store.viewmodel.StoreViewModel
import com.owndays.stapa.utils.getViewModelFactory

const val REFINE_BOTTOMDIALOG_FRAGMENT = "RefineBottomDialogFragment"

class RefineBottomDialogFragment : BottomSheetDialogFragment(){

    private lateinit var viewDataBinding : RefineBottomDialogFragmentBinding
    private lateinit var listAdapter : RefineItemAdapter
    private val storeViewModel by activityViewModels<StoreViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = RefineBottomDialogFragmentBinding.inflate(layoutInflater).apply {
            storeviewmodel = storeViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        //expanded bottom sheet just in case too many item
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener { dialog ->
                (dialog as BottomSheetDialog).findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)?.apply {
                    BottomSheetBehavior.from(this).state = BottomSheetBehavior.STATE_EXPANDED
                }
            }
        }
    }

    private fun setupView(){
        viewDataBinding.apply {
            btnClose.setOnClickListener { dismiss() }
        }
    }

    private fun setupEvent(){
        storeViewModel.apply {
            onRefineSelect.observe(viewLifecycleOwner, EventObserver {
                dismiss()
            })
        }
    }

    private fun setupAdapter(){
        listAdapter = RefineItemAdapter(storeViewModel)
        viewDataBinding.refineListRecyclerView.adapter = listAdapter
    }
}