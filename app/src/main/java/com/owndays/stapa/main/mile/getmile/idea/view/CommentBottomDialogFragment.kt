package com.owndays.stapa.main.mile.getmile.idea.view

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.owndays.stapa.R
import com.owndays.stapa.databinding.CommentBottomdialogFragmentBinding
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.utils.getViewModelFactory

const val COMMENT_BOTTOMDIALOG_FRAGMENT = "CommentBottomDialogFragment"

class CommentBottomDialogFragment(private val commentId : Int, private val onConfirmDelete: () -> Unit) : BottomSheetDialogFragment(){

    private lateinit var viewDataBinding: CommentBottomdialogFragmentBinding
    private val ideaViewModel by activityViewModels<IdeaViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = CommentBottomdialogFragmentBinding.inflate(layoutInflater).apply {
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        //Remove background from layout
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener { dialog ->
                (dialog as BottomSheetDialog).findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)?.background = null
            }
        }
    }

    private fun setupView(){
        viewDataBinding.apply {

        }
    }

    private fun setupEvent(){

    }

    private val onClickListener = View.OnClickListener {
        viewDataBinding.apply {
            when(it.id){
                R.id.btnEdit -> {
                    ideaViewModel.commentToggleEditMode(commentId)
                    dismiss()
                }
                R.id.btnDelete -> {
                    onConfirmDelete.invoke()
                    dismiss()
                }
                R.id.btnCancel -> {
                    dismiss()
                }
            }
        }
    }
}