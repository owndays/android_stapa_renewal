package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Product

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Product>?) {
    items?.let {
        (listView.adapter as ProductItemAdapter).submitList(items)
    }
}