package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Sort
import com.owndays.stapa.databinding.SortItemBinding
import com.owndays.stapa.main.adapters.SortItemAdapter.ViewHolder
import com.owndays.stapa.main.store.viewmodel.StoreViewModel

class SortItemAdapter(private val storeViewModel: StoreViewModel) : ListAdapter<Sort, ViewHolder>(SortDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, storeViewModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: SortItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(sort: Sort, storeViewModel: StoreViewModel) {
            binding.apply {
                sortitem = sort
                storeviewmodel = storeViewModel
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = SortItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class SortDiffCallback : DiffUtil.ItemCallback<Sort>() {
    override fun areItemsTheSame(oldItem: Sort, newItem: Sort): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Sort, newItem: Sort): Boolean {
        return oldItem == newItem
    }
}