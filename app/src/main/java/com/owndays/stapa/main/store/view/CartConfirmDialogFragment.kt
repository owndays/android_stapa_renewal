package com.owndays.stapa.main.store.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.CartConfirmDialogFragmentBinding
import com.owndays.stapa.main.adapters.PRODUCT_CHECKOUT_MODE
import com.owndays.stapa.main.adapters.ProductItemAdapter
import com.owndays.stapa.main.setting.view.ADDRESS_DIALOG_FRAGMENT
import com.owndays.stapa.main.setting.view.AddressDialogFragment
import com.owndays.stapa.main.setting.viewmodel.AddressViewModel
import com.owndays.stapa.main.store.viewmodel.StoreViewModel
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.getViewModelFactory
import java.text.DecimalFormat

const val CART_CONFIRM_DIALOG_FRAGMENT = "CartConfirmDialogFragment"

class CartConfirmDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: CartConfirmDialogFragmentBinding
    private lateinit var listAdapter : ProductItemAdapter
    private val storeViewModel by activityViewModels<StoreViewModel> { getViewModelFactory() }
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }
    private val addressViewModel by activityViewModels<AddressViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = CartConfirmDialogFragmentBinding.inflate(layoutInflater).apply {
            storeviewmodel = storeViewModel
            addressviewmodel = addressViewModel
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()

        calTotalMile()
        addressViewModel.refreshAddressList()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            btnCancel.setOnClickListener { dismiss() }
            btnBuy.setOnClickListener {

            }
        }
    }

    private fun calTotalMile(){
        viewDataBinding.apply {
            val formatter = DecimalFormat("#,###")
            var totalMile = 0
            storeViewModel.cartItems.value?.forEach { _product ->
                totalMile += ((_product.mile?:0) * (_product.cartQty?:1))
            }
            textMileAmount.text = formatter.format(totalMile)
            textMileLeftAmount.text = formatter.format((mainViewModel.mileItem.value?.balance?:0) - totalMile)
        }
    }

    private fun setupEvent(){
        storeViewModel.onCartEmpty.observe(viewLifecycleOwner, EventObserver {
            dismiss()
        })
    }

    private fun setupAdapter(){
        listAdapter = ProductItemAdapter(PRODUCT_CHECKOUT_MODE, {}, {}, {})
        viewDataBinding.cartListRecyclerView.adapter = listAdapter
    }

    private val onClickListener = View.OnClickListener {
        viewDataBinding.apply {
            when(it.id){
                R.id.btnCancel -> { dismiss() }
                R.id.btnBuy -> {

                }
                R.id.btnAddress -> {
                    val cartAddressDialogFragment = CartAddressDialogFragment()
                    cartAddressDialogFragment.show(childFragmentManager, CART_ADDRESS_DIALOG_FRAGMENT)
                }
            }
        }
    }
}