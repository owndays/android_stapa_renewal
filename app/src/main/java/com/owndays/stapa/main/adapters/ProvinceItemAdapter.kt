package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.R
import com.owndays.stapa.data.model.Province
import com.owndays.stapa.databinding.ProvinceItemBinding
import com.owndays.stapa.dialog.viewmodel.ProvinceViewModel
import com.owndays.stapa.main.adapters.ProvinceItemAdapter.ViewHolder

class ProvinceItemAdapter(private val provinceViewModel: ProvinceViewModel) : ListAdapter<Province, ViewHolder>(ProvinceDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, provinceViewModel, position, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: ProvinceItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(province: Province, provinceViewModel: ProvinceViewModel, position: Int, itemCount: Int) {
            binding.apply {
                provinceitem = province
                provinceviewmodel = provinceViewModel

                if(position == itemCount - 1)
                    layoutBg.setBackgroundResource(R.color.colorWhite)
                else layoutBg.setBackgroundResource(R.drawable.bg_white_grey_border_bottomonly)
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ProvinceItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class ProvinceDiffCallback : DiffUtil.ItemCallback<Province>() {
    override fun areItemsTheSame(oldItem: Province, newItem: Province): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Province, newItem: Province): Boolean {
        return oldItem == newItem
    }
}