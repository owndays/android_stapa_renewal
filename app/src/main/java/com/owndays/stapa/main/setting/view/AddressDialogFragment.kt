package com.owndays.stapa.main.setting.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.ItemTouchHelper
import com.owndays.stapa.R
import com.owndays.stapa.databinding.AddressDialogFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_CANCEL_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultCancelOkDialog
import com.owndays.stapa.main.adapters.AddressItemAdapter
import com.owndays.stapa.main.adapters.MODE_PROFILE_ADDRESS
import com.owndays.stapa.main.adapters.MODE_STORE_ADDRESS
import com.owndays.stapa.main.setting.viewmodel.AddressViewModel
import com.owndays.stapa.utils.getViewModelFactory
import kotlinx.android.synthetic.main.address_addedit_dialog_fragment.*

const val ADDRESS_DIALOG_FRAGMENT = "AddressDialogFragment"

class AddressDialogFragment(private val mode: String) : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: AddressDialogFragmentBinding
    private lateinit var listAdapter: AddressItemAdapter
    private val addressViewModel by activityViewModels<AddressViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = AddressDialogFragmentBinding.inflate(layoutInflater).apply {
            addressviewmodel = addressViewModel
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupAdapter()

        addressViewModel.refreshAddressList()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            if(mode == MODE_PROFILE_ADDRESS){
                textTitle.text = getString(R.string.setting_address_title)
                btnBack.visibility = View.VISIBLE
                btnCancel.visibility = View.GONE
                layoutAdd.visibility = View.VISIBLE
            }else if(mode == MODE_STORE_ADDRESS){
                textTitle.text = getString(R.string.store_cart_address_edit_title)
                btnBack.visibility = View.GONE
                btnCancel.visibility = View.VISIBLE
                layoutAdd.visibility = View.GONE
            }
        }
    }

    private fun setupAdapter(){
        listAdapter = AddressItemAdapter(MODE_PROFILE_ADDRESS, addressViewModel, { _address ->
            addressViewModel.setAddress(_address)
            val addressDialogFragment = AddressAddEditDialogFragment(ADDRESS_EDIT_MODE)
            addressDialogFragment.show(childFragmentManager, ADDRESS_EDIT_MODE)
        },{ _address ->
            addressViewModel.setAddress(_address)
            val defaultCancelOkDialog = DefaultCancelOkDialog(
                "",
                getString(R.string.confirm_msg_001),
                getString(R.string.dialog_cancel),
                getString(R.string.dialog_del),
                {},{ addressViewModel.confirmDeleteAddress() }
            )
            defaultCancelOkDialog.show(childFragmentManager, DEFAULT_CANCEL_OK_DIALOG)
        })
        viewDataBinding.addressListRecyclerView.adapter = listAdapter

        //attach swipe feature
        ItemTouchHelper(AddressItemSwipeCallback(listAdapter, requireContext())).apply {
            attachToRecyclerView(viewDataBinding.addressListRecyclerView)
        }
    }

    private val onClickListener = View.OnClickListener {
        viewDataBinding.apply {
            when(it.id){
                R.id.btnBack -> { dismiss() }
                R.id.btnCancel -> { dismiss() }
                R.id.btnAdd -> {
                    val addressAddEditDialogFragment = AddressAddEditDialogFragment(ADDRESS_ADD_MODE)
                    addressAddEditDialogFragment.show(childFragmentManager, ADDRESS_ADD_MODE)
                }
            }
        }
    }
}