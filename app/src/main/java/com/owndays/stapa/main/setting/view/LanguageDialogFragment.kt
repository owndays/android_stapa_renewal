package com.owndays.stapa.main.setting.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.LanguageDialogFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_CANCEL_OK_DIALOG
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultCancelOkDialog
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.LanguageItemAdapter
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory

const val LANGUAGE_DIALOG_FRAGMENT = "LanguageDialogFragment"

class LanguageDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: LanguageDialogFragmentBinding
    private lateinit var listAdapter: LanguageItemAdapter
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = LanguageDialogFragmentBinding.inflate(layoutInflater).apply {
            mainviewmodel = mainViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            btnBack.setOnClickListener { dismiss() }
        }
    }

    private fun setupEvent(){
        mainViewModel.apply {
            onConfirmChangeLanguage.observe(viewLifecycleOwner, EventObserver {
                val cancelOkDialog = DefaultCancelOkDialog(
                    "",
                    getString(R.string.change_language_warn),
                    getString(R.string.dialog_cancel),
                    getString(R.string.dialog_ok),
                    { mainViewModel.cancelChangeLanguage() },
                    { mainViewModel.confirmChangeLanguage() })
                cancelOkDialog.show(childFragmentManager, DEFAULT_CANCEL_OK_DIALOG)
                listAdapter.notifyDataSetChanged()
            })
            onCancelChangeLanguage.observe(viewLifecycleOwner, EventObserver {
                listAdapter.notifyDataSetChanged()
            })
            onChangeLanguageComplete.observe(viewLifecycleOwner, EventObserver { _language ->
                ServiceLocator.setLanguage(_language.code?:"ja")
                LocaleManager.setLocale(requireContext())
                (activity as MainActivity)?.recreate()
                dismiss()
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun setupAdapter(){
        listAdapter = LanguageItemAdapter(mainViewModel)
        viewDataBinding.languageListRecyclerView.adapter = listAdapter
    }
}