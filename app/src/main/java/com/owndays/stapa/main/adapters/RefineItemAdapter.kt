package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Refine
import com.owndays.stapa.databinding.RefineItemBinding
import com.owndays.stapa.main.adapters.RefineItemAdapter.ViewHolder
import com.owndays.stapa.main.store.viewmodel.StoreViewModel

class RefineItemAdapter(private val storeViewModel: StoreViewModel) : ListAdapter<Refine, ViewHolder>(RefineDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, storeViewModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: RefineItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(refine: Refine, storeViewModel: StoreViewModel) {
            binding.apply {
                refineitem = refine
                storeviewmodel = storeViewModel
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = RefineItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class RefineDiffCallback : DiffUtil.ItemCallback<Refine>() {
    override fun areItemsTheSame(oldItem: Refine, newItem: Refine): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Refine, newItem: Refine): Boolean {
        return oldItem == newItem
    }
}