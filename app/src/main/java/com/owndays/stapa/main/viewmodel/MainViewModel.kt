package com.owndays.stapa.main.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.owndays.stapa.BaseViewModel
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.Event
import com.owndays.stapa.data.model.*
import com.owndays.stapa.data.source.remote.model.request.DigitalIdCardRequest
import com.owndays.stapa.data.source.remote.model.request.LanguageRequest
import com.owndays.stapa.data.source.remote.model.request.ProfileUpdateRequest
import com.owndays.stapa.data.source.remote.model.response.UserInfoResponse
import com.owndays.stapa.data.source.repository.BloodRepository
import com.owndays.stapa.data.source.repository.LanguageRepository
import com.owndays.stapa.data.source.repository.NotificationRepository
import com.owndays.stapa.data.source.repository.UserRepository
import kotlinx.coroutines.launch
import java.util.*

class MainViewModel(
    private val userRepository: UserRepository,
    private val languageRepository: LanguageRepository,
    private val bloodRepository: BloodRepository,
    private val notificationRepository: NotificationRepository
) : BaseViewModel(){

    private val _dataLoading = MutableLiveData<Boolean>().apply { value = false }
    val dataLoading : LiveData<Boolean> = _dataLoading

    private val _mileLoading = MutableLiveData<Boolean>().apply { value = false }
    val mileLoading : LiveData<Boolean> = _mileLoading

    private val _notificationLoading = MutableLiveData<Boolean>().apply { value = false }
    val notificationLoading : LiveData<Boolean> = _notificationLoading

    private val _languageItems = MutableLiveData<List<Language>>().apply { value = emptyList() }
    val languageItems : LiveData<List<Language>> = _languageItems

    private val _selectedLanguage = MutableLiveData<Language>()
    val selectedLanguage : LiveData<Language> = _selectedLanguage

    private val _bloodItems = MutableLiveData<List<Blood>>().apply { value = emptyList() }
    val bloodItems : LiveData<List<Blood>> = _bloodItems

    private val _userItem = MutableLiveData<User>()
    val userItem : LiveData<User> = _userItem

    private val _mileItem = MutableLiveData<Mile>()
    val mileItem : LiveData<Mile> = _mileItem

    private val _mileNotiDisplay = MutableLiveData<Int>().apply { value = 0 }
    val mileNotiDisplay : LiveData<Int> = _mileNotiDisplay

    private val _shiftMonth = MutableLiveData<String>()
    val shiftMonth : LiveData<String> = _shiftMonth

    private val _shiftYear = MutableLiveData<String>()
    val shiftYear : LiveData<String> = _shiftYear

    private val _shiftItems = MutableLiveData<List<ShiftData>>()
    val shiftItems : LiveData<List<ShiftData>> = _shiftItems

    private val _gachaChanceRemain = MutableLiveData<Int>().apply { value = 0 }
    val gachaChanceRemain : LiveData<Int> = _gachaChanceRemain

    private val _notiItems = MutableLiveData<List<Notification>>()
    val notiItems : LiveData<List<Notification>> = _notiItems

    private val _notiItem = MutableLiveData<Notification>()
    val notiItem : LiveData<Notification> = _notiItem

    private val _isReadAll = MutableLiveData<Boolean>().apply { value = true }
    val isReadAll : LiveData<Boolean> = _isReadAll

    private val _digitalIdItem = MutableLiveData<DigitalId>()
    val digitalIdItem : LiveData<DigitalId> = _digitalIdItem

    private val _profileImage64 = MutableLiveData<String>().apply { value = "" }
    val profileImage64 : LiveData<String> = _profileImage64

    val profileUserName = MutableLiveData<String>().apply { value = "" }

    private val _profileBloodItem = MutableLiveData<Blood>()
    val profileBloodItem : LiveData<Blood> = _profileBloodItem

    val profileHobby = MutableLiveData<String>().apply { value = "" }

    private val _profileDigitalUrl = MutableLiveData<String>().apply { value = "" }
    val profileDigitalUrl : LiveData<String> = _profileDigitalUrl

    //Event
    private val _onLoadGachaHistoryComplete = MutableLiveData<Event<List<Int>>>()
    val onLoadGachaHistoryComplete: LiveData<Event<List<Int>>> = _onLoadGachaHistoryComplete

    private val _onMileFragmentSetting = MutableLiveData<Event<UserInfoResponse>>()
    val onMileFragmentSetting: LiveData<Event<UserInfoResponse>> = _onMileFragmentSetting

    private val _onRefreshNoti = MutableLiveData<Event<Unit>>()
    val onRefreshNoti: LiveData<Event<Unit>> = _onRefreshNoti

    private val _onOpenNotiDetail = MutableLiveData<Event<Int>>()
    val onOpenNotiDetail: LiveData<Event<Int>> = _onOpenNotiDetail

    private val _onOpenMileWait = MutableLiveData<Event<Unit>>()
    val onOpenMileWait: LiveData<Event<Unit>> = _onOpenMileWait

    private val _onOpenOrderHistory = MutableLiveData<Event<Unit>>()
    val onOpenOrderHistory: LiveData<Event<Unit>> = _onOpenOrderHistory

    private val _onLogoutComplete = MutableLiveData<Event<Unit>>()
    val onLogoutComplete: LiveData<Event<Unit>> = _onLogoutComplete

    private val _onConfirmChangeLanguage = MutableLiveData<Event<Unit>>()
    val onConfirmChangeLanguage: LiveData<Event<Unit>> = _onConfirmChangeLanguage

    private val _onCancelChangeLanguage = MutableLiveData<Event<Unit>>()
    val onCancelChangeLanguage: LiveData<Event<Unit>> = _onCancelChangeLanguage

    private val _onChangeLanguageComplete = MutableLiveData<Event<Language>>()
    val onChangeLanguageComplete : LiveData<Event<Language>> = _onChangeLanguageComplete

    private val _onSaveProfileComplete = MutableLiveData<Event<Unit>>()
    val onSaveProfileComplete : LiveData<Event<Unit>> = _onSaveProfileComplete

    private val _onCheckLanguage = MutableLiveData<Event<String>>()
    val onCheckLanguage: LiveData<Event<String>> = _onCheckLanguage

    private val _onPopupApiError = MutableLiveData<Event<String>>()
    val onPopupApiError: LiveData<Event<String>> = _onPopupApiError

    private val _onPopupError = MutableLiveData<Event<Int>>()
    val onPopupError: LiveData<Event<Int>> = _onPopupError

    private var accessToken = ""
    private var tempConfirmLanguage : Language? = null

    init {
        accessToken = ServiceLocator.getUserToken()

        if(_userItem.value == null) getUserProfile()
        if(_languageItems.value?.isEmpty() == true) getLanguageList()
        if(_bloodItems.value?.isEmpty() == true) getBloodList()
    }

    fun refreshMileInfo(){
        getUserProfile()
    }

    fun refreshGacha(){
        getUserGachaHistory()
    }

    fun prepareShift(){
        val calendar = Calendar.getInstance()
        _shiftMonth.value = (calendar.get(Calendar.MONTH) + 1).toString()
        _shiftYear.value = calendar.get(Calendar.YEAR).toString()
        getUserShift((_shiftMonth.value?:1).toString(), (_shiftYear.value?:2020).toString())
    }

    fun refreshNotifications(){
        getNotification()
    }

    fun loadNotificationDetail(notificationId: Int){
        getNotificationDetail(notificationId.toString())
        _mileNotiDisplay.value = (_mileNotiDisplay.value?:0) - 1
        processReadSelectNotification(notificationId.toString())
    }

    fun readNotification(notification: Notification){
        if(notification.readFlag == 0){
            notification.readFlag = 1
            _mileNotiDisplay.value = (_mileNotiDisplay.value?:0) - 1
            processReadSelectNotification((notification.id?:0).toString())
            _onRefreshNoti.value = Event(Unit)
        }

        when(notification.type){
            2 -> {
                //Mile Wait
                _notiItem.value = notification
                _onOpenMileWait.value = Event(Unit)
            }
            3,4,5 -> {
                //3 Detail
                //4,5 HTML Detail
                _notiItem.value = notification
                _onOpenNotiDetail.value = Event(notification.id?:0)
            }
            6,7 -> {
                //Order History
                _notiItem.value = notification
                _onOpenOrderHistory.value = Event(Unit)
            }
            else -> {
                //Read Only
                //Do nothing
            }
        }
    }

    fun readAllNotification(){
        _notiItems.value?.forEach { _notification ->
            _notification.readFlag = 1
        }
        _onRefreshNoti.value = Event(Unit)
        processReadAllNotification()
    }

    fun logout(){
        ServiceLocator.clearUserToken()
        processRemoteLogout()
    }

    fun selectLanguage(language: Language){
        tempConfirmLanguage = _selectedLanguage.value
        _selectedLanguage.value = language
        _onConfirmChangeLanguage.value = Event(Unit)
    }

    fun confirmChangeLanguage(){
        processChangeLanguage(_selectedLanguage.value?.id?:0)
    }

    fun cancelChangeLanguage(){
        _selectedLanguage.value = tempConfirmLanguage
        _onCancelChangeLanguage.value = Event(Unit)
    }

    fun setupProfile(){
        profileUserName.value = _userItem.value?.name?:""
        _profileImage64.value = _userItem.value?.imageBase64?:""
        _profileBloodItem.value = _userItem.value?.blood
        profileHobby.value = _userItem.value?.hobby?:""
    }

    fun setProfileImage64(image64: String){
        _profileImage64.value = image64
    }

    fun setBlood(blood: Blood){
        if(blood != null) _profileBloodItem.value = blood
    }

    fun updateProfile(){
        processUpdateProfile(
            profileUserName.value?:"",
            _profileImage64.value?:"",
            _profileBloodItem.value?.id?:0,
            profileHobby.value?:"")
    }

    fun setupDigitalId(){
        profileUserName.value = _userItem.value?.name?:""
        _profileDigitalUrl.value = _digitalIdItem.value?.imageUrl?:""
    }

    fun updateDigitalId(image64: String){
        if(image64.isNotEmpty()) processUpdateDigitalId(image64)
        else _onSaveProfileComplete.value = Event(Unit)
    }

    private fun getUserProfile(){
        _mileLoading.value = true
        viewModelScope.launch {
            userRepository.getUser(accessToken, ServiceLocator.getLanguage(),
                { _userInfoResponse ->
                    _userItem.value = _userInfoResponse.user
                    _mileItem.value = _userInfoResponse.mile
                    getDigitalId()
                    _onMileFragmentSetting.value = Event(_userInfoResponse)
                },
                {
                    _mileLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getUserGachaHistory(){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.getUserGachaHistoryList(accessToken, ServiceLocator.getLanguage(),
                { _gachaResponse ->
                    _gachaChanceRemain.value = _gachaResponse.remain?:0
                    _onLoadGachaHistoryComplete.value = Event(_gachaResponse.dates?: emptyList())
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getUserShift(month: String, year: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.getUserShiftList(accessToken, ServiceLocator.getLanguage(), year, month,
                { _shiftList ->
                    _shiftItems.value = _shiftList
                    //Process Added duration
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getNotification(){
        _dataLoading.value = true
        _isReadAll.value = true
        viewModelScope.launch {
            userRepository.getUserNotificationList(accessToken, ServiceLocator.getLanguage(),
                { _notificationList ->
                    var unreadCount = 0
                    _notificationList.forEach { _notification ->
                        if(_notification.readFlag == 0) {
                            if(_isReadAll.value == true) _isReadAll.value = false
                            unreadCount++
                        }
                    }
                    _mileNotiDisplay.value = unreadCount
                    _notiItems.value = _notificationList
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processReadSelectNotification(notificationId : String){
        viewModelScope.launch {
            userRepository.readNotification(accessToken, ServiceLocator.getLanguage(), notificationId,
                {

                },
                {

                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getNotificationDetail(notificationId : String){
        _notificationLoading.value = true
        viewModelScope.launch {
            notificationRepository.getNotificationDetail(accessToken, ServiceLocator.getLanguage(), notificationId,
                { _notification ->
                    _notiItem.value = _notification
                    _notificationLoading.value = false
                },
                {
                    _notificationLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processReadAllNotification(){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.readAllNotification(accessToken, ServiceLocator.getLanguage(),
                {
                    _isReadAll.value = true
                    _mileNotiDisplay.value = 0
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getDigitalId(){
        _mileLoading.value = true
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.getUserDigitalIdCard(accessToken, ServiceLocator.getLanguage(),
                { _digitalId ->
                    _digitalIdItem.value = _digitalId
                    _mileLoading.value = false
                    _dataLoading.value = false
                },
                {
                    _mileLoading.value = false
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processRemoteLogout(){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.clearFCMToken(accessToken, ServiceLocator.getLanguage(),
                {
                    _dataLoading.value = false
                    processLocalLogout()
                },
                {
                    _dataLoading.value = false
                    processLocalLogout()
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processLocalLogout(){
        viewModelScope.launch {
            userRepository.clearUser()
            ServiceLocator.clearUserToken()
            _onLogoutComplete.value = Event(Unit)
        }
    }

    private fun getLanguageList(){
        viewModelScope.launch {
            languageRepository.getLanguageList(accessToken, ServiceLocator.getLanguage(),
                { _languageList ->
                    _languageList.forEach { _language ->
                        if(ServiceLocator.getLanguage() == _language.code?:"")
                            _selectedLanguage.value = _language
                    }
                    _languageItems.value = _languageList
                },
                {
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processChangeLanguage(langId: Int){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.changeLanguage(accessToken, ServiceLocator.getLanguage(), LanguageRequest(langId),
                {
                    var isFoundLanguage = false
                    _languageItems.value?.forEach { _language ->
                        if(langId == _language.id){
                            isFoundLanguage = true
                            _onChangeLanguageComplete.value = Event(_language)
                        }
                    }
                    if(!isFoundLanguage) _onPopupError.value = Event(R.string.err_msg_002)
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getBloodList(){
        viewModelScope.launch {
            val lang = ServiceLocator.getLanguage()
            bloodRepository.getBloodList(accessToken, lang, lang,
                { _bloodList ->
                    _bloodItems.value = _bloodList
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },{ _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processUpdateProfile(profileName: String, image64: String, bloodId: Int, hobby: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.updateUserProfile(accessToken, ServiceLocator.getLanguage(), ProfileUpdateRequest(profileName, image64, bloodId, hobby),
                {
                    _dataLoading.value = false
                    _onSaveProfileComplete.value = Event(Unit)
                    getUserProfile()
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processUpdateDigitalId(image64: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.updateDigitalIdCardImage(accessToken, ServiceLocator.getLanguage(), DigitalIdCardRequest(image64),
                {
                    _dataLoading.value = false
                    _onSaveProfileComplete.value = Event(Unit)
                    getUserProfile()
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }
}