package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Comment

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Comment>?) {
    items?.let {
        (listView.adapter as CommentItemAdapter).submitList(items)
    }
}