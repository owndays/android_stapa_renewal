package com.owndays.stapa.main.mile.view

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.viewpager2.widget.ViewPager2
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.data.model.*
import com.owndays.stapa.databinding.MileFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.MileTextSliderAdapter
import com.owndays.stapa.main.mile.givemile.view.GIVEMILE_DIALOG_FRAGMENT
import com.owndays.stapa.main.mile.givemile.view.GiveMileDialogFragment
import com.owndays.stapa.main.view.*
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory
import timber.log.Timber
import java.text.DecimalFormat

const val REFRESH_INFO = "RefreshInfo"

class MileFragment : Fragment(){

    private lateinit var viewDataBinding: MileFragmentBinding
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }
    private val mileTextSliderAdapter = MileTextSliderAdapter()
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if(intent?.action == REFRESH_INFO)
                mainViewModel.refreshMileInfo()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = MileFragmentBinding.inflate(layoutInflater).apply {
            mainviewmodel = mainViewModel
            navclick = navClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()

        mainViewModel.run {
            refreshMileInfo()
            refreshGacha()
            refreshNotifications()
        }
    }

    private fun setupView(){

    }

    private fun setupEvent(){
        mainViewModel.apply {
            //Special case, refresh badge
            mileNotiDisplay.observe(viewLifecycleOwner, { _notiCount ->
                (activity as MainActivity)?.setBadge(BADGE_NOTI, _notiCount)
            })
            onMileFragmentSetting.observe(viewLifecycleOwner, EventObserver { _userInfoResponse ->
                viewDataBinding.apply {
                    if(_userInfoResponse.mile?.rank != null) setupRank(_userInfoResponse.mile.rank.type?:NORMAL_RANK)
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
                        mileProgress.setProgress((((_userInfoResponse.mile?.grand?:0) * 100) / (_userInfoResponse.mile?.rank?.nextRank?:0)), true)
                    }else mileProgress.progress = (((_userInfoResponse.mile?.grand?:0) * 100) / (_userInfoResponse.mile?.rank?.nextRank?:0))

                    val formatter = DecimalFormat("#,###")
                    textMileAvail.text = formatter.format(_userInfoResponse.mile?.balance?:0)

                    val textSliderArray = ArrayList<String>()
                    textSliderArray.add(String.format(requireContext().getString(R.string.mile_message, _userInfoResponse.user?.name?:"")))
                    textSliderArray.add(_userInfoResponse.mile?.rank?.message?:"")
                    mileTextSliderAdapter.updateDataItems(textSliderArray)
                    pagerMileHeader.adapter = mileTextSliderAdapter
                    autoPlay(pagerMileHeader)
                }

                (activity as MainActivity)?.setDrawerUserInfo(_userInfoResponse.user)
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun autoPlay(viewPager: ViewPager2){
        var currentCount = viewPager.currentItem
        viewPager.postDelayed({run{
            try{
                if(viewPager.adapter != null && viewPager.adapter?.itemCount?:0 > 0){
                    if(currentCount >= ((viewPager.adapter?.itemCount?:0) - 1)){
                        currentCount = 0
                        viewPager.setCurrentItem(currentCount, false)
                    } else {
                        currentCount++
                        viewPager.currentItem = currentCount }
                    autoPlay(viewPager)
                }
            }catch (e: Exception){
                Timber.e(e.message)
            }
        }
        }, 5000)
    }

    private fun setupRank(rankType: String){
        viewDataBinding.apply {
            when(rankType){
                NORMAL_RANK -> {
                    val normalTheme = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankNormalTheme))
                    val normalColor = ContextCompat.getColor(requireContext(), R.color.colorRankNormalTheme)
                    btnHistory.imageTintList = normalTheme
                    btnNoti.imageTintList = normalTheme
                    badgeNoti.backgroundTintList = normalTheme
                    mileProgress.apply {
                        progressTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankNormalProgress))
                        backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankNormalBgProgress)) }
                    btnGetMile.apply {
                        setTextColor(normalColor)
                        setStrokeColorResource(R.color.colorRankNormalTheme) }
                    btnGiveMile.apply {
                        setTextColor(normalColor)
                        setStrokeColorResource(R.color.colorRankNormalTheme) }
                    btnStore.backgroundTintList = normalTheme
                    textMileProgressHeader.setTextColor(normalColor)
                    textMileRankType.setTextColor(normalColor)
                }
                BRONZE_RANK -> {
                    val bronzeTheme = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankBronzeTheme))
                    val bronzeColor = ContextCompat.getColor(requireContext(), R.color.colorRankBronzeTheme)
                    btnHistory.imageTintList = bronzeTheme
                    btnNoti.imageTintList = bronzeTheme
                    badgeNoti.backgroundTintList = bronzeTheme
                    mileProgress.apply {
                        progressTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankBronzeProgress))
                        backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankBronzeBgProgress)) }
                    btnGetMile.apply {
                        setTextColor(bronzeColor)
                        setStrokeColorResource(R.color.colorRankBronzeTheme) }
                    btnGiveMile.apply {
                        setTextColor(bronzeColor)
                        setStrokeColorResource(R.color.colorRankBronzeTheme) }
                    btnStore.backgroundTintList = bronzeTheme
                    textMileProgressHeader.setTextColor(bronzeTheme)
                    textMileRankType.setTextColor(bronzeTheme)
                }
                SILVER_RANK -> {
                    val silverTheme = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankSilverTheme))
                    val silverColor = ContextCompat.getColor(requireContext(), R.color.colorRankSilverTheme)
                    btnHistory.imageTintList = silverTheme
                    btnNoti.imageTintList = silverTheme
                    badgeNoti.backgroundTintList = silverTheme
                    mileProgress.apply {
                        progressTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankSilverProgress))
                        backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankSilverBgProgress)) }
                    btnGetMile.apply {
                        setTextColor(silverColor)
                        setStrokeColorResource(R.color.colorRankSilverTheme) }
                    btnGiveMile.apply {
                        setTextColor(silverColor)
                        setStrokeColorResource(R.color.colorRankSilverTheme) }
                    btnStore.backgroundTintList = silverTheme
                    textMileProgressHeader.setTextColor(silverTheme)
                    textMileRankType.setTextColor(silverTheme)
                }
                GOLD_RANK -> {
                    val goldTheme = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankGoldTheme))
                    val goldColor = ContextCompat.getColor(requireContext(), R.color.colorRankGoldTheme)
                    btnHistory.imageTintList = goldTheme
                    btnNoti.imageTintList = goldTheme
                    badgeNoti.backgroundTintList = goldTheme
                    mileProgress.apply {
                        progressTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankGoldProgress))
                        backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankGoldBgProgress)) }
                    btnGetMile.apply {
                        setTextColor(goldColor)
                        setStrokeColorResource(R.color.colorRankGoldTheme) }
                    btnGiveMile.apply {
                        setTextColor(goldColor)
                        setStrokeColorResource(R.color.colorRankGoldTheme) }
                    btnStore.backgroundTintList = goldTheme
                }
                PLATINUM_RANK -> {
                    val platinumTheme = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankPlatinumTheme))
                    val platinumColor = ContextCompat.getColor(requireContext(), R.color.colorRankPlatinumTheme)
                    btnHistory.imageTintList = platinumTheme
                    btnNoti.imageTintList = platinumTheme
                    badgeNoti.backgroundTintList = platinumTheme
                    mileProgress.apply {
                        progressTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankPlatinumProgress))
                        backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankPlatinumBgProgress)) }
                    btnGetMile.apply {
                        setTextColor(platinumColor)
                        setStrokeColorResource(R.color.colorRankPlatinumTheme) }
                    btnGiveMile.apply {
                        setTextColor(platinumColor)
                        setStrokeColorResource(R.color.colorRankPlatinumTheme) }
                    btnStore.backgroundTintList = platinumTheme
                }
                DIAMOND_RANK -> {
                    val diamondTheme = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankDiamondTheme))
                    val diamondColor = ContextCompat.getColor(requireContext(), R.color.colorRankDiamondTheme)
                    btnHistory.imageTintList = diamondTheme
                    btnNoti.imageTintList = diamondTheme
                    badgeNoti.backgroundTintList = diamondTheme
                    mileProgress.apply {
                        progressTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankDiamondProgress))
                        backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorRankDiamondBgProgress)) }
                    btnGetMile.apply {
                        setTextColor(diamondColor)
                        setStrokeColorResource(R.color.colorRankDiamondTheme) }
                    btnGiveMile.apply {
                        setTextColor(diamondColor)
                        setStrokeColorResource(R.color.colorRankDiamondTheme) }
                    btnStore.backgroundTintList = diamondTheme
                }
            }
        }
    }

    private val navClickListener = View.OnClickListener {
        when(it.id){
            R.id.btnGiveMile -> {
                LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(broadCastReceiver, IntentFilter(
                    REFRESH_INFO
                ))
                val giveMileFragment = GiveMileDialogFragment()
                giveMileFragment.show(childFragmentManager, GIVEMILE_DIALOG_FRAGMENT)
            }
            R.id.btnGetMile -> {
                (activity as MainActivity).navigate(GETMILE_FRAGMENT)
            }
            R.id.btnStore -> {
                (activity as MainActivity).navigate(STORE_FRAGMENT)
            }
            R.id.btnHistory -> {
                (activity as MainActivity).navigate(MILEHISTORY_FRAGMENT)
            }
            R.id.btnNoti -> {
                (activity as MainActivity).navigate(INFO_FRAGMENT)
            }
        }
    }
}