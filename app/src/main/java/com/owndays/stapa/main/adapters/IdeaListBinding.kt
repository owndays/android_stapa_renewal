package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Idea

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Idea>?) {
    items?.let {
        (listView.adapter as IdeaItemAdapter).submitList(items)
    }
}