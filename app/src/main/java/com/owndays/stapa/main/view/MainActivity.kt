package com.owndays.stapa.main.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.core.view.forEach
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.owndays.stapa.BaseActivity
import com.owndays.stapa.R
import com.owndays.stapa.data.model.User
import com.owndays.stapa.databinding.MainActivityBinding
import com.owndays.stapa.main.digitalid.view.DIGITALID_DIALOG_FRAGMENT
import com.owndays.stapa.main.digitalid.view.DigitalIdDialogFragment
import com.owndays.stapa.main.mile.getmile.idea.view.POST_FRAGMENT
import com.owndays.stapa.main.mile.getmile.idea.view.TARGET_IDEA_TAB
import com.owndays.stapa.main.mile.getmile.idea.view.TIMELINE_FRAGMENT
import com.owndays.stapa.main.mile.history.view.ACCEPTMILE_COMPLETE_DIALOG
import com.owndays.stapa.main.mile.history.view.AcceptMileCompleteDialog
import com.owndays.stapa.main.mile.history.view.TARGET_HISTORY_TAB
import com.owndays.stapa.main.mile.history.view.WAIT_FRAGMENT
import com.owndays.stapa.main.mile.view.HelpActivity
import com.owndays.stapa.main.setting.view.SETTING_DIALOG_FRAGMENT
import com.owndays.stapa.main.setting.view.SettingDialogFragment
import com.owndays.stapa.main.store.view.ALL_PICKUP
import com.owndays.stapa.main.store.view.ALL_PRODUCT
import com.owndays.stapa.main.store.view.TARGET_STORE_MODE
import com.owndays.stapa.utils.LocaleManager

const val LOGIN_FRAGMENT = "LoginFragment"
const val MILE_FRAGMENT = "MileFragment"
const val MILEHISTORY_FRAGMENT = "MileHistoryFragment"
const val MILEWAIT_FRAGMENT = "MileWaitFragment"
const val CHECKIN_FRAGMENT = "CheckinFragment"
const val GETMILE_FRAGMENT = "GetmileFragment"
const val STORE_FRAGMENT = "StoreFragment"
const val STORE_ALL_PRODUCT_FRAGMENT = "StoreAllProductFragment"
const val STORE_ALL_PICKUP_FRAGMENT = "StoreAllPickupFragment"
const val PRODUCT_DETAIL_FRAGMENT = "ProductDetailFragment"
const val IDEA_POST_FRAGMENT = "IdeaPostFragment"
const val IDEA_POST_HISTORY_FRAGMENT = "IdeaPostHistoryFragment"
const val IDEA_CREATEPOST_FRAGMENT = "IdeaCreatepostFragment"
const val IDEA_TIMELINE_FRAGMENT = "IdeaTimelineFragment"
const val GACHA_FRAGMENT = "GachaFragment"
const val SHIFT_FRAGMENT = "ShiftFragment"
const val INFO_FRAGMENT = "InfoFragment"
const val DIGITALID_FRAGMENT = "DigitalIdFragment"
const val SETTING_FRAGMENT = "SettingFragment"

const val BADGE_NOTI = "BadgeNoti"
const val BADGE_CHECKIN = "InfoFragment"
const val BADGE_CART = "BadgeCart"

class MainActivity : BaseActivity(){

    private lateinit var viewDataBinding: MainActivityBinding
    private lateinit var navController : NavController

    private var badgeFragment: String = ""

    //Language re-config
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase?:baseContext))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewDataBinding = MainActivityBinding.inflate(layoutInflater).apply {
            setContentView(root)
            setSupportActionBar(toolbar)
            navclick = navClickListener

            btnMenu.setOnClickListener {
                (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                    .hideSoftInputFromWindow(currentFocus?.windowToken,0)
                if(drawerLayout.isDrawerOpen(drawerMenu.root))
                    drawerLayout.closeDrawer(drawerMenu.root)
                else if(!drawerLayout.isDrawerOpen(drawerMenu.root))
                    drawerLayout.openDrawer(drawerMenu.root)
            }
            iconMile.isSelected = true
            badgeFragment = MILE_FRAGMENT
            btnHelp.visibility = View.VISIBLE
            btnCheckInHistory.visibility = View.GONE
            btnBack.visibility = View.GONE

            drawerMenu.apply {
                drawernavelistener = drawerNavButtonListener
                btnClose.setOnClickListener { drawerLayout.closeDrawer(drawerMenu.root) }
            }
        }

        navController = findNavController(R.id.fragmentContainer)
    }

    override fun onBackPressed() {

    }

    fun setDrawerUserInfo(user: User?){
        if(user != null){
            viewDataBinding.drawerMenu.apply {
                textName.text = user.name
                textDepartment.text = user.departmentName
                Glide.with(root)
                    .load(user.imageUrl)
                    .circleCrop()
                    .placeholder(R.drawable.img_placeholder_profile)
                    .into(imgProfile)
            }
        }
    }

    fun setBadge(badgeType: String, badgeCount: Int?){
        viewDataBinding.apply {
            badgeInfo.visibility = View.GONE
            badgeCheckIn.visibility = View.GONE
            drawerMenu.badgeInfo.visibility = View.GONE

            if(badgeType == BADGE_NOTI){
                if(badgeCount != null && badgeCount > 0){
                    badgeInfo.visibility = View.VISIBLE
                    badgeCheckIn.visibility = View.GONE
                    drawerMenu.badgeInfo.apply {
                        visibility = View.VISIBLE
                        text = badgeCount.toString()
                    }
                }else{
                    badgeInfo.visibility = View.GONE
                    badgeCheckIn.visibility = View.GONE
                    drawerMenu.badgeInfo.visibility = View.GONE
                }
            }else if(badgeType == BADGE_CHECKIN){
                badgeInfo.visibility = View.GONE
                badgeCheckIn.visibility = View.VISIBLE
                drawerMenu.badgeInfo.visibility = View.GONE
            }else if(badgeType == BADGE_CART){
                if(badgeCount?:0 > 0) badgeCart.visibility = View.VISIBLE
                else badgeCart.visibility = View.GONE
            }
        }
    }

    fun navigate(destination: String, arg: String = ""){
        viewDataBinding.apply {
            btnBack.visibility = View.GONE
            btnMenu.visibility = View.VISIBLE
            btnCancel.visibility = View.GONE
            btnHelp.visibility = View.GONE
            btnCart.visibility = View.GONE
            btnCheckInHistory.visibility = View.GONE
            buttonNav.forEach { navBtn ->
                ((navBtn as ViewGroup).getChildAt(0) as ImageView).isSelected = false
            }
            when(destination){
                INFO_FRAGMENT -> {
                    iconInfo.isSelected = true
                    badgeFragment = INFO_FRAGMENT
                    btnBack.visibility = View.VISIBLE
                    btnBack.setOnClickListener {
                        btnMile.performClick()
                    }
                    textTitle.text = getString(R.string.info_title)
                    navController.navigate(R.id.info_fragment_dest)
                }
                CHECKIN_FRAGMENT -> {
                    iconCheckIn.isSelected = true
                    badgeFragment = CHECKIN_FRAGMENT
                    btnCheckInHistory.visibility = View.VISIBLE
                    textTitle.text = getString(R.string.checkin_title)
                    navController.navigate(R.id.checkin_fragment_dest)
                }
                MILE_FRAGMENT ->{
                    iconMile.isSelected = true
                    badgeFragment = MILE_FRAGMENT
                    btnHelp.visibility = View.VISIBLE
                    textTitle.text = getString(R.string.mile_title)
                    navController.navigate(R.id.mile_fragment_dest)
                    if(arg.isNotEmpty()){
                        val acceptMileConfirmDialog = AcceptMileCompleteDialog(arg)
                        acceptMileConfirmDialog.show(supportFragmentManager, ACCEPTMILE_COMPLETE_DIALOG)
                    }
                }
                MILEHISTORY_FRAGMENT -> {
                    textTitle.text = getString(R.string.mile_history_title)
                    navController.navigate(R.id.mile_history_fragment_dest)
                }
                MILEWAIT_FRAGMENT -> {
                    textTitle.text = getString(R.string.mile_history_title)
                    var bundle = Bundle().apply { putInt(TARGET_HISTORY_TAB, WAIT_FRAGMENT) }
                    navController.navigate(R.id.mile_history_fragment_dest, bundle)
                }
                GETMILE_FRAGMENT -> {
                    iconGetMile.isSelected = true
                    badgeFragment = GETMILE_FRAGMENT
                    textTitle.text = getString(R.string.getmile_title)
                    navController.navigate(R.id.getmile_fragment_dest)
                }
                IDEA_POST_FRAGMENT -> {
                    iconGetMile.isSelected = true
                    textTitle.text = getString(R.string.getmile_idea_title)
                    var bundle = Bundle().apply { putInt(TARGET_IDEA_TAB, POST_FRAGMENT) }
                    navController.navigate(R.id.idea_fragment_dest, bundle)
                }
                IDEA_POST_HISTORY_FRAGMENT -> {
                    iconGetMile.isSelected = true
                    btnBack.visibility = View.VISIBLE
                    btnBack.setOnClickListener {
                        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                            .hideSoftInputFromWindow(currentFocus?.windowToken,0)
                        navigate(IDEA_POST_FRAGMENT)
                    }
                    btnMenu.visibility = View.VISIBLE
                    textTitle.text = getString(R.string.getmile_idea_post_action2)
                    navController.navigate(R.id.idea_post_history_fragment_dest)
                }
                IDEA_CREATEPOST_FRAGMENT -> {
                    iconGetMile.isSelected = true
                    textTitle.text = getString(R.string.getmile_idea_post_action1)
                    btnCancel.visibility = View.VISIBLE
                    btnMenu.visibility = View.GONE
                    navController.navigate(R.id.ideacreatepost_fragment_dest)
                }
                IDEA_TIMELINE_FRAGMENT -> {
                    iconGetMile.isSelected = true
                    textTitle.text = getString(R.string.getmile_idea_title)
                    var bundle = Bundle().apply { putInt(TARGET_IDEA_TAB, TIMELINE_FRAGMENT) }
                    navController.navigate(R.id.idea_fragment_dest, bundle)
                }
                GACHA_FRAGMENT -> {
                    iconGetMile.isSelected = true
                    textTitle.text = getString(R.string.gacha_title)
                    btnBack.visibility = View.VISIBLE
                    btnBack.setOnClickListener {
                        btnGetMile.performClick()
                    }
                    navController.navigate(R.id.gacha_fragment_dest)
                }
                SHIFT_FRAGMENT -> {
                    textTitle.text = getString(R.string.shift_title)
                    navController.navigate(R.id.shift_fragment_dest)
                }
                STORE_FRAGMENT -> {
                    iconStore.isSelected = true
                    badgeFragment = STORE_FRAGMENT
                    btnCart.visibility = View.VISIBLE
                    textTitle.text = getString(R.string.store_title)
                    navController.navigate(R.id.store_fragment_dest)
                }
                STORE_ALL_PRODUCT_FRAGMENT -> {
                    iconStore.isSelected = true
                    badgeFragment = STORE_FRAGMENT
                    btnCart.visibility = View.VISIBLE
                    textTitle.text = getString(R.string.store_title)
                    var bundle = Bundle().apply { putInt(TARGET_STORE_MODE, ALL_PRODUCT) }
                    navController.navigate(R.id.store_all_fragment_dest, bundle)
                }
                STORE_ALL_PICKUP_FRAGMENT -> {
                    iconStore.isSelected = true
                    badgeFragment = STORE_FRAGMENT
                    btnCart.visibility = View.VISIBLE
                    textTitle.text = getString(R.string.store_title)
                    var bundle = Bundle().apply { putInt(TARGET_STORE_MODE, ALL_PICKUP) }
                    navController.navigate(R.id.store_all_fragment_dest, bundle)
                }
                PRODUCT_DETAIL_FRAGMENT -> {
                    iconStore.isSelected = true
                    badgeFragment = STORE_FRAGMENT
                    btnCart.visibility = View.VISIBLE
                    btnBack.visibility = View.VISIBLE
                    btnBack.setOnClickListener {
                        navigate(STORE_ALL_PRODUCT_FRAGMENT)
                    }
                    textTitle.text = getString(R.string.store_title)
                    navController.navigate(R.id.product_detail_fragment_dest)
                }
                DIGITALID_FRAGMENT -> {
                    val digitalIdDialogFragment = DigitalIdDialogFragment()
                    digitalIdDialogFragment.show(supportFragmentManager, DIGITALID_DIALOG_FRAGMENT)
                }
                SETTING_FRAGMENT -> {
                    val settingDialogFragment = SettingDialogFragment()
                    settingDialogFragment.show(supportFragmentManager, SETTING_DIALOG_FRAGMENT)
                }
            }
        }
    }

    private val navClickListener = View.OnClickListener {
        viewDataBinding.apply {
            when(it.id){
                R.id.btnInfo ->{
                    navigate(INFO_FRAGMENT)
                }
                R.id.btnMile ->{
                    navigate(MILE_FRAGMENT)
                }
                R.id.btnCheckIn ->{
                    navigate(CHECKIN_FRAGMENT)
                }
                R.id.btnGetMile ->{
                    navigate(GETMILE_FRAGMENT)
                }
                R.id.btnStore ->{
                    navigate(STORE_FRAGMENT)
                }
                R.id.btnBack -> {

                }
                R.id.btnHelp -> {
                    btnHelp.visibility = View.VISIBLE
                    val intent = Intent(this@MainActivity, HelpActivity::class.java)
                    startActivity(intent)
                }
                R.id.btnCheckInHistory -> {

                }
                R.id.btnCancel -> {
                    navigate(IDEA_POST_FRAGMENT)
                }
            }
        }
    }

    private val drawerNavButtonListener = View.OnClickListener {
        viewDataBinding.apply {
            buttonNav.forEach { navBtn ->
                ((navBtn as ViewGroup).getChildAt(0) as ImageView).isSelected = false
            }
            when(badgeFragment){
                INFO_FRAGMENT -> { iconInfo.isSelected = true }
                MILE_FRAGMENT -> { iconMile.isSelected = true }
                CHECKIN_FRAGMENT -> { iconCheckIn.isSelected = true }
                GETMILE_FRAGMENT -> { iconGetMile.isSelected = true }
                STORE_FRAGMENT -> { iconStore.isSelected = true }
            }
            btnCancel.visibility = View.GONE
            btnHelp.visibility = View.GONE
            if(it.id != R.id.btnMile && it.id != R.id.btnGetMile && it.id != R.id.btnStore){
                drawerMenu.apply {
                    layoutMile.visibility = View.GONE
                    arrowMile.rotation = 0f
                    layoutGetMile.visibility = View.GONE
                    arrowGetMile.rotation = 0f
                    layoutStore.visibility = View.GONE
                    arrowStore.rotation = 0f
                }
                drawerLayout.closeDrawer(drawerMenu.root)
            }
            when(it.id){
                R.id.btnDigitalIdCard -> {
                    navigate(DIGITALID_FRAGMENT)
                }
                R.id.btnInfo ->{
                    navigate(INFO_FRAGMENT)
                }
                R.id.btnMile ->{
                    drawerMenu.apply {
                        if(layoutMile.visibility == View.VISIBLE){
                            layoutMile.visibility = View.GONE
                            arrowMile.rotation = 0f
                        }else{
                            layoutMile.visibility = View.VISIBLE
                            arrowMile.rotation = 180f
                        }
                    }
                }
                R.id.btnMileage ->{
                    btnMile.performClick()
                }
                R.id.btnMileHistory ->{
                    navigate(MILEHISTORY_FRAGMENT)
                }
                R.id.btnGetMile ->{
                    drawerMenu.apply {
                        if(layoutGetMile.visibility == View.VISIBLE){
                            layoutGetMile.visibility = View.GONE
                            arrowGetMile.rotation = 0f
                        }else{
                            layoutGetMile.visibility = View.VISIBLE
                            arrowGetMile.rotation = 180f
                        }
                    }
                }
                R.id.btnContent ->{
                    btnGetMile.performClick()
                }
                R.id.btnIdeaPost ->{
                    navigate(IDEA_POST_FRAGMENT)
                }
                R.id.btnIdeaEveryone ->{
                    navigate(IDEA_TIMELINE_FRAGMENT)
                }
                R.id.btnCheckIn ->{
                    btnCheckIn.performClick()
                }
                R.id.btnGacha ->{
                    navigate(GACHA_FRAGMENT)
                }
                R.id.btnStore ->{
                    drawerMenu.apply {
                        if(layoutStore.visibility == View.VISIBLE){
                            layoutStore.visibility = View.GONE
                            arrowStore.rotation = 0f
                        }else{
                            layoutStore.visibility = View.VISIBLE
                            arrowStore.rotation = 180f
                        }
                    }
                }
                R.id.btnStoreTop ->{
                    btnStore.performClick()
                }
                R.id.btnItemList ->{
                    navigate(STORE_ALL_PRODUCT_FRAGMENT)
                }
                R.id.btnOrderHistory ->{

                }
                R.id.btnShift ->{
                    navigate(SHIFT_FRAGMENT)
                }
                R.id.btnSetting ->{
                    navigate(SETTING_FRAGMENT)
                }
            }
        }
    }
}