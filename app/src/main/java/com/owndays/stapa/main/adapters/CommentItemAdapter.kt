package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.owndays.stapa.R
import com.owndays.stapa.data.model.Comment
import com.owndays.stapa.databinding.CommentItemBinding
import com.owndays.stapa.main.adapters.CommentItemAdapter.ViewHolder
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel

class CommentItemAdapter(private val ideaPostName: String, private val onHold: (position: Int) -> Unit, private val ideaViewModel: IdeaViewModel)
    : ListAdapter<Comment, ViewHolder>(CommentDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, ideaPostName, onHold, ideaViewModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: CommentItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(comment: Comment, ideaPostName: String, onHold: (position: Int) -> Unit, ideaViewModel: IdeaViewModel) {
            binding.apply {
                commentitem = comment

                if(comment.inEditMode) edtComment.setText(comment.text)
                layoutBg.setOnLongClickListener {
                    if(comment.name == ideaPostName){
                        onHold.invoke(comment.id?:0)
                        return@setOnLongClickListener true
                    }
                    return@setOnLongClickListener false
                }
                Glide.with(root)
                    .load(comment.imageUrl?:"")
                    .circleCrop()
                    .placeholder(R.drawable.img_placeholder_profile)
                    .into(imgPhoto)
                btnCancel.setOnClickListener {
                    ideaViewModel.commentToggleEditMode(comment.id?:0)
                }
                btnSubmitComment.setOnClickListener {
                    ideaViewModel.commentEdit(comment.id?:0, edtComment.text.toString())
                }
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = CommentItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class CommentDiffCallback : DiffUtil.ItemCallback<Comment>() {
    override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem == newItem
    }
}