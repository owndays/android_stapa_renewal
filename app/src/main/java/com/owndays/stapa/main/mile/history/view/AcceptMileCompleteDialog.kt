package com.owndays.stapa.main.mile.history.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import com.owndays.stapa.databinding.AcceptmileCompleteDialogBinding

const val ACCEPTMILE_COMPLETE_DIALOG = "AcceptMileCompleteDialog"

class AcceptMileCompleteDialog(private val mileAmountString: String) : AppCompatDialogFragment(){

    lateinit var viewBinding : AcceptmileCompleteDialogBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = AcceptmileCompleteDialogBinding.inflate(layoutInflater).apply {
            mileamount = mileAmountString
        }
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        viewBinding.apply {
            btnOk.setOnClickListener{
                dismiss()
            }
        }
    }
}