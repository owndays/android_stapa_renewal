package com.owndays.stapa.main.mile.getmile.idea.view

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.IdeaTimelineFragmentBinding
import com.owndays.stapa.main.adapters.IdeaItemAdapter
import com.owndays.stapa.main.adapters.OTHER_IDEA
import com.owndays.stapa.main.adapters.viewmodel.CategoryViewModel
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.utils.getViewModelFactory

class IdeaTimeLineFragment : Fragment(){

    private lateinit var viewDataBinding : IdeaTimelineFragmentBinding
    private lateinit var listAdapter: IdeaItemAdapter
    private val ideaViewModel by activityViewModels<IdeaViewModel> { getViewModelFactory() }
    private val categoryViewModel by activityViewModels<CategoryViewModel> { getViewModelFactory() }
    private val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if(intent?.action == REFRESH_LIST_ADAPTER) listAdapter.notifyDataSetChanged()
        }
    }

    companion object{
        fun newInstance() : IdeaTimeLineFragment{
            return IdeaTimeLineFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = IdeaTimelineFragmentBinding.inflate(layoutInflater).apply {
            ideaviewmodel = ideaViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()

        ideaViewModel.refreshIdeaOtherHistory()
    }

    private fun setupView(){
        viewDataBinding.apply {
            refreshLayout.apply {
                setOnRefreshListener {
                    isRefreshing = false
                    hideKeyboard()
                    ideaViewModel.refreshIdeaOtherHistory()
                }
            }
//            nestScrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, _, scrollY, _, _ ->
//                if(scrollY == (v!!.getChildAt(0)!!.measuredHeight) - v!!.height){
//                    ideaViewModel.loadNextIdeaOtherPaging()
//                }
//            })
            edtSearch.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    ideaViewModel.setTextKeyword(OTHER_IDEA)
                    ideaViewModel.filterKeyword(OTHER_IDEA)
                    hideKeyboard()
                    return@setOnEditorActionListener true
                }
                return@setOnEditorActionListener false
            }
            btnCategoryFilter.setOnClickListener {
                hideKeyboard()
                categoryViewModel.setCategoryItem(ideaViewModel.filterCategoryItem.value)
                val ideaCategoryFragment = IdeaFilterCategoryDialogFragment(OTHER_IDEA)
                ideaCategoryFragment.show(childFragmentManager, IDEA_FILTER_CATEGORY_DIALOG_FRAGMENT)
            }
        }
    }

    private fun setupEvent(){
        ideaViewModel.onEmergencyRefreshIdeaList.observe(viewLifecycleOwner, EventObserver{
            listAdapter.notifyDataSetChanged()
        })
    }

    private fun setupAdapter(){
        listAdapter = IdeaItemAdapter(OTHER_IDEA, ideaViewModel, requireContext(),
            { _idea ->
                LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(broadCastReceiver, IntentFilter(
                    REFRESH_LIST_ADAPTER
                ))
                ideaViewModel.setIdeaItemDetail(_idea)
                val ideaDetailDialogFragment = IdeaDetailDialogFragment()
                ideaDetailDialogFragment.show(childFragmentManager, IDEA_DETAIL_DIALOG_FRAGMENT)
            },{ _idea ->
                activity?.intent?.putExtra(ON_COMMENT, 1)
                LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(broadCastReceiver, IntentFilter(
                    REFRESH_LIST_ADAPTER
                ))
                ideaViewModel.setIdeaItemDetail(_idea)
                val ideaDetailDialogFragment = IdeaDetailDialogFragment()
                ideaDetailDialogFragment.show(childFragmentManager, IDEA_DETAIL_DIALOG_FRAGMENT)
            }, { _url ->
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(_url))
                startActivity(browserIntent)
            })
        viewDataBinding.ideaListRecyclerView.adapter = listAdapter
    }

    private fun hideKeyboard(){
        (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(view?.windowToken,0)
    }
}