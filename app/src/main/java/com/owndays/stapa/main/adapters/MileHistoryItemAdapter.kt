package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.MileHistory
import com.owndays.stapa.databinding.MilehistoryItemBinding
import com.owndays.stapa.main.adapters.MileHistoryItemAdapter.ViewHolder
import com.owndays.stapa.main.mile.viewmodel.MileViewModel
import java.text.DecimalFormat

class MileHistoryItemAdapter(private val mode: String, private val mileViewModel: MileViewModel, private val year: String,
                             private val onViewMileDetail : (year: String, mileHistory: MileHistory, mode: String) -> Unit) : ListAdapter<MileHistory, ViewHolder>(MileHistoryDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, mode, mileViewModel, year, onViewMileDetail)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: MilehistoryItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(mileHistory: MileHistory, mode: String, mileViewModel: MileViewModel, year: String,
                 onViewMileDetail: (year: String, mileHistory: MileHistory, mode: String) -> Unit) {

            binding.apply {
                milehistoryitem = mileHistory
                mileviewmodel = mileViewModel

                textMileAmount.text = "0"
                val formatter = DecimalFormat("#,###")
                var totalMile = 0
                if(mode == EARN_HISTORY){
                    if(mileHistory.system?:0 > 0){
                        totalMile += mileHistory.system?:0
                        layoutSystem.layoutParams = LinearLayout.LayoutParams(0, FrameLayout.LayoutParams.MATCH_PARENT, (mileHistory.percentSystem?:0).toFloat())
                    }

                    if(mileHistory.user?:0 > 0){
                        totalMile += mileHistory.user?:0
                        layoutUser.layoutParams = LinearLayout.LayoutParams(0, FrameLayout.LayoutParams.MATCH_PARENT, (mileHistory.percentUser?:0).toFloat())
                    }

                    if(mileHistory.content?:0 > 0){
                        totalMile += mileHistory.content?:0
                        layoutContent.layoutParams = LinearLayout.LayoutParams(0, FrameLayout.LayoutParams.MATCH_PARENT, (mileHistory.percentContent?:0).toFloat())
                    }
                }else if(mode == SPEND_HISTORY){
                    if(mileHistory.total?:0 > 0){
                        totalMile += mileHistory.total?:0
                        layoutContent.layoutParams = LinearLayout.LayoutParams(0, FrameLayout.LayoutParams.MATCH_PARENT, (mileHistory.percentSummary?:0).toFloat())
                    }
                }

                if(mileHistory.total?:0 - ((mileHistory.system?:0) + (mileHistory.user?:0) + (mileHistory.content?:0)) > 0)
                    layoutNull.layoutParams = LinearLayout.LayoutParams(0, FrameLayout.LayoutParams.MATCH_PARENT,
                        (100 - (mileHistory.percentSummary?:0)).toFloat())

                textMileAmount.text = formatter.format(totalMile)
                layoutBg.setOnClickListener {
                    onViewMileDetail.invoke(year, mileHistory, mode)
                }
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = MilehistoryItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class MileHistoryDiffCallback : DiffUtil.ItemCallback<MileHistory>() {
    override fun areItemsTheSame(oldItem: MileHistory, newItem: MileHistory): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: MileHistory, newItem: MileHistory): Boolean {
        return oldItem == newItem
    }
}