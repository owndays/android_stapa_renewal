package com.owndays.stapa.main.setting.view

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.SettingDialogFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_CANCEL_OK_DIALOG
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultCancelOkDialog
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.MODE_PROFILE_ADDRESS
import com.owndays.stapa.main.view.MILEHISTORY_FRAGMENT
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory
import com.owndays.stapa.welcome.view.WelcomeActivity

const val SETTING_DIALOG_FRAGMENT = "SettingDialogFragment"

class SettingDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: SettingDialogFragmentBinding
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = SettingDialogFragmentBinding.inflate(layoutInflater).apply {
            mainviewmodel = mainViewModel
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            Glide.with(root)
                .load(mainViewModel.userItem.value?.imageUrl?:"")
                .circleCrop()
                .placeholder(R.drawable.img_placeholder_profile)
                .into(imgProfile)
        }
    }

    private fun setupEvent(){
        mainViewModel.apply {
            //special case, reload user image
            userItem.observe(viewLifecycleOwner, {
                Glide.with(viewDataBinding.root)
                    .load(mainViewModel.userItem.value?.imageUrl?:"")
                    .placeholder(R.drawable.img_placeholder_profile)
                    .into(viewDataBinding.imgProfile)
            })
            onLogoutComplete.observe(viewLifecycleOwner, EventObserver {
                val intent = Intent(requireActivity(), WelcomeActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private val onClickListener = View.OnClickListener {
        viewDataBinding.apply {
            when(it.id){
                R.id.btnClose -> { dismiss() }
                R.id.btnProfile -> {
                    mainViewModel.setupProfile()
                    val profileDialogFragment = ProfileDialogFragment()
                    profileDialogFragment.show(childFragmentManager, PROFILE_DIALOG_FRAGMENT)
                }
                R.id.btnDigitalIdImage -> {
                    mainViewModel.setupDigitalId()
                    val digitalIdImageDialogFragment = DigitalIdImageDialogFragment()
                    digitalIdImageDialogFragment.show(childFragmentManager, DIGITALID_PHOTO_DIALOG_FRAGMENT)
                }
                R.id.btnAddress -> {
                    val addressDialogFragment = AddressDialogFragment(MODE_PROFILE_ADDRESS)
                    addressDialogFragment.show(childFragmentManager, ADDRESS_DIALOG_FRAGMENT)
                }
                R.id.btnMileHistory -> {
                    (activity as MainActivity)?.navigate(MILEHISTORY_FRAGMENT)
                    dismiss()
                }
                R.id.btnLanguage -> {
                    val languageDialogFragmentBinding = LanguageDialogFragment()
                    languageDialogFragmentBinding.show(childFragmentManager, LANGUAGE_DIALOG_FRAGMENT)
                }
                R.id.btnLogout -> {
                    val cancelOkDialog = DefaultCancelOkDialog(
                        "",
                        getString(R.string.setting_logout_msg),
                        getString(R.string.dialog_cancel),
                        getString(R.string.dialog_submit),
                        {}, { mainViewModel.logout() })
                    cancelOkDialog.show(childFragmentManager, DEFAULT_CANCEL_OK_DIALOG)
                }
            }
        }
    }
}