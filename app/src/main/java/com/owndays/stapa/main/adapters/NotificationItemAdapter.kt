package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.R
import com.owndays.stapa.data.model.Notification
import com.owndays.stapa.databinding.NotificationItemBinding
import com.owndays.stapa.main.adapters.NotificationItemAdapter.ViewHolder
import com.owndays.stapa.main.viewmodel.MainViewModel

class NotificationItemAdapter(private val mainViewModel: MainViewModel) : ListAdapter<Notification, ViewHolder>(NotificationDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, position, itemCount, mainViewModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: NotificationItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(notification: Notification, position: Int, itemCount: Int, mainViewModel: MainViewModel) {
            binding.apply {
                notificationitem = notification
                mainviewmodel = mainViewModel

                when(notification.type){
                    1 -> {
                        //Read Only
                        imgIcon.setImageResource(R.drawable.ic_noti_plane)
                        btnMoreDetail.visibility = View.GONE
                    }
                    2 -> {
                        //Mile Wait
                        imgIcon.setImageResource(R.drawable.ic_noti_heart)
                        btnMoreDetail.visibility = View.VISIBLE
                    }
                    3 -> {
                        //Read Only
                        imgIcon.setImageResource(R.drawable.ic_noti_info)
                        btnMoreDetail.visibility = View.VISIBLE
                    }
                    4 -> {
                        //HTML Detail
                        imgIcon.setImageResource(R.drawable.ic_noti_info)
                        btnMoreDetail.visibility = View.VISIBLE
                    }
                    5 -> {
                        //HTML Detail
                        imgIcon.setImageResource(R.drawable.ic_noti_star)
                        btnMoreDetail.visibility = View.VISIBLE
                    }
                    6,7 -> {
                        //Order History
                        imgIcon.setImageResource(R.drawable.ic_noti_info)
                        btnMoreDetail.visibility = View.VISIBLE
                    }
                    else -> {
                        //Read Only
                        imgIcon.setImageResource(R.drawable.ic_noti_info)
                        btnMoreDetail.visibility = View.GONE
                    }
                }
                if(position == (itemCount - 1)) lineLastItem.visibility = View.VISIBLE
                else lineLastItem.visibility = View.GONE
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = NotificationItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class NotificationDiffCallback : DiffUtil.ItemCallback<Notification>() {
    override fun areItemsTheSame(oldItem: Notification, newItem: Notification): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Notification, newItem: Notification): Boolean {
        return oldItem == newItem
    }
}