package com.owndays.stapa.main.store.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.StoreFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.PRODUCT_STORE_MODE
import com.owndays.stapa.main.adapters.ProductItemAdapter
import com.owndays.stapa.main.adapters.SliderItemAdapter
import com.owndays.stapa.main.setting.viewmodel.AddressViewModel
import com.owndays.stapa.main.store.viewmodel.StoreViewModel
import com.owndays.stapa.main.view.*
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory
import timber.log.Timber
import java.text.DecimalFormat

class StoreFragment : Fragment(){

    private lateinit var viewDataBinding: StoreFragmentBinding
    private lateinit var sliderAdapter: SliderItemAdapter
    private lateinit var listNewAdapter: ProductItemAdapter
    private lateinit var listPickupAdapter: ProductItemAdapter
    private val storeViewModel by activityViewModels<StoreViewModel> { getViewModelFactory() }
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = StoreFragmentBinding.inflate(layoutInflater).apply {
            storeviewmodel = storeViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()

        storeViewModel.checkCart()
    }

    private fun setupView(){
        viewDataBinding.apply {
            val configuration = resources.configuration
            val screenWidthDp = configuration.screenWidthDp
            val sliderHeight = screenWidthDp * 0.6

            val scale = resources.displayMetrics.density

            var sliderParam = newsSliderPager.layoutParams
            sliderParam.height = (sliderHeight * scale + 0.5).toInt()
            newsSliderPager.layoutParams = sliderParam

            val formatter = DecimalFormat("#,###")
            textMileAmount.text = formatter.format(mainViewModel.mileItem.value?.balance?:0)

            btnAllNew.setOnClickListener {
                (activity as MainActivity)?.navigate(STORE_ALL_PRODUCT_FRAGMENT)
            }
            btnAllPickup.setOnClickListener {
                (activity as MainActivity)?.navigate(STORE_ALL_PICKUP_FRAGMENT)
            }
        }
    }

    private fun setupEvent(){
        storeViewModel.apply {
            //Special case, set badge cart
            cartItems.observe(viewLifecycleOwner, { _cartItemList ->
                if(!_cartItemList.isNullOrEmpty()) (activity as MainActivity)?.setBadge(BADGE_CART, 1)
            })
            onInitSlider.observe(viewLifecycleOwner, EventObserver { _sliderList ->
                sliderAdapter = SliderItemAdapter()
                sliderAdapter.updateDataItems(_sliderList)
                viewDataBinding.apply {
                    newsSliderPager.adapter = sliderAdapter
                    TabLayoutMediator(layoutTab, newsSliderPager) { _, _ -> }.attach()

                    //Add tab padding >> could do layer-list but transparent area will be clickable
                    val sideMargin = (resources.getDimension(R.dimen.icon_size_semismall) / resources.displayMetrics.density).toInt()
                    (layoutTab.getChildAt(0) as ViewGroup)?.children.forEach { _tab ->
                        val newMarginParam  = (_tab.layoutParams as ViewGroup.MarginLayoutParams)
                        newMarginParam?.setMargins(sideMargin, 0 , sideMargin, 0)
                        _tab.requestLayout()
                    }
                    layoutTab.requestLayout()

                    autoPlay(newsSliderPager)
                }
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun setupAdapter(){
        viewDataBinding.apply {
            listNewAdapter = ProductItemAdapter(PRODUCT_STORE_MODE,
                { _product ->
                    storeViewModel.loadProductDetail(_product)
                    (activity as MainActivity)?.navigate(PRODUCT_DETAIL_FRAGMENT)
                }, {}, {})
            productNewListRecyclerView.adapter = listNewAdapter
            listPickupAdapter = ProductItemAdapter(PRODUCT_STORE_MODE,
                { _product ->
                    storeViewModel.loadProductDetail(_product)
                    (activity as MainActivity)?.navigate(PRODUCT_DETAIL_FRAGMENT)
                }, {}, {})
            productPickupListRecyclerView.adapter = listPickupAdapter
        }
    }

    private fun autoPlay(viewPager: ViewPager2){
        var currentCount = viewPager.currentItem
        viewPager.postDelayed({run{
            try{
                if(viewPager.adapter != null && viewPager.adapter?.itemCount?:0 > 0){
                    if(currentCount >= ((viewPager.adapter?.itemCount?:0) - 1))
                        currentCount = 0
                    else currentCount++
                    viewPager.currentItem = currentCount
                    autoPlay(viewPager)
                }
            }catch (e: Exception){
                Timber.e(e.message)
            }
        } }, 4000)
    }
}