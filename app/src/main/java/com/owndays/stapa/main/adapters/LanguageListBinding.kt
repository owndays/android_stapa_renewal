package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Language

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Language>?) {
    items?.let {
        (listView.adapter as LanguageItemAdapter).submitList(items)
    }
}