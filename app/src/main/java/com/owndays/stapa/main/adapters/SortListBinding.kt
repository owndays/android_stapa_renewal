package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Sort

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Sort>?) {
    items?.let {
        (listView.adapter as SortItemAdapter).submitList(items)
    }
}