package com.owndays.stapa.main.mile.getmile.idea.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.owndays.stapa.BaseViewModel
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.Event
import com.owndays.stapa.data.model.*
import com.owndays.stapa.data.source.remote.model.request.CommentRequest
import com.owndays.stapa.data.source.remote.model.request.IdeaRequest
import com.owndays.stapa.data.source.repository.IdeaRepository
import com.owndays.stapa.data.source.repository.UserRepository
import com.owndays.stapa.main.adapters.OTHER_IDEA
import com.owndays.stapa.main.adapters.USER_IDEA
import kotlinx.coroutines.launch
import java.text.DecimalFormat

class IdeaViewModel(
    private val ideaRepository: IdeaRepository,
    private val userRepository: UserRepository
) : BaseViewModel(){

    private val _dataLoading = MutableLiveData<Boolean>().apply { value = false }
    val dataLoading : LiveData<Boolean> = _dataLoading

    private val _categoryItems = MutableLiveData<List<Category>>().apply { value = emptyList() }
    val categoryItems : LiveData<List<Category>> = _categoryItems

    private val _filterCategoryItems = MutableLiveData<List<Category>>().apply { value = emptyList() }
    val filterCategoryItems : LiveData<List<Category>> = _filterCategoryItems

    private val _selectedCategoryItem = MutableLiveData<Category>()
    val selectedCategoryItem : LiveData<Category> = _selectedCategoryItem

    private val _filterCategoryItem = MutableLiveData<Category>()
    val filterCategoryItem : LiveData<Category> = _filterCategoryItem

    private val _titleMessage = MutableLiveData<String>().apply { value = "" }
    val titleMessage : LiveData<String> = _titleMessage

    private val _whatMessage = MutableLiveData<String>().apply { value = "" }
    val whatMessage : LiveData<String> = _whatMessage

    private val _whyMessage = MutableLiveData<String>().apply { value = "" }
    val whyMessage : LiveData<String> = _whyMessage

    private val _howMessage = MutableLiveData<String>().apply { value = "" }
    val howMessage : LiveData<String> = _howMessage

    private val _messageCount = MutableLiveData<String>().apply { value = "0" }
    val messageCount : LiveData<String> = _messageCount

    private val _documentItems = MutableLiveData<List<Document>>().apply { value = emptyList() }
    val documentItems : LiveData<List<Document>> = _documentItems

    private val _isPostIdeaReady = MutableLiveData<Boolean>().apply { value = false }
    val isPostIdeaReady : LiveData<Boolean> = _isPostIdeaReady

    private val _postCompleteMile = MutableLiveData<String>().apply { value = "0" }
    val postCompleteMile : LiveData<String> = _postCompleteMile

    private val _ideaItems = MutableLiveData<List<Idea>>().apply { value = emptyList() }
    val ideaItems : LiveData<List<Idea>> = _ideaItems

    private val _ideaItem = MutableLiveData<Idea>()
    val ideaItem : LiveData<Idea> = _ideaItem

    private val _isEditing = MutableLiveData<Boolean>().apply { value = false }
    val isEditing : LiveData<Boolean> = _isEditing

    val searchTextShow = MutableLiveData<String>().apply { value = "" }
    private val _searchText = MutableLiveData<String>().apply { value = "" }

    val commentText = MutableLiveData<String>().apply { value = "" }

    //Event
    private val _onEmergencyRefreshIdeaList = MutableLiveData<Event<Unit>>()
    val onEmergencyRefreshIdeaList: LiveData<Event<Unit>> = _onEmergencyRefreshIdeaList

    private val _onRefreshIdeaInfo = MutableLiveData<Event<Idea>>()
    val onRefreshIdeaInfo: LiveData<Event<Idea>> = _onRefreshIdeaInfo

    private val _onDocumentDelete = MutableLiveData<Event<String>>()
    val onDocumentDelete: LiveData<Event<String>> = _onDocumentDelete

    private val _onPostIdeaComplete = MutableLiveData<Event<Unit>>()
    val onPostIdeaComplete: LiveData<Event<Unit>> = _onPostIdeaComplete

    private val _onCommentPostComplete = MutableLiveData<Event<List<Comment>>>()
    val onCommentPostComplete: LiveData<Event<List<Comment>>> = _onCommentPostComplete

    private val _onCommentDeleteComplete = MutableLiveData<Event<List<Comment>>>()
    val onCommentDeleteComplete: LiveData<Event<List<Comment>>> = _onCommentDeleteComplete

    private val _onCommentRefresh = MutableLiveData<Event<Unit>>()
    val onCommentRefresh: LiveData<Event<Unit>> = _onCommentRefresh

    private val _onCheckLanguage = MutableLiveData<Event<String>>()
    val onCheckLanguage: LiveData<Event<String>> = _onCheckLanguage

    private val _onPopupApiError = MutableLiveData<Event<String>>()
    val onPopupApiError: LiveData<Event<String>> = _onPopupApiError

    private val _onPopupError = MutableLiveData<Event<Int>>()
    val onPopupError: LiveData<Event<Int>> = _onPopupError

    private var accessToken = ""
    private var page = 1
    private var perPage = 10
    private var flagEndPage = false

    init {
        accessToken = ServiceLocator.getUserToken()
    }

    fun checkCategory(context: Context){
        if(_categoryItems.value?.isEmpty() == true) getCategoryList(context)
    }

    fun clearValue(){
        _selectedCategoryItem.value = null
        _titleMessage.value = ""
        _whatMessage.value = ""
        _whyMessage.value = ""
        _howMessage.value = ""
        _messageCount.value = "0"
        _documentItems.value = emptyList()
    }

    fun setCountNo(count: Int){
        _messageCount.value = (count/2).toString()
        validatePostIdea()
    }

    fun postIdea(){
        processPostIdea((_selectedCategoryItem.value?.id?:0).toString(),
            _titleMessage.value?:"",
            _whatMessage.value?:"",
            _whyMessage.value?:"",
            _howMessage.value?:"",
            _documentItems.value?: emptyList())
    }

    fun setCategory(category: Category?){
        if(category != null) _selectedCategoryItem.value = category
        validatePostIdea()
    }

    fun setFilterCategory(category: Category?, mode: String){
        if(category != null) _filterCategoryItem.value = category
        filterKeyword(mode)
    }

    fun setTitle(title: String){
        _titleMessage.value = title
    }

    fun setWhatMessage(whatMessage: String){
        _whatMessage.value = whatMessage
    }

    fun setWhyMessage(whyMessage: String){
        _whyMessage.value = whyMessage
    }

    fun setHowMessage(howMessage: String){
        _howMessage.value = howMessage
    }

    fun addDocument(document: Document){
        var tempDocumentArray = ArrayList<Document>(_documentItems.value?: emptyList())
        tempDocumentArray.add(document)
        _documentItems.value = tempDocumentArray
    }

    fun removeDocument(document: Document){
        var tempDocumentArray = ArrayList<Document>()
        _documentItems.value?.forEach { _document ->
            if(_document != document)
                tempDocumentArray.add(_document)
        }
        _documentItems.value = tempDocumentArray
        _onDocumentDelete.value = Event(document.base64?:"")
    }

    fun clearSearchKeyword(){
        searchTextShow.value = ""
        _searchText.value = ""
    }

    fun setTextKeyword(mode: String){
        _searchText.value = searchTextShow.value
        if(mode == USER_IDEA) refreshIdeaPostHistory()
        else if(mode == OTHER_IDEA) refreshIdeaOtherHistory()
    }

    fun refreshIdeaPostHistory(){
        page = 1
        flagEndPage = false
        getIdeaPostHistory(
            page.toString(),
            perPage.toString(),
            _searchText.value?:"",
            if(_selectedCategoryItem.value != null) (_filterCategoryItem.value?.id?:0).toString() else "")
    }

    fun loadNextHistoryPaging(){
        if(!flagEndPage){
            page++

            getIdeaPostHistory(
                page.toString(),
                perPage.toString(),
                _searchText.value?:"",
                if(_selectedCategoryItem.value != null) (_filterCategoryItem.value?.id?:0).toString() else "")
        }
    }

    fun refreshIdeaOtherHistory(){
        page = 1
        flagEndPage = false
        getIdeaOther(
            page.toString(),
            perPage.toString(),
            _searchText.value?:"",
            if(_selectedCategoryItem.value != null) (_filterCategoryItem.value?.id?:0).toString() else "")
    }

    fun loadNextIdeaOtherPaging(){
        if(!flagEndPage){
            page++

            getIdeaOther(
                page.toString(),
                perPage.toString(),
                _searchText.value?:"",
                if(_selectedCategoryItem.value != null) (_filterCategoryItem.value?.id?:0).toString() else "")
        }
    }

    fun toggleLike(idea: Idea, name: String){
        processLikePost(idea.id.toString())
        if(idea.isLike == true){
            //add Like Count
            var tempLike = ArrayList<Like>(idea.like?: emptyList())
            tempLike.add(Like(0, name))
            idea.like = tempLike
        }else if(idea.isLike == false){
            //remove Like Count
            var tempLike = ArrayList<Like>()
            idea.like?.forEach { _like ->
                if(_like.name != name) tempLike.add(_like)
            }
            idea.like = tempLike
        }
        if(_ideaItem.value != null) _onRefreshIdeaInfo.value = Event(_ideaItem.value!!)
        _onEmergencyRefreshIdeaList.value = Event(Unit)
    }

    fun setIdeaItemDetail(idea: Idea?){
        if(idea != null) _ideaItem.value = idea
    }

    fun commentPost(idea: Idea, comment: String){
        if(idea!= null && comment.trim().isNotEmpty()) processPostComment(idea, comment)
    }

    fun commentDelete(commentId: Int){
        _ideaItem.value?.comments?.forEach { _comment ->
            if(_comment.id == commentId){
                if(_ideaItem.value != null) processDeleteComment(_ideaItem.value!!, (commentId.toString()))
            }
        }
    }

    fun commentToggleEditMode(commentId: Int){
        _ideaItem.value?.comments?.forEach { _comment ->
            if(_comment.id == commentId){
                _comment.inEditMode = !_comment.inEditMode
                _isEditing.value = _comment.inEditMode
                _onCommentRefresh.value = Event(Unit)
            }
        }
    }

    fun commentEdit(commentId: Int, newComment: String){
        _ideaItem.value?.comments?.forEach { _comment ->
            if(_comment.id == commentId){
                if(ideaItem.value != null) processEditComment(_ideaItem.value!!, _comment, newComment)
            }
        }
    }

    fun filterKeyword(mode: String){
        if(mode == USER_IDEA){
            getIdeaPostHistory(
                page.toString(),
                perPage.toString(),
                _searchText.value?:"",
                if(_filterCategoryItem.value != null) (_filterCategoryItem.value?.id?:0).toString() else "")
        }else if(mode == OTHER_IDEA){
            getIdeaOther(
                page.toString(),
                perPage.toString(),
                _searchText.value?:"",
                if(_filterCategoryItem.value != null) (_filterCategoryItem.value?.id?:0).toString() else "")
        }
    }

    fun clearCommentText(){
        commentText.value = ""
    }

    private fun validatePostIdea(){
        _isPostIdeaReady.value = ( _selectedCategoryItem.value != null
                && !_titleMessage.value.isNullOrEmpty()
                && !_whatMessage.value.isNullOrEmpty()
                && !_whyMessage.value.isNullOrEmpty()
                && !_howMessage.value.isNullOrEmpty()
                && (_messageCount.value?:"0").toInt() >= 500)
    }

    private fun getCategoryList(context: Context){
        _dataLoading.value = true
        viewModelScope.launch {
            ideaRepository.getIdeaCategoryList(accessToken, ServiceLocator.getLanguage(),
                { _categoryList ->
                    _categoryItems.value = _categoryList
                    var tempFilterCategoryList = ArrayList<Category>()
                    tempFilterCategoryList.add(Category(0,context.getString(R.string.store_all_item_category0)))
                    _categoryList.forEach { _category ->
                        tempFilterCategoryList.add(_category)
                    }
                    _filterCategoryItems.value = tempFilterCategoryList
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processPostIdea(categoryId: String, title: String, what: String, why: String, how: String, documents: List<Document>){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.postIdea(accessToken, ServiceLocator.getLanguage(), IdeaRequest(categoryId, title, what, why, how, documents),
                {   _ideaPostResponse ->
                    val formatter = DecimalFormat("#,###")
                    _postCompleteMile.value = formatter.format(_ideaPostResponse.mile?:"0")
                    _dataLoading.value = false
                    _onPostIdeaComplete.value = Event(Unit)
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getIdeaPostHistory(_page: String, _perPage: String, keyword: String, categoryId: String){
        _dataLoading.value = true
        viewModelScope.launch {
            ideaRepository.getUserIdeaList(accessToken, ServiceLocator.getLanguage(), _page, _perPage,"1", keyword, categoryId,
                {  _ideaListResponse ->
                    if(_page == "1") _ideaItems.value = _ideaListResponse.ideas
                    else {
                        if (_ideaListResponse.ideas.isNullOrEmpty()) flagEndPage = true
                        else {
                            var tempIdeaArray =
                                ArrayList<Idea>(_ideaItems.value ?: emptyList())
                            _ideaListResponse.ideas?.forEach { _idea ->
                                tempIdeaArray.add(_idea)
                            }
                            _ideaItems.value = tempIdeaArray
                        }
                    }
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getIdeaOther(_page: String, _perPage: String, keyword: String, categoryId: String){
        _dataLoading.value = true
        viewModelScope.launch {
            ideaRepository.getIdeaList(accessToken, ServiceLocator.getLanguage(), _page, _perPage, keyword, categoryId,
                { _ideaListResponse ->
                    if(_page == "1") _ideaItems.value = _ideaListResponse.ideas
                    else {
                        if (_ideaListResponse.ideas.isNullOrEmpty()) flagEndPage = true
                        else {
                            var tempIdeaArray =
                                ArrayList<Idea>(_ideaItems.value ?: emptyList())
                            _ideaListResponse.ideas?.forEach { _idea ->
                                tempIdeaArray.add(_idea)
                            }
                            _ideaItems.value = tempIdeaArray
                        }
                    }
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processLikePost(ideaId: String){
        viewModelScope.launch {
            userRepository.likeIdea(accessToken, ServiceLocator.getLanguage(), ideaId,
                {

                },
                {

                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processPostComment(idea: Idea, comment: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.postComment(accessToken, ServiceLocator.getLanguage(), (idea.id?:0).toString(), CommentRequest(comment),
                { _comment ->
                    var tempCommentArray = ArrayList<Comment>(idea.comments?: emptyList())
                    tempCommentArray.add(_comment)
                    idea.comments = tempCommentArray
                    _onCommentPostComplete.value = Event(idea.comments?: emptyList())
                    if(_ideaItem.value != null) _onRefreshIdeaInfo.value = Event(_ideaItem.value!!)
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processDeleteComment(idea: Idea, commentId: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.deleteComment(accessToken, ServiceLocator.getLanguage(), (idea.id?:0).toString(), commentId,
                {
                    var tempCommentArray = ArrayList<Comment>()
                    idea.comments?.forEach { _comment ->
                        if((_comment.id?:0).toString() != commentId) tempCommentArray.add(_comment)
                    }
                    idea.comments = tempCommentArray
                    _onCommentDeleteComplete.value = Event(idea.comments?: emptyList())
                    if(_ideaItem.value != null) _onRefreshIdeaInfo.value = Event(_ideaItem.value!!)
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processEditComment(idea: Idea, comment: Comment, newComment: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.editComment(accessToken, ServiceLocator.getLanguage(), (idea.id?:0).toString(), (comment.id?:0).toString(), CommentRequest(newComment),
                { _comment ->
                    comment.text = _comment.text
                    comment.inEditMode = false
                    _dataLoading.value = false
                    _isEditing.value = false
                    _onCommentRefresh.value = Event(Unit)
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }
}