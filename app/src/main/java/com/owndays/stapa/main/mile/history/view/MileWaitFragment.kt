package com.owndays.stapa.main.mile.history.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.databinding.MileWaitFragmentBinding
import com.owndays.stapa.main.adapters.MILEWAIT_ITEM
import com.owndays.stapa.main.adapters.MileDetailItemAdapter
import com.owndays.stapa.main.mile.viewmodel.MileViewModel
import com.owndays.stapa.utils.getViewModelFactory

class MileWaitFragment : Fragment(){

    private lateinit var viewDataBinding: MileWaitFragmentBinding
    private lateinit var listAdapter: MileDetailItemAdapter
    private val mileViewModel by activityViewModels<MileViewModel> { getViewModelFactory() }

    companion object{
        fun newInstance() : MileWaitFragment {
            return MileWaitFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = MileWaitFragmentBinding.inflate(layoutInflater).apply {
            mileviewmodel = mileViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()
    }

    private fun setupView(){

    }

    private fun setupEvent(){

    }

    private fun setupAdapter(){
        listAdapter = MileDetailItemAdapter(MILEWAIT_ITEM, mileViewModel)
        viewDataBinding.mileWaitRecyclerView.adapter = listAdapter
    }
}