package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Refine

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Refine>?) {
    items?.let {
        (listView.adapter as RefineItemAdapter).submitList(items)
    }
}