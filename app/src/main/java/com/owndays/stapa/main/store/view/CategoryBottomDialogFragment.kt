package com.owndays.stapa.main.store.view

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.data.model.Category
import com.owndays.stapa.databinding.CategoryBottomDialogFragmentBinding
import com.owndays.stapa.main.adapters.CategoryItemAdapter
import com.owndays.stapa.main.adapters.MODE_PRODUCT_CATEGORY
import com.owndays.stapa.main.adapters.viewmodel.CategoryViewModel
import com.owndays.stapa.utils.getViewModelFactory

const val CATEGORY_BOTTOMDIALOG_FRAGMENT = "CategoryBottomDialogFragment"

class CategoryBottomDialogFragment(private val onSelectCategory: (category: Category) -> Unit) : BottomSheetDialogFragment(){

    private lateinit var viewDataBinding : CategoryBottomDialogFragmentBinding
    private lateinit var listAdapter: CategoryItemAdapter
    private val categoryViewModel by activityViewModels<CategoryViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = CategoryBottomDialogFragmentBinding.inflate(layoutInflater).apply {
            categoryviewmodel = categoryViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        //expanded bottom sheet just in case too many item
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener { dialog ->
                (dialog as BottomSheetDialog).findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)?.apply {
                    BottomSheetBehavior.from(this).state = BottomSheetBehavior.STATE_EXPANDED
                }
            }
        }
    }

    private fun setupView(){
        viewDataBinding.apply {
            btnClose.setOnClickListener { dismiss() }
        }
    }

    private fun setupEvent(){
        categoryViewModel.onCategorySelected.observe(viewLifecycleOwner, EventObserver { _category ->
            onSelectCategory.invoke(_category)
            dismiss()
        })
    }

    private fun setupAdapter(){
        listAdapter = CategoryItemAdapter(MODE_PRODUCT_CATEGORY, categoryViewModel)
        viewDataBinding.categoryListRecyclerView.adapter = listAdapter
    }
}