package com.owndays.stapa.main.setting.view

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.DigitalidImageDialogFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.mile.getmile.idea.view.GET_GALLERY_REQUEST_CODE
import com.owndays.stapa.main.mile.getmile.idea.view.GET_PHOTO_REQUEST_CODE
import com.owndays.stapa.main.mile.getmile.idea.view.REQ_ACCESS_FILE_CODE
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

const val DIGITALID_PHOTO_DIALOG_FRAGMENT = "DigitalIdImageDialogFragment"

class DigitalIdImageDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: DigitalidImageDialogFragmentBinding
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }

    private var image64 = ""
    private var photoFileName = ""
    private var photoFilePath = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = DigitalidImageDialogFragmentBinding.inflate(layoutInflater).apply {
            mainviewmodel = mainViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var allGranted = true
        grantResults.forEach { if(it == PackageManager.PERMISSION_DENIED) allGranted = false }
        if(allGranted) viewDataBinding.imageDigitalId.performClick()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            when(requestCode){
                GET_GALLERY_REQUEST_CODE -> {
                    val byteFile = requireActivity().contentResolver.openInputStream(data?.data!!)?.readBytes()
                    image64 = Base64.encodeToString(byteFile, Base64.NO_WRAP)
                    Glide.with(viewDataBinding.root)
                        .load(BitmapFactory.decodeByteArray(byteFile,0,byteFile?.size?:0))
                        .circleCrop()
                        .placeholder(R.drawable.img_placeholder_profile)
                        .into(viewDataBinding.imageDigitalId)
                }
                GET_PHOTO_REQUEST_CODE -> {
                    val byteFile = requireActivity().contentResolver.openInputStream(Uri.fromFile(File(photoFilePath)))?.readBytes()
                    image64 = Base64.encodeToString(byteFile, Base64.NO_WRAP)
                    Glide.with(viewDataBinding.root)
                        .load(BitmapFactory.decodeByteArray(byteFile,0,byteFile?.size?:0))
                        .circleCrop()
                        .placeholder(R.drawable.img_placeholder_profile)
                        .into(viewDataBinding.imageDigitalId)
                }
            }
        }else{
            val errorDialog = DefaultOkDialog("", getString(R.string.err_msg_010)) {}
            errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
        }
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            Glide.with(root)
                .load(mainViewModel.profileDigitalUrl.value?:"")
                .placeholder(R.drawable.img_placeholder_profile)
                .into(imageDigitalId)

            btnBack.setOnClickListener { dismiss() }
            btnSave.setOnClickListener {
                if(image64.isNotEmpty()){
                    val bitmap = viewDataBinding.imageDigitalId.drawable.toBitmap()
                    val stream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                    val byteData = stream.toByteArray()
                    mainViewModel.updateDigitalId(Base64.encodeToString(byteData, Base64.NO_WRAP))
                }
                else dismiss()
            }
            if(mainViewModel.userItem.value?.imageUrl.isNullOrEmpty()){
                imageDigitalId.setOnClickListener {
                    if (ContextCompat.checkSelfPermission(
                            requireContext(),
                            Manifest.permission.CAMERA
                        ) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(
                            requireContext(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ) == PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(
                            requireContext(),
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        val photoBottomDialogFragment = PhotoBottomDialogFragment(
                            { openGallery() },
                            { openCamera() })
                        photoBottomDialogFragment.show(childFragmentManager, PHOTO_BOTTOMDIALOG_FRAGMENT)
                    } else {
                        requestPermissions(
                            arrayOf(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                            ),
                            REQ_ACCESS_FILE_CODE
                        )
                    }
                }
            }else btnSave.visibility = View.GONE
        }
    }

    private fun setupEvent(){
        mainViewModel.apply {
            onSaveProfileComplete.observe(viewLifecycleOwner, EventObserver {
                dismiss()
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun openGallery(){
        Intent(Intent.ACTION_PICK).also { galleryIntent ->
            galleryIntent.type = "*/*"
            galleryIntent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/png", "image/jpeg"))
            galleryIntent.resolveActivity(requireActivity().packageManager)?.also {
                startActivityForResult(galleryIntent, GET_GALLERY_REQUEST_CODE)
            }
        }
    }

    private fun openCamera(){
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            photoFileName = "IMG_+ ${
                SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(
                    Date()
                )}"
            val photoFile = File.createTempFile(
                photoFileName,
                ".jpg",
                requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            )
            photoFilePath = photoFile.absolutePath
            val photoURI = FileProvider.getUriForFile(requireContext(), "${requireContext().packageName}.provider" , photoFile)
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                startActivityForResult(takePictureIntent, GET_PHOTO_REQUEST_CODE)
            }
        }
    }
}