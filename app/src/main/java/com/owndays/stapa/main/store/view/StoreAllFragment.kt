package com.owndays.stapa.main.store.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.data.model.REFINE_ALL_PRODUCT
import com.owndays.stapa.data.model.REFINE_PICKUP_PRODUCT
import com.owndays.stapa.databinding.StoreAllFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.PRODUCT_LIST_MODE
import com.owndays.stapa.main.adapters.ProductItemAdapter
import com.owndays.stapa.main.adapters.viewmodel.CategoryViewModel
import com.owndays.stapa.main.store.viewmodel.StoreViewModel
import com.owndays.stapa.main.view.BADGE_CART
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.main.view.PRODUCT_DETAIL_FRAGMENT
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory
import java.text.DecimalFormat

const val TARGET_STORE_MODE = "target_store_mode"
const val ALL_PRODUCT = 1
const val ALL_PICKUP = 2

class StoreAllFragment : Fragment(){

    private lateinit var viewDataBinding: StoreAllFragmentBinding
    private lateinit var listAdapter: ProductItemAdapter
    private val storeViewModel by activityViewModels<StoreViewModel> { getViewModelFactory() }
    private val categoryViewModel by activityViewModels<CategoryViewModel> { getViewModelFactory() }
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = StoreAllFragmentBinding.inflate(layoutInflater).apply {
            storeviewmodel = storeViewModel
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()

        storeViewModel.run {
            initStoreAll(requireContext())
            checkCart()
        }

        val mode = arguments?.getInt(TARGET_STORE_MODE)?:0
        when (mode) {
            ALL_PRODUCT -> storeViewModel.setLoadRefine(REFINE_ALL_PRODUCT)
            ALL_PICKUP -> storeViewModel.setLoadRefine(REFINE_PICKUP_PRODUCT)
            else -> storeViewModel.setLoadRefine(REFINE_ALL_PRODUCT)
        }
    }

    private fun setupView(){
        viewDataBinding.apply {
            val formatter = DecimalFormat("#,###")
            textMileAmount.text = formatter.format(mainViewModel.mileItem.value?.balance?:0)

            refreshLayout.apply {
                setOnRefreshListener {
                    isRefreshing = false
                    storeViewModel.refreshStoreAllItem()
                }
            }
            nestScrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, _, scrollY, _, _ ->
                if(scrollY == (v!!.getChildAt(0)!!.measuredHeight) - v!!.height){
                    storeViewModel.loadMoreProduct()
                }
            })
        }
    }

    private fun setupEvent(){
        storeViewModel.apply {
            //Special case, set badge cart
            cartItems.observe(viewLifecycleOwner, { _cartItemList ->
                if(!_cartItemList.isNullOrEmpty()) (activity as MainActivity)?.setBadge(BADGE_CART, 1)
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun setupAdapter(){
        listAdapter = ProductItemAdapter(PRODUCT_LIST_MODE,
            { _product ->
                storeViewModel.loadProductDetail(_product)
                (activity as MainActivity)?.navigate(PRODUCT_DETAIL_FRAGMENT)
            }, {}, {})
        viewDataBinding.productListRecyclerView.adapter = listAdapter
    }

    private val onClickListener = View.OnClickListener {
        viewDataBinding.apply {
            when(it.id){
                R.id.btnRefine -> {
                    val refineBottomDialogFragment = RefineBottomDialogFragment()
                    refineBottomDialogFragment.show(childFragmentManager, REFINE_BOTTOMDIALOG_FRAGMENT)
                }
                R.id.btnCategory -> {
                    categoryViewModel.setCategoryItem(storeViewModel.categoryItem.value)
                    categoryViewModel.setCategoryList(storeViewModel.categoryItems.value?: emptyList())
                    val categoryBottomDialogFragment = CategoryBottomDialogFragment { _category ->
                        storeViewModel.selectCategory(_category)
                    }
                    categoryBottomDialogFragment.show(childFragmentManager, CATEGORY_BOTTOMDIALOG_FRAGMENT)
                }
                R.id.btnSort -> {
                    val sortBottomDialogFragment = SortBottomDialogFragment()
                    sortBottomDialogFragment.show(childFragmentManager, SORT_BOTTOMDIALOG_FRAGMENT)
                }
            }
        }
    }
}