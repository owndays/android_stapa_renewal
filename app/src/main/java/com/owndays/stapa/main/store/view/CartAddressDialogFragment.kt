package com.owndays.stapa.main.store.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.databinding.CartAddressDialogFragmentBinding
import com.owndays.stapa.main.adapters.AddressItemAdapter
import com.owndays.stapa.main.adapters.MODE_STORE_ADDRESS
import com.owndays.stapa.main.setting.view.ADDRESS_ADD_MODE
import com.owndays.stapa.main.setting.view.ADDRESS_DIALOG_FRAGMENT
import com.owndays.stapa.main.setting.view.AddressAddEditDialogFragment
import com.owndays.stapa.main.setting.view.AddressDialogFragment
import com.owndays.stapa.main.setting.viewmodel.AddressViewModel
import com.owndays.stapa.utils.getViewModelFactory

const val CART_ADDRESS_DIALOG_FRAGMENT = "CartAddressDialogFragment"

class CartAddressDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: CartAddressDialogFragmentBinding
    private lateinit var listAdapter: AddressItemAdapter
    private val addressViewModel by activityViewModels<AddressViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = CartAddressDialogFragmentBinding.inflate(layoutInflater).apply {
            addressviewmodel = addressViewModel
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun setupEvent(){
        //Special case, refresh selected address
        addressViewModel.addressItem.observe(viewLifecycleOwner, {
            listAdapter.notifyDataSetChanged()
        })
    }

    private fun setupAdapter(){
        listAdapter = AddressItemAdapter(MODE_STORE_ADDRESS, addressViewModel, {}, {})
        viewDataBinding.addressListRecyclerView.adapter = listAdapter
    }

    private val onClickListener = View.OnClickListener {
        viewDataBinding.apply {
            when(it.id){
                R.id.btnBack -> {
                    dismiss()
                }
                R.id.btnEdit -> {
                    val addressDialogFragment = AddressDialogFragment(MODE_STORE_ADDRESS)
                    addressDialogFragment.show(childFragmentManager, ADDRESS_DIALOG_FRAGMENT)
                }
                R.id.btnAdd -> {
                    val addressAddEditDialogFragment = AddressAddEditDialogFragment(ADDRESS_ADD_MODE)
                    addressAddEditDialogFragment.show(childFragmentManager, ADDRESS_ADD_MODE)
                }
            }
        }
    }
}