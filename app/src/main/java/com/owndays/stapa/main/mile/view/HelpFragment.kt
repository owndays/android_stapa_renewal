package com.owndays.stapa.main.mile.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.owndays.stapa.BuildConfig
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.databinding.HelpFragmentBinding
import java.util.*


class HelpFragment : Fragment(){

    private lateinit var viewBinding: HelpFragmentBinding

    private val HELP_URL = "about-miles"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = HelpFragmentBinding.inflate(layoutInflater)
        return viewBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    private fun setupView(){
        viewBinding.apply {
            var header = HashMap<String, String>().also { _header ->
                _header["Authorization"] = ServiceLocator.getUserToken()
            }
            webView.settings.javaScriptEnabled = true
            webView.loadUrl(BuildConfig.DOMAIN + BuildConfig.API_PATH + HELP_URL, header)
        }
    }
}