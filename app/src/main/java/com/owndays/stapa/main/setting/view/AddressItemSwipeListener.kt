package com.owndays.stapa.main.setting.view

interface AddressItemSwipeListener {

    fun onItemSwipe(position: Int, direction: Int)
}