package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.R
import com.owndays.stapa.databinding.ViewholderSliderTextBinding
import com.owndays.stapa.main.adapters.MileTextSliderAdapter.ViewHolder

class MileTextSliderAdapter : RecyclerView.Adapter<ViewHolder>() {

    var dataItems: MutableList<String> = mutableListOf()

    fun updateDataItems(list: List<String>) {
        when (dataItems.size) {
            0 -> {
                dataItems.clear()
                dataItems.addAll(list)
                notifyDataSetChanged()
            }
            else -> {
                val diffCallback = SliderDiffCallback(dataItems, list)
                DiffUtil.calculateDiff(diffCallback, true)
                    .dispatchUpdatesTo(this@MileTextSliderAdapter)
                dataItems.clear()
                dataItems.addAll(list)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ViewholderSliderTextBinding = DataBindingUtil.inflate(inflater, R.layout.viewholder_slider_text, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = dataItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = dataItems[position]
        holder.bind(item)
    }

    inner class ViewHolder(val binding: ViewholderSliderTextBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: String) {
            binding.text = item
            binding.executePendingBindings()
        }
    }

    class SliderDiffCallback (private val oldList: List<String>, private val newList: List<String>): DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size
        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldList[oldItemPosition]
            val newItem = newList[newItemPosition]
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldItem = oldList[oldItemPosition]
            val newItem = newList[newItemPosition]
            return oldItem == newItem
        }
    }
}