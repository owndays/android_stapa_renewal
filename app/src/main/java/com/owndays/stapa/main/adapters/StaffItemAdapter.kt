package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.owndays.stapa.R
import com.owndays.stapa.data.model.User
import com.owndays.stapa.databinding.StaffItemBinding
import com.owndays.stapa.main.adapters.StaffItemAdapter.ViewHolder
import com.owndays.stapa.main.mile.viewmodel.MileViewModel

class StaffItemAdapter(private val mileViewModel: MileViewModel) : ListAdapter<User, ViewHolder>(
    StaffDiffCallback()
){
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, mileViewModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: StaffItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(user: User, mileViewModel: MileViewModel) {
            binding.apply {
                staffitem = user
                mileviewmodel = mileViewModel

                Glide.with(root)
                    .load(user.imageUrl)
                    .circleCrop()
                    .placeholder(R.drawable.img_placeholder_profile)
                    .into(imgProfile)
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = StaffItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class StaffDiffCallback : DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem == newItem
    }
}