package com.owndays.stapa.main.digitalid.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.owndays.stapa.R
import com.owndays.stapa.databinding.DigitalidDialogFragmentBinding
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.getViewModelFactory

const val DIGITALID_DIALOG_FRAGMENT = "DigitalIdDialogFragment"

class DigitalIdDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: DigitalidDialogFragmentBinding
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = DigitalidDialogFragmentBinding.inflate(layoutInflater).apply {
            mainviewmodel = mainViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            Glide.with(root)
                .load(mainViewModel.digitalIdItem.value?.imageUrl?:"")
                .placeholder(R.drawable.img_placeholder_profile)
                .into(imgProfile)

            btnClose.setOnClickListener { dismiss() }
        }
    }
}