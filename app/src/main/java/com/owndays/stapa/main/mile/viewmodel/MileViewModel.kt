package com.owndays.stapa.main.mile.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.owndays.stapa.BaseViewModel
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.Event
import com.owndays.stapa.data.model.Mile
import com.owndays.stapa.data.model.MileDetail
import com.owndays.stapa.data.model.User
import com.owndays.stapa.data.source.remote.model.request.GiveMileRequest
import com.owndays.stapa.data.source.repository.StaffRepository
import com.owndays.stapa.data.source.repository.UserRepository
import com.owndays.stapa.main.adapters.EARN_HISTORY
import com.owndays.stapa.main.adapters.SPEND_HISTORY
import kotlinx.coroutines.launch
import java.text.DecimalFormat

class MileViewModel(
    private val userRepository: UserRepository,
    private val staffRepository: StaffRepository
) : BaseViewModel(){

    private val _dataLoading = MutableLiveData<Boolean>().apply { value = false }
    val dataLoading : LiveData<Boolean> = _dataLoading

    private val _userItem = MutableLiveData<User>()
    val userItem : LiveData<User> = _userItem

    private val _staffItem = MutableLiveData<User>()
    val staffItem : LiveData<User> = _staffItem

    private val _chosenStaffItem = MutableLiveData<User>()
    val chosenStaffItem : LiveData<User> = _chosenStaffItem

    private val _staffItems = MutableLiveData<List<User>>().apply { value = emptyList() }

    private val _staffItemsShow = MutableLiveData<List<User>>().apply { value = emptyList() }
    val staffItemsShow : LiveData<List<User>> = _staffItemsShow

    private val _mileItem = MutableLiveData<Mile>()
    val mileItem : LiveData<Mile> = _mileItem

    private val _mileWaitItems = MutableLiveData<List<MileDetail>>()
    val mileWaitItems : LiveData<List<MileDetail>> = _mileWaitItems

    private val _mileDetailList = MutableLiveData<List<MileDetail>>()
    val mileDetailList : LiveData<List<MileDetail>> = _mileDetailList

    private val _mileLeft = MutableLiveData<String>()
    val mileLeft : LiveData<String> = _mileLeft

    private val _giveMileCount = MutableLiveData<String>().apply { value = "" }
    val giveMileCount : LiveData<String> = _giveMileCount

    private val _giveMileMessage = MutableLiveData<String>().apply { value = "" }
    val giveMileMessage : LiveData<String> = _giveMileMessage

    private val _popupGiveMileCount = MutableLiveData<String>().apply { value = "1" }
    val popupGiveMileCount : LiveData<String> = _popupGiveMileCount

    //Event
    private val _onAcceptMileComplete = MutableLiveData<Event<String>>()
    val onAcceptMileComplete: LiveData<Event<String>> = _onAcceptMileComplete

    private val _onRejectMileConfirm = MutableLiveData<Event<String>>()
    val onRejectMileConfirm: LiveData<Event<String>> = _onRejectMileConfirm

    private val _onRejectMileComplete = MutableLiveData<Event<Unit>>()
    val onRejectMileComplete: LiveData<Event<Unit>> = _onRejectMileComplete

    private val _onGiveMileComplete = MutableLiveData<Event<Unit>>()
    val onGiveMileComplete: LiveData<Event<Unit>> = _onGiveMileComplete

    private val _onCheckLanguage = MutableLiveData<Event<String>>()
    val onCheckLanguage: LiveData<Event<String>> = _onCheckLanguage

    private val _onPopupApiError = MutableLiveData<Event<String>>()
    val onPopupApiError: LiveData<Event<String>> = _onPopupApiError

    private val _onPopupError = MutableLiveData<Event<Int>>()
    val onPopupError: LiveData<Event<Int>> = _onPopupError

    private var accessToken = ""
    private val formatter = DecimalFormat("#,###")

    init {
        accessToken = ServiceLocator.getUserToken()
    }

    fun refreshStaffList(){
        getStaffList()
    }

    fun setMileValue(mile: Mile?){
        if(mile != null) _mileItem.value = mile
        else getUserMile()
        _giveMileCount.value = "0"
        _staffItem.value = null
        _chosenStaffItem.value = null
    }

    fun giveMileValue(amount : Int){
        _giveMileCount.value = formatter.format(amount)
        _mileLeft.value = formatter.format((_mileItem.value?.balance?:0) - amount)
    }

    fun initMileAddDialog(){
        if(_giveMileCount.value.isNullOrEmpty()){
            _popupGiveMileCount.value = "1"
        }else _popupGiveMileCount.value = _giveMileCount.value?.dropLast(2)?:"1"
    }

    fun setMileAddDialog(mileAmountString: String){
        _giveMileCount.value = "${mileAmountString}00"
        _mileLeft.value = formatter.format((_mileItem.value?.balance?:0) - "${mileAmountString}00".replace(",","").toInt())
    }

    fun setMessageAddDialog(mileMessageString: String){
        _giveMileMessage.value = mileMessageString
    }

    fun validateGiveMile(){
        if(!_giveMileCount.value.isNullOrEmpty() || _giveMileCount.value?.replace(",","")?.toInt()?:0 > 0
            && _staffItem.value != null){
            val mileAmount = _giveMileCount.value?.replace(",","")?.toInt()?:0
            giveMile((_staffItem.value?.id?:0).toString(), mileAmount, _giveMileMessage.value?:"")
        }
        else _onPopupError.value = Event(R.string.err_msg_002)
    }

    fun chooseStaff(staff: User){
        _chosenStaffItem.value = staff
    }

    fun confirmSelectStaff(){
        _staffItem.value = _chosenStaffItem.value
    }

    fun filterName(searchString: String){
        _dataLoading.value = true
        var tempFilteredStaff = ArrayList<User>()
        _staffItems.value?.forEach { _staffUser ->
            if((_staffUser.name?:"").toLowerCase().contains(searchString.toLowerCase())) tempFilteredStaff.add(_staffUser)
        }
        _staffItemsShow.value = tempFilteredStaff
        _dataLoading.value = false
    }

    fun refreshMileHistory(){
        getMileHistory()
    }

    fun acceptMile(mileId: String, mileAmount: Int){
        if(!mileId.isNullOrEmpty())
            processAcceptMile(mileId, mileAmount)
        else _onPopupError.value = Event(R.string.err_msg_002)
    }

    fun rejectMile(mileId: String){
        if(!mileId.isNullOrEmpty())
            _onRejectMileConfirm.value = Event(mileId)
        else _onPopupError.value = Event(R.string.err_msg_002)
    }

    fun confirmRejectMile(mileId: String){
        processRejectMile(mileId)
    }

    fun loadMileDetail(mode: String, year: String, month: String){
        _mileDetailList.value = emptyList()
        if(mode == EARN_HISTORY)
            getMileEarnHistoryDetail(year, month)
        else if(mode == SPEND_HISTORY)
            getMileSpendHistoryDetail(year, month)
    }

    private fun getUserMile(){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.getUserMileHistoryList(accessToken, ServiceLocator.getLanguage(),
                { _mile ->
                    _mileItem.value = _mile
                    _dataLoading.value = false
                }, {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getStaffList(){
        _dataLoading.value = true
        viewModelScope.launch {
            staffRepository.getStaffList(accessToken, ServiceLocator.getLanguage(),
                { _staffList ->
                    _staffItems.value = _staffList
                    _staffItemsShow.value = _staffList
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun giveMile(staffId: String, mileAmount: Int, message: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.giveMile(accessToken, ServiceLocator.getLanguage(), GiveMileRequest(staffId, mileAmount, message),
                {
                    _dataLoading.value = false
                    _onGiveMileComplete.value = Event(Unit)
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getMileHistory(){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.getUserMileHistoryList(accessToken, ServiceLocator.getLanguage(),
                { _mile ->
                    _mileItem.value = null
                    _mileItem.value = _mile
                    getMileWait()
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getMileWait(){
        viewModelScope.launch {
            userRepository.getUserMileWaitingList(accessToken, ServiceLocator.getLanguage(),
                { _mileList ->
                    _mileWaitItems.value = emptyList()
                    _mileWaitItems.value = _mileList
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processAcceptMile(mileId: String, mileAmount: Int){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.acceptMile(accessToken, ServiceLocator.getLanguage(), mileId,
                {
                    val formatter = DecimalFormat("#,###")
                    var mileAmountString = if(mileAmount > 0) formatter.format(mileAmount) else "0"
                    _onAcceptMileComplete.value = Event(mileAmountString)
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processRejectMile(mileId: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.rejectMile(accessToken, ServiceLocator.getLanguage(), mileId,
                {
                    _onRejectMileComplete.value = Event(Unit)
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getMileEarnHistoryDetail(year: String, month: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.getUserMileEarnList(accessToken, ServiceLocator.getLanguage(), year, month,
                { _detailList ->
                    _mileDetailList.value = emptyList()
                    _mileDetailList.value = _detailList
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getMileSpendHistoryDetail(year: String, month: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.getUserMileSpendList(accessToken, ServiceLocator.getLanguage(), year, month,
                { _detailList ->
                    _mileDetailList.value = emptyList()
                    _mileDetailList.value = _detailList
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }
}