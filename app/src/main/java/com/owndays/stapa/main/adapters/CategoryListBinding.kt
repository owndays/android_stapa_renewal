package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Category

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Category>?) {
    items?.let {
        (listView.adapter as CategoryItemAdapter).submitList(items)
    }
}