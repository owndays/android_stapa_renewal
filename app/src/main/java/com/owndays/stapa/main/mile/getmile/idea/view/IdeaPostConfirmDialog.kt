package com.owndays.stapa.main.mile.getmile.idea.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.databinding.IdeaPostConfirmDialogBinding
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.utils.getViewModelFactory

const val IDEA_POST_CONFIRM_DIALOG = "IdeaPostConfirmDialog"

class IdeaPostConfirmDialog : AppCompatDialogFragment(){

    private lateinit var viewDataBinding : IdeaPostConfirmDialogBinding
    private val ideaViewModel by activityViewModels<IdeaViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = IdeaPostConfirmDialogBinding.inflate(layoutInflater)
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
    }

    private fun setupView() {
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            btnCancel.setOnClickListener {
                dismiss()
            }
            btnOk.setOnClickListener {
                ideaViewModel.postIdea()
                dismiss()
            }
        }
    }
}