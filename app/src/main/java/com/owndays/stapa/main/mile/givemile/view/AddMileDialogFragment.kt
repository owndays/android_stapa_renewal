package com.owndays.stapa.main.mile.givemile.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.databinding.AddmileDialogFragmentBinding
import com.owndays.stapa.main.mile.viewmodel.MileViewModel
import com.owndays.stapa.utils.getViewModelFactory
import java.text.DecimalFormat

const val ADDMILE_DIALOG_FRAGMENT = "AddMileDialogFragment"

class AddMileDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding : AddmileDialogFragmentBinding
    private val mileViewModel by activityViewModels<MileViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = AddmileDialogFragmentBinding.inflate(layoutInflater).apply {
            mileviewmodel = mileViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        mileViewModel.initMileAddDialog()
        setupView()

        (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .toggleSoftInput(InputMethodManager.SHOW_FORCED,0)
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        //number format for API  >= 21
        val formatter = DecimalFormat("###,###,###,#")
        var firstLength = 0
        viewDataBinding.apply {
            edtMileAmount.addTextChangedListener (object : TextWatcher{
                override fun afterTextChanged(s: Editable?) {
                    //check cursor position
                    var cursorPosition = edtMileAmount.selectionStart
                    //prevent infinite loop by remove watcher
                    edtMileAmount.removeTextChangedListener(this)
                    //count for re-position
                    val beforeLength = s.toString().length
                    //simplify for process
                    var string = s.toString().replace(",","")

                    if(string.isEmpty() || string == "0"){
                        //replace with "SHOULD-BE" value
                        s?.clear()
                        s?.append("1")
                    }else{
                        //new value calculation
                        val newString = formatter.format(("$string").toInt())
                        //replace with "SHOULD-BE" value
                        s?.clear()
                        s?.append(newString)
                    }
                    //count for cursor reposition
                    val afterLength = s?.length?:0
                    //calculate cursor "SHOULD-BE" position
                    when{
                        afterLength - beforeLength > 0 && firstLength < beforeLength -> { cursorPosition += 1 }
                        beforeLength - afterLength > 0 && cursorPosition > 0 -> { cursorPosition -= 1 }
                    }
                    //reposition cursor after replace text
                    edtMileAmount.setSelection(cursorPosition)
                    //re-attach watcher again
                    edtMileAmount.addTextChangedListener(this)
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    firstLength = s?.length?:0
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }
            })
            btnComplete.setOnClickListener {
                mileViewModel.setMileAddDialog(edtMileAmount.text.toString())
                (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                    .hideSoftInputFromWindow(view?.windowToken,0)
                dismiss()
            }
            btnClose.setOnClickListener {
                (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                    .hideSoftInputFromWindow(view?.windowToken,0)
                dismiss()
            }
        }
    }
}