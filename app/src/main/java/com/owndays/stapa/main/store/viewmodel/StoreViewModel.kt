package com.owndays.stapa.main.store.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.owndays.stapa.BaseViewModel
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.Event
import com.owndays.stapa.data.Result.Success
import com.owndays.stapa.data.model.*
import com.owndays.stapa.data.source.repository.OrderRepository
import com.owndays.stapa.data.source.repository.ShopRepository
import kotlinx.coroutines.launch

class StoreViewModel (
    private val orderRepository: OrderRepository,
    private val shopRepository: ShopRepository
) : BaseViewModel(){

    private val _dataLoading = MutableLiveData<Boolean>().apply { value = false }
    val dataLoading : LiveData<Boolean> = _dataLoading

    private val _pageLoading = MutableLiveData<Boolean>().apply { value = false }
    val pageLoading : LiveData<Boolean> = _pageLoading

    private val _productTopItems = MutableLiveData<List<Product>>().apply { value = emptyList() }
    val productTopItems : LiveData<List<Product>> = _productTopItems

    private val _productPickupItems = MutableLiveData<List<Product>>().apply { value = emptyList() }
    val productPickupItems : LiveData<List<Product>> = _productPickupItems

    private val _sliderItems = MutableLiveData<List<Slider>>().apply { value = emptyList() }

    private val _refineItems = MutableLiveData<List<Refine>>().apply { value = emptyList() }
    val refineItems : LiveData<List<Refine>> = _refineItems

    private val _refineItem = MutableLiveData<Refine>()
    val refineItem : LiveData<Refine> = _refineItem

    private val _categoryItems = MutableLiveData<List<Category>>().apply { value = emptyList() }
    val categoryItems : LiveData<List<Category>> = _categoryItems

    private val _categoryItem = MutableLiveData<Category>()
    val categoryItem : LiveData<Category> = _categoryItem

    private val _sortItems = MutableLiveData<List<Sort>>().apply { value = emptyList() }
    val sortItems : LiveData<List<Sort>> = _sortItems

    private val _sortItem = MutableLiveData<Sort>()
    val sortItem : LiveData<Sort> = _sortItem

    private val _productItems = MutableLiveData<List<Product>>().apply { value = emptyList() }
    val productItems : LiveData<List<Product>> = _productItems

    private val _productItem = MutableLiveData<Product>()
    val productItem : LiveData<Product> = _productItem

    private val _cartItems = MutableLiveData<List<Product>>().apply { value = emptyList() }
    val cartItems : LiveData<List<Product>> = _cartItems

    private val _onInitSlider = MutableLiveData<Event<List<Slider>>>()
    val onInitSlider: LiveData<Event<List<Slider>>> = _onInitSlider

    private val _onLoadProductComplete = MutableLiveData<Event<Product>>()
    val onLoadProductComplete: LiveData<Event<Product>> = _onLoadProductComplete

    private val _onRefineSelect = MutableLiveData<Event<Unit>>()
    val onRefineSelect: LiveData<Event<Unit>> = _onRefineSelect

    private val _onSortSelect = MutableLiveData<Event<Unit>>()
    val onSortSelect: LiveData<Event<Unit>> = _onSortSelect

    private val _onCartEmpty = MutableLiveData<Event<Unit>>()
    val onCartEmpty: LiveData<Event<Unit>> = _onCartEmpty

    private val _onCheckLanguage = MutableLiveData<Event<String>>()
    val onCheckLanguage: LiveData<Event<String>> = _onCheckLanguage

    private val _onPopupApiError = MutableLiveData<Event<String>>()
    val onPopupApiError: LiveData<Event<String>> = _onPopupApiError

    private val _onPopupError = MutableLiveData<Event<Int>>()
    val onPopupError: LiveData<Event<Int>> = _onPopupError

    private var accessToken = ""
    private var page = 1
    private var perPage = 10
    private var flagEndPage = false

    init {
        accessToken = ServiceLocator.getUserToken()

        if(_productTopItems.value.isNullOrEmpty() || _productPickupItems.value.isNullOrEmpty())
            getTopStore()
    }

    fun initStoreAll(context: Context){
        if(_categoryItems.value.isNullOrEmpty()){
            var tempCategory = ArrayList<Category>()
            tempCategory.add(Category(0, context.getString(R.string.store_all_item_category0)))
            _categoryItems.value = tempCategory
            _categoryItem.value = _categoryItems.value?.get(0)
        }
    }

    fun setLoadRefine(refineId: Int){
        if(refineId == REFINE_ALL_PRODUCT)
            _refineItem.value = Refine(1, "")
        else _refineItem.value = Refine(3, "")
        _sortItem.value = Sort(1, "")
        refreshStoreAllItem()
    }

    fun refreshStoreAllItem(){
        page = 1
        flagEndPage = false
        getAllProductInit((_refineItem.value?.id?:0).toString(), (_categoryItem.value?.id?:1).toString(), (_sortItem.value?.id?:0).toString(), perPage.toString())
    }

    fun loadMoreProduct(){
        if(!flagEndPage){
            page++

            getMoreProduct((_refineItem.value?.id?:1).toString(), (_categoryItem.value?.id?:1).toString(), (_sortItem.value?.id?:1).toString(), perPage.toString(), page.toString())
        }
    }

    fun selectRefine(refine: Refine){
        _refineItem.value = refine
        _onRefineSelect.value = Event(Unit)
        refreshStoreAllItem()
    }

    fun selectCategory(category: Category){
        _categoryItem.value = category
        refreshStoreAllItem()
    }

    fun selectSort(sort: Sort){
        _sortItem.value = sort
        _onSortSelect.value = Event(Unit)
        refreshStoreAllItem()
    }

    fun loadProductDetail(product: Product){
        _productItem.value = product
        getProductDetail((_productItem.value?.id?:0).toString())
    }

    fun addProductToCart(){
        _cartItems.value = emptyList()
        if(_productItem.value != null){
            _productItem.value?.cartQty = 1
            var tempCartList = ArrayList<Product>()
            tempCartList.add(_productItem.value!!)
            _cartItems.value = tempCartList

            //save cart
            if(tempCartList.isNotEmpty()) saveCart(tempCartList)
        }
    }

    fun removeProductCart(product: Product){
        var tempCart = ArrayList<Product>(_cartItems.value?: emptyList())
        tempCart.forEach { _product ->
            if(product.id == _product.id) tempCart.remove(_product)
        }
        if(tempCart.isEmpty()){
            _cartItems.value = emptyList()
            clearCart()
            _onCartEmpty.value = Event(Unit)
        } else _cartItems.value = tempCart
    }

    fun checkCart(){
        viewModelScope.launch {
            val cartResult = shopRepository.getShopCart()
            if(cartResult is Success){
                if(!cartResult.data.isNullOrEmpty()) _cartItems.value = cartResult.data
            }
        }
    }

    private fun getTopStore(){
        _dataLoading.value = true
        viewModelScope.launch {
            orderRepository.getProductList(accessToken, ServiceLocator.getLanguage(),
                { _topStoreResponse ->
                    _sliderItems.value = _topStoreResponse.sliders
                    _productTopItems.value = _topStoreResponse.newArrivals
                    _productPickupItems.value = _topStoreResponse.pickups
                    _onInitSlider.value = Event(_topStoreResponse.sliders?: emptyList())
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getAllProductInit(refineId: String, categoryId: String, sortId: String, perPage: String){
        _dataLoading.value = true
        viewModelScope.launch {
            orderRepository.getProductFilteredList(accessToken, ServiceLocator.getLanguage(), refineId, categoryId, sortId, perPage, "1",
                { _productListResponse ->
                    _refineItems.value = _productListResponse.refines?: emptyList()
                    _productListResponse.refines?.forEach { _refine ->
                        if (_refine.id ?: 0 == refineId.toInt())
                            _refineItem.value = _refine
                    }

                    var tempCategory = ArrayList<Category>(_categoryItems.value?: emptyList())
                    _productListResponse.categories?.forEach { _category ->
                        tempCategory.add(_category)
                        if (_category.id ?: 0 == categoryId.toInt())
                            _categoryItem.value = _category
                    }
                    _categoryItems.value = tempCategory

                    _sortItems.value = _productListResponse.sorts?: emptyList()
                    _productListResponse.sorts?.forEach { _sort ->
                        if (_sort.id ?: 0 == sortId.toInt())
                            _sortItem.value = _sort
                    }

                    if(_productListResponse.products.isNullOrEmpty() || _productListResponse.products?.size < perPage.toInt()) {
                        flagEndPage = true
                        _pageLoading.value = false
                    }else _pageLoading.value = true

                    _productItems.value = _productListResponse.products?: emptyList()
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getMoreProduct(refineId: String, categoryId: String, sortId: String, perPage: String, page: String){
        _pageLoading.value = true
        viewModelScope.launch {
            orderRepository.getProductFilteredList(accessToken, ServiceLocator.getLanguage(), refineId, categoryId, sortId, perPage, page,
                { _productListResponse ->
                    if(_productListResponse.products.isNullOrEmpty() || _productListResponse.products?.size < perPage.toInt()) {
                        flagEndPage = true
                        _pageLoading.value = false
                    }else _pageLoading.value = true

                    var tempProductList = ArrayList<Product>(_productItems.value?: emptyList())
                    _productListResponse.products?.forEach {  _product ->
                        tempProductList.add(_product)
                    }
                    _productItems.value = tempProductList
                },
                {
                    _pageLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getProductDetail(productId: String){
        _dataLoading.value = true
        viewModelScope.launch {
            orderRepository.getProductDetail(accessToken, ServiceLocator.getLanguage(), productId,
                { _product ->
                    if(_product.imageUrl.isNullOrEmpty() && !_product.imageUrls.isNullOrEmpty()) _product.imageUrl = _product.imageUrls?.get(0)
                    _productItem.value = _product
                    _onLoadProductComplete.value = Event(_product)
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun saveCart(cartList: List<Product>){
        viewModelScope.launch {
            shopRepository.insertCart(cartList)
        }
    }

    private fun clearCart(){
        viewModelScope.launch {
            shopRepository.clearCart()
        }
    }
}