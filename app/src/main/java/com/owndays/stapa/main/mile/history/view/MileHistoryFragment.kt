package com.owndays.stapa.main.mile.history.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.MileHistoryFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.FragmentPagerAdapter
import com.owndays.stapa.main.mile.viewmodel.MileViewModel
import com.owndays.stapa.main.view.MILE_FRAGMENT
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory
import kotlinx.android.synthetic.main.mile_history_fragment.*
import java.text.DecimalFormat

const val TARGET_HISTORY_TAB = "target_history_tab"
const val WAIT_FRAGMENT = 2

class MileHistoryFragment : Fragment(){

    private lateinit var viewDataBinding: MileHistoryFragmentBinding
    private lateinit var fragmentPagerAdapter: FragmentPagerAdapter
    private val mileViewModel by activityViewModels<MileViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = MileHistoryFragmentBinding.inflate(layoutInflater).apply {
            mileviewmodel = mileViewModel
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupEvent()
        setupView()
        setupAdapter()

        mileViewModel.refreshMileHistory()
    }

    private fun setupView(){
        viewDataBinding.apply {
            mileFragmentPager.setOnTouchListener { _, _ -> true }
            btnTab1.isEnabled = false
            mileFragmentPager.setCurrentItem(0,false)
        }
    }

    private fun setupEvent(){
        mileViewModel.apply {
            //Special case, load profileUser
            mileItem.observe(viewLifecycleOwner, { _mile ->
                if(_mile != null){
                    val formatter = DecimalFormat("#,###")
                    viewDataBinding.textMileBalance.text = formatter.format(_mile.grand)
                }
            })
            onAcceptMileComplete.observe(viewLifecycleOwner, EventObserver { _mileAmountString ->
                (activity as MainActivity)?.navigate(MILE_FRAGMENT, _mileAmountString)
            })
            onRejectMileConfirm.observe(viewLifecycleOwner, EventObserver { mileId ->
                val rejectMileConfirmDialog = RejectMileConfirmDialog(mileId)
                rejectMileConfirmDialog.show(childFragmentManager, REJECTMILE_CONFIRM_DIALOG)
            })
            onRejectMileComplete.observe(viewLifecycleOwner, EventObserver {
                val rejectMileCompleteDialog = RejectMileCompleteDialog()
                rejectMileCompleteDialog.show(childFragmentManager, REJECTMILE_COMPLETE_DIALOG)
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun setupAdapter(){
        fragmentPagerAdapter = FragmentPagerAdapter(childFragmentManager).apply {
            addFragment(MileEarnFragment.newInstance(), getString(R.string.mile_history_tab1))
            addFragment(MileSpendFragment.newInstance(), getString(R.string.mile_history_tab2))
            addFragment(MileWaitFragment.newInstance(), getString(R.string.mile_history_tab3))
        }
        viewDataBinding.mileFragmentPager.adapter = fragmentPagerAdapter

        val target = arguments?.getInt(TARGET_HISTORY_TAB)
        if(target == 2) {
            btnTab1.isEnabled = true
            btnTab3.isEnabled = false
        }
        viewDataBinding.mileFragmentPager.setCurrentItem(target?:0, false)
    }

    private val onClickListener = View.OnClickListener{
        viewDataBinding.apply {
            when(it.id){
                R.id.btnTab1 -> {
                    btnTab1.isEnabled = false
                    btnTab2.isEnabled = true
                    btnTab3.isEnabled = true
                    mileFragmentPager.setCurrentItem(0,false)
                }
                R.id.btnTab2 -> {
                    btnTab1.isEnabled = true
                    btnTab2.isEnabled = false
                    btnTab3.isEnabled = true
                    mileFragmentPager.setCurrentItem(1,false)
                }
                R.id.btnTab3 -> {
                    btnTab1.isEnabled = true
                    btnTab2.isEnabled = true
                    btnTab3.isEnabled = false
                    mileFragmentPager.setCurrentItem(2,false)
                }
            }
        }
    }
}