package com.owndays.stapa.main.store.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.observe
import com.owndays.stapa.R
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.CartDialogFragmentBinding
import com.owndays.stapa.databinding.NumberPickerDialogBinding
import com.owndays.stapa.dialog.view.DEFAULT_CANCEL_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultCancelOkDialog
import com.owndays.stapa.dialog.view.NUMBER_PICKER_DIALOG
import com.owndays.stapa.dialog.view.NumberPickerDialog
import com.owndays.stapa.main.adapters.PRODUCT_CART_MODE
import com.owndays.stapa.main.adapters.ProductItemAdapter
import com.owndays.stapa.main.setting.viewmodel.AddressViewModel
import com.owndays.stapa.main.store.viewmodel.StoreViewModel
import com.owndays.stapa.main.view.BADGE_CART
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.getViewModelFactory
import java.text.DecimalFormat

const val CART_DIALOG_FRAGMENT = "CartDialogFragment"

class CartDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding : CartDialogFragmentBinding
    private lateinit var listAdapter : ProductItemAdapter
    private val storeViewModel by activityViewModels<StoreViewModel> { getViewModelFactory() }
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = CartDialogFragmentBinding.inflate(layoutInflater).apply {
            storeviewmodel = storeViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()

        calTotalMile()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            btnClose.setOnClickListener { dismiss() }
            btnBuy.setOnClickListener {
                val cartConfirmDialogFragment = CartConfirmDialogFragment()
                cartConfirmDialogFragment.show(childFragmentManager, CART_CONFIRM_DIALOG_FRAGMENT)
            }
        }
    }

    private fun calTotalMile(){
        viewDataBinding.apply {
            val formatter = DecimalFormat("#,###")
            var totalMile = 0
            storeViewModel.cartItems.value?.forEach { _product ->
                totalMile += ((_product.mile?:0) * (_product.cartQty?:1))
            }
            textMileAmount.text = formatter.format(totalMile)
            textMileLeftAmount.text = formatter.format((mainViewModel.mileItem.value?.balance?:0) - totalMile)
        }
    }

    private fun setupEvent(){
        storeViewModel.apply {
            //Special case, set badge cart
            cartItems.observe(viewLifecycleOwner, { _cartItemList ->
                if(!_cartItemList.isNullOrEmpty()) (activity as MainActivity)?.setBadge(BADGE_CART, 1)
            })
            onCartEmpty.observe(viewLifecycleOwner, EventObserver {
                dismiss()
            })
        }
    }

    private fun setupAdapter(){
        listAdapter = ProductItemAdapter(PRODUCT_CART_MODE, {},
            { _product ->
                val defaultCancelOkDialog = DefaultCancelOkDialog(
                    "",
                    getString(R.string.confirm_msg_002),
                    getString(R.string.dialog_cancel),
                    getString(R.string.dialog_del),
                    {},
                    { storeViewModel.removeProductCart(_product)}
                )
                defaultCancelOkDialog.show(childFragmentManager, DEFAULT_CANCEL_OK_DIALOG)
            },
            { _product ->
                val numberPickerDialog = NumberPickerDialog(
                    1,
                    _product.stock?.limitQty?:1,
                    _product.cartQty?:1)
                { _selectQty ->
                    _product.cartQty = _selectQty
                    calTotalMile()
                    listAdapter.notifyDataSetChanged()
                }
                numberPickerDialog.show(childFragmentManager, NUMBER_PICKER_DIALOG)
            })
        viewDataBinding.cartListRecyclerView.adapter = listAdapter
    }
}