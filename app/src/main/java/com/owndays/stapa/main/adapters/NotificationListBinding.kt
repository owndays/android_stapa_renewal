package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Notification

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Notification>?) {
    items?.let {
        (listView.adapter as NotificationItemAdapter).submitList(items)
    }
}