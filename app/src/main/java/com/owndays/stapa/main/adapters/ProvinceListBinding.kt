package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Province

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Province>?) {
    items?.let {
        (listView.adapter as ProvinceItemAdapter).submitList(items)
    }
}