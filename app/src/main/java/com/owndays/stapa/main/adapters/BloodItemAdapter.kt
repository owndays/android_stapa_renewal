package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.R
import com.owndays.stapa.data.model.Blood
import com.owndays.stapa.databinding.BloodItemBinding
import com.owndays.stapa.dialog.viewmodel.BloodViewModel
import com.owndays.stapa.main.adapters.BloodItemAdapter.ViewHolder

class BloodItemAdapter(private val bloodViewModel: BloodViewModel) : ListAdapter<Blood, ViewHolder>(BloodDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, bloodViewModel, position, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: BloodItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(blood: Blood, bloodViewModel: BloodViewModel, position: Int, itemCount: Int) {
            binding.apply {
                blooditem = blood
                bloodviewmodel = bloodViewModel

                if(position == itemCount - 1)
                    layoutBg.setBackgroundResource(R.color.colorWhite)
                else layoutBg.setBackgroundResource(R.drawable.bg_white_grey_border_bottomonly)
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = BloodItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class BloodDiffCallback : DiffUtil.ItemCallback<Blood>() {
    override fun areItemsTheSame(oldItem: Blood, newItem: Blood): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Blood, newItem: Blood): Boolean {
        return oldItem == newItem
    }
}