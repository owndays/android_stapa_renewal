package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.History
import com.owndays.stapa.data.model.MileHistory
import com.owndays.stapa.databinding.MilehistoryYearItemBinding
import com.owndays.stapa.main.adapters.HistoryItemAdapter.ViewHolder
import com.owndays.stapa.main.mile.viewmodel.MileViewModel
import java.text.DecimalFormat

const val EARN_HISTORY = "EarnHistory"
const val SPEND_HISTORY = "SpendHistory"

class HistoryItemAdapter(private val mode: String, private val mileViewModel: MileViewModel,
                         private val onViewMileDetail : (year: String, mileHistory: MileHistory, mode: String) -> Unit) : ListAdapter<History, ViewHolder>(HistoryDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, mode, mileViewModel, position, onViewMileDetail)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: MilehistoryYearItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(history: History, mode: String, mileViewModel: MileViewModel, position: Int,
                 onViewMileDetail : (year: String, mileHistory: MileHistory, mode: String) -> Unit) {
            binding.apply {
                historyitem = history

                var totalMile = 0
                if(mode == EARN_HISTORY){
                    history.earnMileHistories?.forEach { _mileHistory ->
                        totalMile += _mileHistory.total?:0
                    }
                    textEarnMile.visibility = View.VISIBLE
                    textSpendMile.visibility = View.GONE

                    val listAdapter = MileHistoryItemAdapter(EARN_HISTORY, mileViewModel, history.year?:"2020", onViewMileDetail)
                    milehistoryRecyclerView.adapter = listAdapter
                    setItems(milehistoryRecyclerView, history.earnMileHistories?: emptyList())
                }else if(mode == SPEND_HISTORY){
                    history.spendMileHistories?.forEach { _mileHistory ->
                        totalMile += _mileHistory.total?:0
                    }
                    textEarnMile.visibility = View.GONE
                    textSpendMile.visibility = View.VISIBLE

                    val listAdapter = MileHistoryItemAdapter(SPEND_HISTORY, mileViewModel, history.year?:"2020", onViewMileDetail)
                    milehistoryRecyclerView.adapter = listAdapter
                    setItems(milehistoryRecyclerView, history.spendMileHistories?: emptyList())
                }
                val formatter = DecimalFormat("#,###")
                textMileAmount.text = if(totalMile > 0) formatter.format(totalMile) else "0"

                if(position != 0) imgArrow.visibility = View.GONE
            }
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = MilehistoryYearItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class HistoryDiffCallback : DiffUtil.ItemCallback<History>() {
    override fun areItemsTheSame(oldItem: History, newItem: History): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: History, newItem: History): Boolean {
        return oldItem == newItem
    }
}