package com.owndays.stapa.main.mile.getmile.idea.view

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.content.ContextCompat
import androidx.core.view.doOnNextLayout
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.IdeaDetailDialogFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_CANCEL_OK_DIALOG
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultCancelOkDialog
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.CommentItemAdapter
import com.owndays.stapa.main.adapters.DocumentItemAdapter
import com.owndays.stapa.main.adapters.setItems
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory
import com.owndays.stapa.utils.toNewPatternDateString
import java.text.DecimalFormat

const val IDEA_DETAIL_DIALOG_FRAGMENT = "IdeaDetailDialogFragment"
const val ON_COMMENT = "comment"

const val REFRESH_LIST_ADAPTER = "RefreshListAdapter"

class IdeaDetailDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: IdeaDetailDialogFragmentBinding
    private lateinit var commentListAdapter : CommentItemAdapter
    private lateinit var documentListAdapter : DocumentItemAdapter
    private val ideaViewModel by activityViewModels<IdeaViewModel> { getViewModelFactory() }

    private var ideaPostName = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = IdeaDetailDialogFragmentBinding.inflate(layoutInflater).apply {
            ideaviewmodel = ideaViewModel
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()

        if(activity?.intent?.getIntExtra(ON_COMMENT, 0) == 1){
            activity?.intent?.putExtra(ON_COMMENT, 0)
            viewDataBinding.edtComment.requestFocus()
            showKeyBoard()
        }
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            ideaViewModel.ideaItem.value?.let { idea ->
                textCreateDate.text = if(idea.createdDate.isNullOrEmpty()) "----.--.--" else idea.createdDate!!.toNewPatternDateString("yyyy-MM-dd","yyyy.MM.dd")
                val formatter = DecimalFormat("#,###")
                imgRank.apply {
                    if(idea.isApprove == false){
                        textMile.text = requireContext().getString(R.string.getmile_idea_post_action2_waiting)
                        setImageResource(R.drawable.ic_star_border)
                        imageTintList = null
                    }else{
                        textMile.text = formatter.format(idea.mile?:0)
                        setImageResource(R.drawable.ic_star_noborder)
                        imageTintList = when(idea.rank){
                            "bronze" -> { ContextCompat.getColorStateList(requireContext(), R.color.colorIdeaRankBronze) }
                            "silver" -> { ContextCompat.getColorStateList(requireContext(), R.color.colorIdeaRankSilver) }
                            "gold" -> { ContextCompat.getColorStateList(requireContext(), R.color.colorIdeaRankGold) }
                            else -> { ContextCompat.getColorStateList(requireContext(), R.color.colorIdeaRankNormal) }
                        }
                    }
                }
                Glide.with(root)
                    .load(idea.staff?.imageUrl?:"")
                    .circleCrop()
                    .placeholder(R.drawable.img_placeholder_profile)
                    .into(imgProfile)
                Glide.with(root)
                    .load(idea.staff?.imageUrl?:"")
                    .circleCrop()
                    .placeholder(R.drawable.img_placeholder_profile)
                    .into(imgCommentPhoto)
                ideaPostName = idea.staff?.name?:""
            }
        }
    }

    private fun setupEvent(){
        ideaViewModel.apply {
            onRefreshIdeaInfo.observe(viewLifecycleOwner, EventObserver{ _idea ->
                viewDataBinding.apply {
                    textLikeCount.text = (_idea.like?.size?:0).toString()
                    textCommentCount.text = (_idea.comments?.size?:0).toString()
                }
            })
            onCommentPostComplete.observe(viewLifecycleOwner, EventObserver{ _commentList ->
                hideKeyboard()
                viewDataBinding.apply {
                    setItems(commentListRecyclerView, _commentList)
                    commentListRecyclerView.doOnNextLayout { nestScrollView.fullScroll(View.FOCUS_DOWN) }
                    ideaViewModel.clearCommentText()
                    edtComment.clearFocus()
                }
                LocalBroadcastManager.getInstance(requireActivity()).sendBroadcast(Intent(REFRESH_LIST_ADAPTER))
            })
            onCommentDeleteComplete.observe(viewLifecycleOwner, EventObserver{ _commentList ->
                hideKeyboard()
                setItems(viewDataBinding.commentListRecyclerView, _commentList)
                LocalBroadcastManager.getInstance(requireActivity()).sendBroadcast(Intent(REFRESH_LIST_ADAPTER))
            })
            onCommentRefresh.observe(viewLifecycleOwner, EventObserver{
                hideKeyboard()
                commentListAdapter.notifyDataSetChanged()
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver{ errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver{ errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun setupAdapter(){
        documentListAdapter = DocumentItemAdapter(ideaViewModel) { _url ->
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(_url))
            startActivity(browserIntent)
        }
        viewDataBinding.documentListRecyclerView.adapter = documentListAdapter
        commentListAdapter = CommentItemAdapter ( ideaPostName,
            { _position ->
                val commentBottomDialogFragment = CommentBottomDialogFragment(_position) {
                    val cancelOkDialog = DefaultCancelOkDialog(
                        "",
                        getString(R.string.confirm_msg_003),
                        getString(R.string.dialog_cancel),
                        getString(R.string.dialog_del),
                        {}, { ideaViewModel.commentDelete(_position) })
                    cancelOkDialog.show(childFragmentManager, DEFAULT_CANCEL_OK_DIALOG)
                }
                commentBottomDialogFragment.show(childFragmentManager, COMMENT_BOTTOMDIALOG_FRAGMENT)
            }, ideaViewModel)
        viewDataBinding.commentListRecyclerView.adapter = commentListAdapter
    }

    private fun hideKeyboard(){
        (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(view?.windowToken,0)
    }

    private fun showKeyBoard(){
        (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .toggleSoftInput(InputMethodManager.SHOW_FORCED,0)
    }

    private val onClickListener = View.OnClickListener{
        viewDataBinding.apply {
            when(it.id){
                R.id.btnLike -> {
                    val idea = ideaViewModel.ideaItem.value
                    if(idea != null){
                        if(idea.isLike == true){
                            imgLike.imageTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorTextBlueGrey)
                            textLike.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorTextBlueGrey))
                        }else if(idea.isLike == false){
                            imgLike.imageTintList = ContextCompat.getColorStateList(requireContext(), R.color.colorAccent)
                            textLike.setTextColor(ContextCompat.getColor(requireContext(), R.color.colorAccent))
                        }
                        idea.isLike = !(idea.isLike?:false)
                        ideaViewModel.toggleLike(idea, ideaPostName)
                        LocalBroadcastManager.getInstance(requireActivity()).sendBroadcast(Intent(REFRESH_LIST_ADAPTER))
                    }
                }
                R.id.btnBack -> {
                    hideKeyboard()
                    ideaViewModel.clearCommentText()
                    dismiss()
                }
                R.id.btnSubmitComment -> {
                    val idea = ideaViewModel.ideaItem.value
                    if(idea != null) {
                        hideKeyboard()
                        ideaViewModel.commentPost(idea, edtComment.text.toString())
                    }
                }
                R.id.btnComment -> {
                    edtComment.requestFocus()
                    showKeyBoard()
                }
            }
        }
    }
}