package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Address

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<Address>?) {
    items?.let {
        (listView.adapter as AddressItemAdapter).submitList(items)
    }
}