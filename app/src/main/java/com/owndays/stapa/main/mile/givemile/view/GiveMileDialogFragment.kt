package com.owndays.stapa.main.mile.givemile.view

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.bumptech.glide.Glide
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.GivemileDialogFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.mile.view.REFRESH_INFO
import com.owndays.stapa.main.mile.viewmodel.MileViewModel
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory
import java.text.DecimalFormat

const val GIVEMILE_DIALOG_FRAGMENT = "GiveMileDialogFragment"

class GiveMileDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding : GivemileDialogFragmentBinding
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }
    private val mileViewModel by activityViewModels<MileViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = GivemileDialogFragmentBinding.inflate(layoutInflater).apply {
            mileviewmodel = mileViewModel
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()

        mileViewModel.setMileValue(mainViewModel.mileItem.value)
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            Glide.with(viewDataBinding.root)
                .load(mainViewModel.userItem.value?.imageUrl)
                .circleCrop()
                .placeholder(R.drawable.img_placeholder_profile)
                .into(viewDataBinding.imgUserProfile)
        }
    }

    private fun setupEvent(){
        mileViewModel.apply {
            //Special case, load profileUser
            mileItem.observe(viewLifecycleOwner, { _mile ->
                val formatter = DecimalFormat("#,###")
                viewDataBinding.textMile.text = formatter.format(_mile.balance?:0)
                viewDataBinding.textMileLeft.text = formatter.format(_mile.balance?:0)
            })
            //Special case, load profileUser
            staffItem.observe(viewLifecycleOwner, { _staffUser ->
                if(_staffUser != null){
                    Glide.with(viewDataBinding.root)
                        .load(_staffUser.imageUrl)
                        .circleCrop()
                        .placeholder(R.drawable.img_placeholder_profile)
                        .into(viewDataBinding.btnChooseStaff)
                }
            })
            onGiveMileComplete.observe(viewLifecycleOwner, EventObserver {
                LocalBroadcastManager.getInstance(requireActivity()).sendBroadcast(Intent(REFRESH_INFO))
                dismiss()
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private val onClickListener = View.OnClickListener {
        when(it.id){
            R.id.btnCancel -> {
                dismiss()
            }
            R.id.btn1000 -> {
                val colorBgReset = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorTransparent))
                val colorStrokeReset = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorDarkGrey))
                val colorTextReset = ContextCompat.getColor(requireContext(), R.color.colorTextDarkGrey)
                viewDataBinding.btn1000.apply {
                    backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorAccent))
                    strokeColor = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorAccent))
                    setTextColor(ContextCompat.getColor(requireContext(), R.color.colorWhite))
                }
                viewDataBinding.btn5000.apply {
                    backgroundTintList = colorBgReset
                    strokeColor = colorStrokeReset
                    setTextColor(colorTextReset)
                }
                viewDataBinding.btn10000.apply {
                    backgroundTintList = colorBgReset
                    strokeColor = colorStrokeReset
                    setTextColor(colorTextReset)
                }
                mileViewModel.giveMileValue(1000)
            }
            R.id.btn5000 -> {
                val colorBgReset = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorTransparent))
                val colorStrokeReset = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorDarkGrey))
                val colorTextReset = ContextCompat.getColor(requireContext(), R.color.colorTextDarkGrey)
                viewDataBinding.btn1000.apply {
                    backgroundTintList = colorBgReset
                    strokeColor = colorStrokeReset
                    setTextColor(colorTextReset)
                }
                viewDataBinding.btn5000.apply {
                    backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorAccent))
                    strokeColor = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorAccent))
                    setTextColor(ContextCompat.getColor(requireContext(), R.color.colorWhite))
                }
                viewDataBinding.btn10000.apply {
                    backgroundTintList = colorBgReset
                    strokeColor = colorStrokeReset
                    setTextColor(colorTextReset)
                }
                mileViewModel.giveMileValue(5000)
            }
            R.id.btn10000 -> {
                val colorBgReset = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorTransparent))
                val colorStrokeReset = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorDarkGrey))
                val colorTextReset = ContextCompat.getColor(requireContext(), R.color.colorTextDarkGrey)
                viewDataBinding.btn1000.apply {
                    backgroundTintList = colorBgReset
                    strokeColor = colorStrokeReset
                    setTextColor(colorTextReset)
                }
                viewDataBinding.btn5000.apply {
                    backgroundTintList = colorBgReset
                    strokeColor = colorStrokeReset
                    setTextColor(colorTextReset)
                }
                viewDataBinding.btn10000.apply {
                    backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorAccent))
                    strokeColor = ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorAccent))
                    setTextColor(ContextCompat.getColor(requireContext(), R.color.colorWhite))
                }
                mileViewModel.giveMileValue(10000)
            }
            R.id.btnAddMile -> {
                val addMileFragment = AddMileDialogFragment()
                addMileFragment.show(childFragmentManager, ADDMILE_DIALOG_FRAGMENT)
            }
            R.id.btnAddMessage -> {
                val addMessageFragment = AddMessageDialogFragment()
                addMessageFragment.show(childFragmentManager, ADDMESSAGE_DIALOG_FRAGMENT)
            }
            R.id.btnGiveMile -> {
                val giveMileConfirmDialog = GiveMileConfirmDialog()
                giveMileConfirmDialog.show(childFragmentManager, GIVEMILE_CONFIRM_DIALOG)
            }
            R.id.btnChooseStaff -> {
                val addStaffDialogFragment = AddStaffDialogFragment()
                addStaffDialogFragment.show(childFragmentManager, ADDSTAFF_DIALOG_FRAGMENT)
            }
        }
    }
}