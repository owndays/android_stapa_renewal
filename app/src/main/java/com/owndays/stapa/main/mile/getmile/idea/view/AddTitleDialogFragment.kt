package com.owndays.stapa.main.mile.getmile.idea.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.databinding.AddtitleDialogFragmentBinding
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.utils.getViewModelFactory

const val ADDTITLE_DIALOG_FRAGMENT = "AddTitleDialogFragment"

class AddTitleDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: AddtitleDialogFragmentBinding
    private val ideaViewModel by activityViewModels<IdeaViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = AddtitleDialogFragmentBinding.inflate(layoutInflater).apply {
            ideaviewmodel = ideaViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()

        (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .toggleSoftInput(InputMethodManager.SHOW_FORCED,0)
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            btnBack.setOnClickListener {
                hideKeyboard()
                dismiss()
            }
            btnComplete.setOnClickListener {
                ideaViewModel.setTitle(edtTitle.text.toString())
                hideKeyboard()
                dismiss()
            }
        }
    }

    private fun setupEvent(){

    }

    private fun hideKeyboard(){
        (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(view?.windowToken,0)
    }
}