package com.owndays.stapa.main.mile.view

import android.content.Context
import android.os.Bundle
import com.owndays.stapa.BaseActivity
import com.owndays.stapa.databinding.HelpActivityBinding
import com.owndays.stapa.utils.LocaleManager

class HelpActivity : BaseActivity(){

    private lateinit var viewBinding: HelpActivityBinding

    //Language re-config
    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleManager.setLocale(newBase?:baseContext))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = HelpActivityBinding.inflate(layoutInflater).apply {
            setContentView(root)

            btnClose.setOnClickListener { onBackPressed() }
        }
    }
}