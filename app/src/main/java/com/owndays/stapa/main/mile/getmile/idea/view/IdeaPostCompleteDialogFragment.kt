package com.owndays.stapa.main.mile.getmile.idea.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.databinding.IdeaPostCompleteDialogFragmentBinding
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.main.view.IDEA_POST_FRAGMENT
import com.owndays.stapa.main.view.IDEA_TIMELINE_FRAGMENT
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.utils.getViewModelFactory

const val IDEA_POST_COMPLETE_DIALOG = "IdeaPostCompleteDialog"

class IdeaPostCompleteDialogFragment : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: IdeaPostCompleteDialogFragmentBinding
    private val ideaViewModel by activityViewModels<IdeaViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = IdeaPostCompleteDialogFragmentBinding.inflate(layoutInflater).apply {
            ideaviewmodel = ideaViewModel
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private val onClickListener = View.OnClickListener {
        viewDataBinding.apply {
            when(it.id){
                R.id.btnClose -> {
                    (activity as MainActivity)?.navigate(IDEA_POST_FRAGMENT)
                    dismiss()
                }
                R.id.btnIdeaPost -> {
                    ideaViewModel.clearValue()
                    dismiss()
                }
                R.id.btnIdeaEveryone -> {
                    (activity as MainActivity)?.navigate(IDEA_TIMELINE_FRAGMENT)
                    dismiss()
                }
            }
        }
    }
}