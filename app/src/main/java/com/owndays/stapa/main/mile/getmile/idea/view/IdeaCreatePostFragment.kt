package com.owndays.stapa.main.mile.getmile.idea.view

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.data.model.Document
import com.owndays.stapa.databinding.IdeaCreatepostFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.DocumentItemAdapter
import com.owndays.stapa.main.adapters.viewmodel.CategoryViewModel
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory
import okio.ByteString.Companion.decodeBase64
import java.io.File
import java.nio.charset.Charset
import java.text.SimpleDateFormat
import java.util.*

const val GET_GALLERY_REQUEST_CODE = 9007
const val GET_PHOTO_REQUEST_CODE = 9008
const val GET_FILE_REQUEST_CODE = 9009
const val REQ_ACCESS_FILE_CODE = 1001

class IdeaCreatePostFragment : Fragment(){

    private lateinit var viewDataBinding : IdeaCreatepostFragmentBinding
    private lateinit var listAdapter: DocumentItemAdapter
    private val ideaViewModel by activityViewModels<IdeaViewModel> { getViewModelFactory() }
    private val categoryViewModel by activityViewModels<CategoryViewModel> { getViewModelFactory() }

    private var photoFileName = ""
    private var photoFilePath = ""
    private var totalFileSize = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = IdeaCreatepostFragmentBinding.inflate(layoutInflater).apply {
            ideaviewmodel = ideaViewModel
            navclick = navClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()

        ideaViewModel.clearValue()
        categoryViewModel.clearCategory()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        var allGranted = true
        grantResults.forEach { if(it == PackageManager.PERMISSION_DENIED) allGranted = false }
        if(allGranted) viewDataBinding.btnAttachFile.performClick()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            var enCoded = ""
            when(requestCode){
                GET_GALLERY_REQUEST_CODE -> {
                    val path = data?.data!!.path?:""
                    val file = File(path)
                    val byteFile = requireActivity().contentResolver.openInputStream(data?.data!!)?.readBytes()

                    //Check if more than 10MB
                    if(totalFileSize + (byteFile?.size?:0) < 10000000){
                        totalFileSize += (byteFile?.size ?: 0)
                        enCoded = Base64.encodeToString(byteFile, Base64.NO_WRAP)
                        ideaViewModel.addDocument(Document(0,"", file.name, if(file.extension.isEmpty()) "jpeg" else file.extension, enCoded))
                    }else{
                        //File is too big
                        val errorDialog = DefaultOkDialog("", getString(R.string.getmile_idea_post_total_file_size_max)) {}
                        errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
                    }
                }
                GET_PHOTO_REQUEST_CODE -> {
                    val byteFile = requireActivity().contentResolver.openInputStream(Uri.fromFile(File(photoFilePath)))?.readBytes()

                    //Check if more than 10MB
                    if(totalFileSize + (byteFile?.size?:0) < 10000000){
                        totalFileSize += (byteFile?.size ?: 0)
                        enCoded = Base64.encodeToString(byteFile, Base64.NO_WRAP)
                        ideaViewModel.addDocument(Document(0,"", photoFileName,"jpg", enCoded))
                    }else{
                        //File is too big
                        val errorDialog = DefaultOkDialog("", getString(R.string.getmile_idea_post_total_file_size_max)) {}
                        errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
                    }
                }
                GET_FILE_REQUEST_CODE -> {
                    val path = data?.data!!.path?:""
                    val file = File(path)
                    val byteFile = requireActivity().contentResolver.openInputStream(data?.data!!)?.readBytes()

                    //Check if more than 10MB
                    if(totalFileSize + (byteFile?.size?:0) < 10000000){
                        enCoded = Base64.encodeToString(byteFile, Base64.NO_WRAP)
                        ideaViewModel.addDocument(Document(0,"", file.name, if(file.extension.isEmpty()) "jpeg" else file.extension, enCoded))
                    }else{
                        //File is too big
                        val errorDialog = DefaultOkDialog("", getString(R.string.getmile_idea_post_total_file_size_max)) {}
                        errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
                    }
                }
            }
        }else{
            val errorDialog = DefaultOkDialog("", getString(R.string.err_msg_010)) {}
            errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
        }
    }

    private fun setupView(){
        viewDataBinding.apply {
            btnWhat.addTextChangedListener {
                var totalCount = 0
                if(it.toString().isNotEmpty())
                    totalCount += it.toString().replace(" ","").toByteArray(Charset.forName("Shift_JIS")).size
                if(btnWhy.text.toString().isNotEmpty())
                    totalCount += btnWhy.text.toString().replace(" ","").toByteArray(Charset.forName("Shift_JIS")).size
                if(btnHow.text.toString().isNotEmpty())
                    totalCount += btnHow.text.toString().replace(" ","").toByteArray(Charset.forName("Shift_JIS")).size
                ideaViewModel.setCountNo(totalCount)
            }
            btnWhy.addTextChangedListener {
                var totalCount = 0
                if(it.toString().isNotEmpty())
                    totalCount += it.toString().replace(" ","").toByteArray(Charset.forName("Shift_JIS")).size
                if(btnWhat.text.toString().isNotEmpty())
                    totalCount += btnWhat.text.toString().replace(" ","").toByteArray(Charset.forName("Shift_JIS")).size
                if(btnHow.text.toString().isNotEmpty())
                    totalCount += btnHow.text.toString().replace(" ","").toByteArray(Charset.forName("Shift_JIS")).size
                ideaViewModel.setCountNo(totalCount)
            }
            btnHow.addTextChangedListener {
                var totalCount = 0
                if(it.toString().isNotEmpty())
                    totalCount += it.toString().replace(" ","").toByteArray(Charset.forName("Shift_JIS")).size
                if(btnWhat.text.toString().isNotEmpty())
                    totalCount += btnWhat.text.toString().replace(" ","").toByteArray(Charset.forName("Shift_JIS")).size
                if(btnWhy.text.toString().isNotEmpty())
                    totalCount += btnWhy.text.toString().replace(" ","").toByteArray(Charset.forName("Shift_JIS")).size
                ideaViewModel.setCountNo(totalCount)
            }
        }
    }

    private fun setupEvent(){
        ideaViewModel.apply {
            onDocumentDelete.observe(viewLifecycleOwner, EventObserver { _enCode ->
                totalFileSize -= (_enCode.decodeBase64()?.size?:0)
            })
            onPostIdeaComplete.observe(viewLifecycleOwner, EventObserver {
                val ideaPostCompleteDialogFragment = IdeaPostCompleteDialogFragment()
                ideaPostCompleteDialogFragment.show(childFragmentManager, IDEA_POST_COMPLETE_DIALOG)
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun setupAdapter(){
        listAdapter = DocumentItemAdapter(ideaViewModel){}
        viewDataBinding.documentListRecyclerView.adapter = listAdapter
    }

    private val navClickListener = View.OnClickListener {
        when(it.id){
            R.id.btnCategory -> {
                categoryViewModel.setCategoryList(ideaViewModel.categoryItems.value?: emptyList())
                categoryViewModel.setCategoryItem(ideaViewModel.selectedCategoryItem.value)
                val ideaCategoryFragment = IdeaCategoryDialogFragment()
                ideaCategoryFragment.show(childFragmentManager, IDEA_CATEGORY_DIALOG_FRAGMENT)
            }
            R.id.btnTitle -> {
                val addTitleFragment = AddTitleDialogFragment()
                addTitleFragment.show(childFragmentManager, ADDTITLE_DIALOG_FRAGMENT)
            }
            R.id.btnWhat -> {
                val addWhatMessageFragment = AddWhatMessageDialogFragment()
                addWhatMessageFragment.show(childFragmentManager, ADDWHATMESSAGE_DIALOG_FRAGMENT)
            }
            R.id.btnWhy -> {
                val addWhyMessageFragment = AddWhyMessageDialogFragment()
                addWhyMessageFragment.show(childFragmentManager, ADDWHYMESSAGE_DIALOG_FRAGMENT)
            }
            R.id.btnHow -> {
                val addHowMessageFragment = AddHowMessageDialogFragment()
                addHowMessageFragment.show(childFragmentManager, ADDHOWMESSAGE_DIALOG_FRAGMENT)
            }
            R.id.btnAttachFile -> {
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.CAMERA
                    ) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(
                        requireContext(),
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    val fileDialogFragment = FileBottomDialogFragment(
                        { openGallery() },
                        { openCamera() },
                        { openFile() })
                    fileDialogFragment.enterTransition
                    fileDialogFragment.show(childFragmentManager, FILE_BOTTOMDIALOG_FRAGMENT)
                } else {
                    requestPermissions(
                        arrayOf(
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ),
                        REQ_ACCESS_FILE_CODE
                    )
                }
            }
            R.id.btnGiveMile -> {
                val ideaPostConfirmDialog = IdeaPostConfirmDialog()
                ideaPostConfirmDialog.show(childFragmentManager, IDEA_POST_CONFIRM_DIALOG)
            }
        }
    }

    private fun openGallery(){
        Intent(Intent.ACTION_PICK).also { galleryIntent ->
            galleryIntent.type = "*/*"
            galleryIntent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/png", "image/jpeg"))
            galleryIntent.resolveActivity(requireActivity().packageManager)?.also {
                startActivityForResult(galleryIntent, GET_GALLERY_REQUEST_CODE)
            }
        }
    }

    private fun openCamera(){
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            photoFileName = "IMG_+ ${SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())}"
            val photoFile = File.createTempFile(
                photoFileName,
                ".jpg",
                requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            )
            photoFilePath = photoFile.absolutePath
            val photoURI = FileProvider.getUriForFile(requireContext(), "${requireContext().packageName}.provider" , photoFile)
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            takePictureIntent.resolveActivity(requireActivity().packageManager)?.also {
                startActivityForResult(takePictureIntent, GET_PHOTO_REQUEST_CODE)
            }
        }
    }

    private fun openFile(){
        Intent(Intent.ACTION_GET_CONTENT).also { getFileIntent ->
            getFileIntent.type = "application/pdf"
            getFileIntent.resolveActivity(requireActivity().packageManager)?.also {
                startActivityForResult(getFileIntent, GET_FILE_REQUEST_CODE)
            }
        }
    }
}