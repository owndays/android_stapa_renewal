package com.owndays.stapa.main.mile.getmile.idea.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.viewpager.widget.ViewPager
import com.owndays.stapa.R
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.IdeaFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.FragmentPagerAdapter
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory

const val TARGET_IDEA_TAB = "target_idea_tab"
const val POST_FRAGMENT = 0
const val TIMELINE_FRAGMENT = 1

class IdeaFragment : Fragment(){

    private lateinit var viewDataBinding : IdeaFragmentBinding
    private lateinit var fragmentPagerAdapter: FragmentPagerAdapter
    private val ideaViewModel by activityViewModels<IdeaViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = IdeaFragmentBinding.inflate(layoutInflater).apply {
            ideaviewmodel = ideaViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupEvent()
        setupPagingAdapter()

        ideaViewModel.checkCategory(requireContext())
    }

    private fun setupEvent(){
        ideaViewModel.apply {
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver{ errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver{ errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun setupPagingAdapter(){
        fragmentPagerAdapter = FragmentPagerAdapter(childFragmentManager).apply {
            addFragment(IdeaPostFragment.newInstance(), getString(R.string.getmile_idea_tab1))
            addFragment(IdeaTimeLineFragment.newInstance(), getString(R.string.getmile_idea_tab2))
        }
        viewDataBinding.ideaFragmentPager.apply {
            adapter = fragmentPagerAdapter
            addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

                }

                override fun onPageSelected(position: Int) {
                    (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                        .hideSoftInputFromWindow(view?.windowToken,0)
                }

                override fun onPageScrollStateChanged(state: Int) {

                }
            })
            val target = arguments?.getInt(TARGET_IDEA_TAB)
            setCurrentItem(target?:0, false)
        }
    }
}