package com.owndays.stapa.main.setting.view

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.owndays.stapa.R
import com.owndays.stapa.databinding.PhotoBottomdialogFragmentBinding

const val PHOTO_BOTTOMDIALOG_FRAGMENT = "PhotoBottomDialogFragment"

class PhotoBottomDialogFragment(private val onGallery: () -> Unit, private val onPhoto: () -> Unit) : BottomSheetDialogFragment(){

    private lateinit var viewDataBinding: PhotoBottomdialogFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = PhotoBottomdialogFragmentBinding.inflate(layoutInflater).apply {
            onclick = onClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener { dialog ->
                (dialog as BottomSheetDialog).findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)?.background = null
            }
        }
    }

    private fun setupView(){
        viewDataBinding.apply {

        }
    }

    private fun setupEvent(){

    }

    private val onClickListener = View.OnClickListener {
        viewDataBinding.apply {
            when(it.id){
                R.id.btnGallery -> {
                    onGallery.invoke()
                    dismiss()
                }
                R.id.btnPhoto -> {
                    onPhoto.invoke()
                    dismiss()
                }
                R.id.btnCancel -> {
                    dismiss()
                }
            }
        }
    }
}