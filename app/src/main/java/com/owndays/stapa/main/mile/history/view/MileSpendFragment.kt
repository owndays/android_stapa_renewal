package com.owndays.stapa.main.mile.history.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.databinding.MileSpendFragmentBinding
import com.owndays.stapa.main.adapters.HistoryItemAdapter
import com.owndays.stapa.main.adapters.SPEND_HISTORY
import com.owndays.stapa.main.mile.viewmodel.MileViewModel
import com.owndays.stapa.utils.getViewModelFactory

class MileSpendFragment : Fragment(){

    private lateinit var viewDataBinding: MileSpendFragmentBinding
    private lateinit var listAdapter: HistoryItemAdapter
    private val mileViewModel by activityViewModels<MileViewModel> { getViewModelFactory() }

    companion object{
        fun newInstance() : MileSpendFragment {
            return MileSpendFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = MileSpendFragmentBinding.inflate(layoutInflater).apply {
            mileviewmodel = mileViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()
    }

    private fun setupView(){

    }

    private fun setupEvent(){

    }

    private fun setupAdapter(){
        listAdapter = HistoryItemAdapter(SPEND_HISTORY, mileViewModel){ year, mileHistory, mode ->
            val mileHistoryDetailDialogFragment = MileHistoryDetailDialogFragment(year, mileHistory, mode)
            mileHistoryDetailDialogFragment.show(childFragmentManager, MILEHISTORY_DETAIL_DIALOG_FRAGMENT)
        }
        viewDataBinding.historyRecyclerView.adapter = listAdapter
    }
}