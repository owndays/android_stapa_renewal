package com.owndays.stapa.main.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.owndays.stapa.R
import com.owndays.stapa.data.model.Idea
import com.owndays.stapa.databinding.IdeaOtherItemBinding
import com.owndays.stapa.databinding.IdeaUserItemBinding
import com.owndays.stapa.main.adapters.IdeaItemAdapter.ViewHolder
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.utils.toNewPatternDateString
import java.text.DecimalFormat

const val USER_IDEA = "UserIdea"
const val OTHER_IDEA = "OtherIdea"

class IdeaItemAdapter(private val mode: String, private val ideaViewModel: IdeaViewModel, private val context: Context,
                      private val openDetail: (idea: Idea) -> Unit, private val openDetailComment: (idea: Idea) -> Unit,
                      private val onViewDocument: (url: String) -> Unit) : ListAdapter<Idea, ViewHolder>(IdeaDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, mode, ideaViewModel, context, openDetail, openDetailComment, onViewDocument, position, itemCount)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        if (mode == USER_IDEA) {
            return ViewHolder.fromUser(parent, mode)
        } else {
            return ViewHolder.fromOther(parent, mode)
        }
    }

    class ViewHolder : RecyclerView.ViewHolder {

        private lateinit var ideaUserItemBinding: IdeaUserItemBinding
        private lateinit var ideaOtherItemBinding: IdeaOtherItemBinding
        private var mode: String

        constructor(binding: IdeaUserItemBinding, _mode: String) : super(binding.root) {
            mode = _mode
            ideaUserItemBinding = binding
        }

        constructor(binding: IdeaOtherItemBinding, _mode: String) : super(binding.root) {
            mode = _mode
            ideaOtherItemBinding = binding
        }

        fun bind(idea: Idea, mode: String, ideaViewModel: IdeaViewModel, context: Context, openDetail: (idea: Idea) -> Unit,
                 openDetailComment: (idea: Idea) -> Unit, onViewDocument: (url: String) -> Unit, position: Int, itemCount: Int) {
            if(mode == USER_IDEA){
                ideaUserItemBinding.apply {
                    ideaitem = idea

                    layoutBg.setOnClickListener { openDetail.invoke(idea) }
                    textCreateDate.text = if(idea.createdDate.isNullOrEmpty()) "----.--.--" else idea.createdDate!!.toNewPatternDateString("yyyy-MM-dd","yyyy.MM.dd")
                    val formatter = DecimalFormat("#,###")
                    imgRank.apply {
                        if(idea.isApprove == false){
                            textMile.text = context.getString(R.string.getmile_idea_post_action2_waiting)
                            setImageResource(R.drawable.ic_star_border)
                            imageTintList = null
                        }else{
                            textMile.text = formatter.format(idea.mile?:0)
                            setImageResource(R.drawable.ic_star_noborder)
                            imageTintList = when(idea.rank){
                                "bronze" -> { ContextCompat.getColorStateList(context, R.color.colorIdeaRankBronze) }
                                "silver" -> { ContextCompat.getColorStateList(context, R.color.colorIdeaRankSilver) }
                                "gold" -> { ContextCompat.getColorStateList(context, R.color.colorIdeaRankGold) }
                                else -> { ContextCompat.getColorStateList(context, R.color.colorIdeaRankNormal) }
                            }
                        }
                    }
                    //Using this way instead of dataBinding >> faster for display without notify data change
                    btnReadMore.setOnClickListener {
                        if(idea.isExpanded) {
                            btnReadMore.setText(R.string.getmile_idea_post_action2_readmore)
                            textWhyTitle.visibility = View.GONE
                            textWhy.visibility = View.GONE
                            textHowTitle.visibility = View.GONE
                            textHow.visibility = View.GONE
                        }else{
                            btnReadMore.setText(R.string.getmile_idea_post_action2_seeless)
                            textWhyTitle.visibility = View.VISIBLE
                            textWhy.visibility = View.VISIBLE
                            textHowTitle.visibility = View.VISIBLE
                            textHow.visibility = View.VISIBLE
                        }
                        idea.isExpanded = !idea.isExpanded
                    }

                    val listAdapter = DocumentItemAdapter(ideaViewModel){ _url ->
                        onViewDocument.invoke(_url)
                    }
                    documentListRecyclerView.adapter = listAdapter
                    setItems(documentListRecyclerView, idea.documents?: emptyList())

                    if(position == itemCount - 1) ideaViewModel.loadNextHistoryPaging()
                }
                ideaUserItemBinding.executePendingBindings()
            }else if(mode == OTHER_IDEA){
                ideaOtherItemBinding.apply {
                    ideaitem = idea
                    ideaviewmodel = ideaViewModel

                    layoutBg.setOnClickListener { openDetail.invoke(idea) }
                    textCreateDate.text = if(idea.createdDate.isNullOrEmpty()) "----.--.--" else idea.createdDate!!.toNewPatternDateString("yyyy-MM-dd","yyyy.MM.dd")
                    val formatter = DecimalFormat("#,###")
                    imgRank.apply {
                        if(idea.isApprove == false){
                            textMile.text = context.getString(R.string.getmile_idea_post_action2_waiting)
                            setImageResource(R.drawable.ic_star_border)
                            imageTintList = null
                        }else{
                            textMile.text = formatter.format(idea.mile?:0)
                            setImageResource(R.drawable.ic_star_noborder)
                            imageTintList = when(idea.rank){
                                "bronze" -> { ContextCompat.getColorStateList(context, R.color.colorIdeaRankBronze) }
                                "silver" -> { ContextCompat.getColorStateList(context, R.color.colorIdeaRankSilver) }
                                "gold" -> { ContextCompat.getColorStateList(context, R.color.colorIdeaRankGold) }
                                else -> { ContextCompat.getColorStateList(context, R.color.colorIdeaRankNormal) }
                            }
                        }
                    }
                    //Using this way instead of dataBinding >> faster for display without notify data change
                    btnReadMore.setOnClickListener {
                        if(idea.isExpanded) {
                            btnReadMore.setText(R.string.getmile_idea_post_action2_readmore)
                            textWhyTitle.visibility = View.GONE
                            textWhy.visibility = View.GONE
                            textHowTitle.visibility = View.GONE
                            textHow.visibility = View.GONE
                        }else{
                            btnReadMore.setText(R.string.getmile_idea_post_action2_seeless)
                            textWhyTitle.visibility = View.VISIBLE
                            textWhy.visibility = View.VISIBLE
                            textHowTitle.visibility = View.VISIBLE
                            textHow.visibility = View.VISIBLE
                        }
                        idea.isExpanded = !idea.isExpanded
                    }
                    btnLike.setOnClickListener {
                        if(idea.isLike == true){
                            imgLike.imageTintList = ContextCompat.getColorStateList(context, R.color.colorTextBlueGrey)
                            textLike.setTextColor(ContextCompat.getColor(context, R.color.colorTextBlueGrey))
                        }else if(idea.isLike == false){
                            imgLike.imageTintList = ContextCompat.getColorStateList(context, R.color.colorAccent)
                            textLike.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
                        }
                        idea.isLike = !(idea.isLike?:false)
                        ideaViewModel.toggleLike(idea, idea.staff?.name?:"")
                    }
                    btnComment.setOnClickListener {
                        openDetailComment.invoke(idea)
                    }

                    val listAdapter = DocumentItemAdapter(ideaViewModel){ _url ->
                        onViewDocument.invoke(_url)
                    }
                    documentListRecyclerView.adapter = listAdapter
                    setItems(documentListRecyclerView, idea.documents?: emptyList())

                    Glide.with(root)
                        .load(idea.staff?.imageUrl?:"")
                        .circleCrop()
                        .placeholder(R.drawable.img_placeholder_profile)
                        .into(imgProfile)

                    if(position == itemCount - 1) ideaViewModel.loadNextIdeaOtherPaging()
                }
                ideaOtherItemBinding.executePendingBindings()
            }
        }

        companion object {
            fun fromUser(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = IdeaUserItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding, mode)
            }

            fun fromOther(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = IdeaOtherItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding, mode)
            }
        }
    }

    class IdeaDiffCallback : DiffUtil.ItemCallback<Idea>() {
        override fun areItemsTheSame(oldItem: Idea, newItem: Idea): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Idea, newItem: Idea): Boolean {
            return oldItem == newItem
        }
    }
}