package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.MileHistory

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<MileHistory>?) {
    items?.let {
        (listView.adapter as MileHistoryItemAdapter).submitList(items)
    }
}