package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Category
import com.owndays.stapa.databinding.CategoryItemBinding
import com.owndays.stapa.databinding.IdeaCategoryItemBinding
import com.owndays.stapa.main.adapters.CategoryItemAdapter.ViewHolder
import com.owndays.stapa.main.adapters.viewmodel.CategoryViewModel

const val MODE_IDEA_CATEGORY = "IdeaCategory"
const val MODE_PRODUCT_CATEGORY = "ProductCategory"

class CategoryItemAdapter(private val mode: String, private val categoryViewModel: CategoryViewModel)
    : ListAdapter<Category, ViewHolder>(CategoryDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, categoryViewModel, mode)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (mode == MODE_IDEA_CATEGORY) {
            return ViewHolder.fromIdea(parent, mode)
        } else {
            return ViewHolder.fromProduct(parent, mode)
        }
    }

    class ViewHolder : RecyclerView.ViewHolder {

        private lateinit var ideaCategoryItemBinding: IdeaCategoryItemBinding
        private lateinit var productCategoryItemBinding: CategoryItemBinding
        private var mode: String

        constructor(binding: IdeaCategoryItemBinding, _mode: String) : super(binding.root) {
            mode = _mode
            ideaCategoryItemBinding = binding
        }

        constructor(binding: CategoryItemBinding, _mode: String) : super(binding.root) {
            mode = _mode
            productCategoryItemBinding = binding
        }

        fun bind(category: Category, categoryViewModel: CategoryViewModel, mode: String) {
            if(mode == MODE_IDEA_CATEGORY){
                ideaCategoryItemBinding.apply {
                    categoryitem = category
                    categoryviewmodel = categoryViewModel
                }
                ideaCategoryItemBinding.executePendingBindings()
            }else if(mode == MODE_PRODUCT_CATEGORY){
                productCategoryItemBinding.apply {
                    categoryitem = category
                    categoryviewmodel = categoryViewModel
                }
                productCategoryItemBinding.executePendingBindings()
            }
        }

        companion object {
            fun fromIdea(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = IdeaCategoryItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding, mode)
            }

            fun fromProduct(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = CategoryItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding, mode)
            }
        }
    }
}

class CategoryDiffCallback : DiffUtil.ItemCallback<Category>() {
    override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
        return oldItem == newItem
    }
}