package com.owndays.stapa.main.adapters

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.User

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<User>?) {
    items?.let {
        (listView.adapter as StaffItemAdapter).submitList(items)
    }
}