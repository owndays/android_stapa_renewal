package com.owndays.stapa.main.setting.view

import android.content.Context
import android.graphics.Canvas
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.R

class AddressItemSwipeCallback(
    private val listener: AddressItemSwipeListener,
    private val context: Context
) :
    ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT){
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        listener.onItemSwipe(viewHolder.adapterPosition, direction)
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){
            val newLeftDx = context.resources.getDimension(R.dimen.address_item_delete_translation)
            if(dX < 0) super.onChildDraw(c, recyclerView, viewHolder, if(dX > newLeftDx) dX else newLeftDx, dY, actionState, isCurrentlyActive)
        }
    }
}