package com.owndays.stapa.main.checkin.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.owndays.stapa.BaseViewModel
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.Event
import com.owndays.stapa.data.source.repository.ShopRepository
import com.owndays.stapa.data.source.repository.UserRepository

class CheckInViewModel (
    private val shopRepository: ShopRepository,
    private val userRepository: UserRepository
) : BaseViewModel(){

    private val _dataLoading = MutableLiveData<Boolean>().apply { value = false }
    val dataLoading : LiveData<Boolean> = _dataLoading

    private val _onCheckLanguage = MutableLiveData<Event<String>>()
    val onCheckLanguage: LiveData<Event<String>> = _onCheckLanguage

    private val _onPopupApiError = MutableLiveData<Event<String>>()
    val onPopupApiError: LiveData<Event<String>> = _onPopupApiError

    private val _onPopupError = MutableLiveData<Event<Int>>()
    val onPopupError: LiveData<Event<Int>> = _onPopupError

    private var accessToken = ""

    init {
        accessToken = ServiceLocator.getUserToken()
    }
}