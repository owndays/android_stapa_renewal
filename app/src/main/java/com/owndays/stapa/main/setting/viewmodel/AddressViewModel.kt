package com.owndays.stapa.main.setting.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.owndays.stapa.BaseViewModel
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.Event
import com.owndays.stapa.data.model.Address
import com.owndays.stapa.data.model.Province
import com.owndays.stapa.data.source.remote.model.request.StaffAddressRequest
import com.owndays.stapa.data.source.repository.UserRepository
import kotlinx.coroutines.launch

class AddressViewModel(
    private val userRepository: UserRepository
) : BaseViewModel(){

    private val _dataLoading = MutableLiveData<Boolean>().apply { value = false }
    val dataLoading : LiveData<Boolean> = _dataLoading

    private val _addressItems = MutableLiveData<List<Address>>().apply { value = emptyList() }
    val addressItems : LiveData<List<Address>> = _addressItems

    private val _addressItem = MutableLiveData<Address>()
    val addressItem : LiveData<Address> = _addressItem

    val addressName = MutableLiveData<String>().apply { value = "" }
    val addressZip = MutableLiveData<String>().apply { value = "" }
    private val _addressProvince = MutableLiveData<Province>()
    val addressProvince: LiveData<Province> = _addressProvince
    val addressCity = MutableLiveData<String>().apply { value = "" }
    val addressTown = MutableLiveData<String>().apply { value = "" }
    val addressOther = MutableLiveData<String>().apply { value = "" }
    val addressPhone = MutableLiveData<String>().apply { value = "" }

    private val _isAddressValid = MutableLiveData<Boolean>().apply { value = false }
    val isAddressValid: LiveData<Boolean> = _isAddressValid

    private val _provinceItems = MutableLiveData<List<Province>>().apply { value = emptyList() }
    val provinceItems : LiveData<List<Province>> = _provinceItems

    private val _onCompleteAddEdit = MutableLiveData<Event<Unit>>()
    val onCompleteAddEdit: LiveData<Event<Unit>> = _onCompleteAddEdit

    private val _onCheckLanguage = MutableLiveData<Event<String>>()
    val onCheckLanguage: LiveData<Event<String>> = _onCheckLanguage

    private val _onPopupApiError = MutableLiveData<Event<String>>()
    val onPopupApiError: LiveData<Event<String>> = _onPopupApiError

    private val _onPopupError = MutableLiveData<Event<Int>>()
    val onPopupError: LiveData<Event<Int>> = _onPopupError

    private var accessToken = ""

    init {
        accessToken = ServiceLocator.getUserToken()

        if(_provinceItems.value.isNullOrEmpty()) getProvince()
    }

    fun refreshAddressList(){
        getUserAddress()
    }

    fun setAddress(address: Address){
        _addressItem.value = address

        addressName.value = _addressItem.value?.name?:""
        addressZip.value = _addressItem.value?.zip?:""
        _addressProvince.value = _addressItem.value?.province
        addressCity.value = _addressItem.value?.city?:""
        addressTown.value = _addressItem.value?.town?:""
        addressOther.value = _addressItem.value?.other?:""
        addressPhone.value = _addressItem.value?.phone?:""
    }

    fun setProvince(province: Province){
        _addressProvince.value = province
    }

    fun clearAddress(){
        addressName.value = ""
        addressZip.value = ""
        _addressProvince.value = null
        addressCity.value = ""
        addressTown.value = ""
        addressOther.value = ""
        addressPhone.value = ""
        _isAddressValid.value = false
    }

    fun validateAddress(){
        if(addressName.value.isNullOrEmpty()){
            _isAddressValid.value = false
            return
        }
        if(addressZip.value.isNullOrEmpty()){
            _isAddressValid.value = false
            return
        }
        if(_addressProvince.value == null){
            _isAddressValid.value = false
            return
        }
        if(addressCity.value.isNullOrEmpty()){
            _isAddressValid.value = false
            return
        }
        if(addressTown.value.isNullOrEmpty()){
            _isAddressValid.value = false
            return
        }
        if(addressPhone.value.isNullOrEmpty()){
            _isAddressValid.value = false
            return
        }
        _isAddressValid.value = true
    }

    fun addAddress(){
        processAddAddress(
            addressName.value?:"",
            addressZip.value?:"",
            addressProvince.value?.id?:0,
            addressTown.value?:"",
            addressCity.value?:"",
            addressPhone.value?:"",
            addressOther.value?:"",
        )
    }

    fun editAddress(){
        processEditAddress(
            _addressItem.value?.id?:0,
            addressName.value?:"",
            addressZip.value?:"",
            addressProvince.value?.id?:0,
            addressTown.value?:"",
            addressCity.value?:"",
            addressPhone.value?:"",
            addressOther.value?:"",
        )
    }

    fun confirmDeleteAddress(){
        if(_addressItem.value != null) processDeleteAddress(_addressItem.value?.id?:0)
    }

    private fun getUserAddress(){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.getUserAddressList(accessToken, ServiceLocator.getLanguage(),
                { _addressList ->
                    _addressList.forEach { _address ->
                        if(_address.default == "1.0") _addressItem.value = _address
                    }
                    _addressItems.value = _addressList
                    _dataLoading.value = false
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processAddAddress(name: String, zip: String, provinceId: Int, town: String, city: String, phone: String, other: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.addUserAddress(accessToken, ServiceLocator.getLanguage(), StaffAddressRequest(0, name, zip, provinceId, town, city, phone, other),
                {
                    _dataLoading.value = false
                    _onCompleteAddEdit.value = Event(Unit)
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processEditAddress(addressId: Int, name: String, zip: String, provinceId: Int, town: String, city: String, phone: String, other: String){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.editUserAddress(accessToken, ServiceLocator.getLanguage(), addressId.toString(), StaffAddressRequest(addressId, name, zip, provinceId, town, city, phone, other),
                {
                    _dataLoading.value = false
                    _onCompleteAddEdit.value = Event(Unit)
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun processDeleteAddress(addressId: Int){
        _dataLoading.value = true
        viewModelScope.launch {
            userRepository.deleteUserAddress(accessToken, ServiceLocator.getLanguage(), addressId.toString(),
                {
                    getUserAddress()
                    _onCompleteAddEdit.value = Event(Unit)
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }

    private fun getProvince(){
        viewModelScope.launch {
            userRepository.getUserProvinceList(accessToken, ServiceLocator.getLanguage(),
                { _provinceList ->
                    _provinceItems.value = _provinceList
                },
                {
                    _dataLoading.value = false
                    _onPopupApiError.value = Event(it?.responseStatus?.message ?: "")
                },
                { _langCode ->
                    _onCheckLanguage.value = Event(_langCode)
                })
        }
    }
}