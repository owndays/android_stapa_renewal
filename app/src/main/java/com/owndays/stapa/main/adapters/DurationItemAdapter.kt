package com.owndays.stapa.main.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.R
import com.owndays.stapa.data.model.Duration
import com.owndays.stapa.databinding.DurationGachaItemBinding
import com.owndays.stapa.databinding.DurationShiftItemBinding
import com.owndays.stapa.main.adapters.DurationItemAdapter.ViewHolder

const val GACHA_MODE = "GachaMode"
const val SHIFT_MODE = "ShiftMode"

class DurationItemAdapter(private val mode: String, private val context: Context) : ListAdapter<Duration, ViewHolder>(DurationDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, mode, context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if(mode == GACHA_MODE)
            return ViewHolder.fromGacha(parent, mode)
        else
            return ViewHolder.fromShift(parent, mode)
    }

    class ViewHolder : RecyclerView.ViewHolder{

        private lateinit var durationGachaItemBinding: DurationGachaItemBinding
        private lateinit var durationShiftItemBinding: DurationShiftItemBinding
        private var mode: String

        constructor(binding: DurationGachaItemBinding, _mode: String) : super(binding.root)  {
            durationGachaItemBinding = binding
            mode = _mode
        }

        constructor(binding: DurationShiftItemBinding, _mode: String) : super(binding.root) {
            durationShiftItemBinding = binding
            mode = _mode
        }

        fun bind(duration: Duration, mode: String, context: Context) {
            if(mode == GACHA_MODE){
                durationGachaItemBinding.apply {
                    durationitem = duration

                    when(duration.dayOfWeek){
                        1 -> { textDate.setTextColor(ContextCompat.getColor(context, R.color.colorTextRed)) }
                        7 -> { textDate.setTextColor(ContextCompat.getColor(context, R.color.colorTextBlueGrey)) }
                        else -> { textDate.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText))}
                    }
                }
                durationGachaItemBinding.executePendingBindings()
            }else if(mode == SHIFT_MODE){
                durationShiftItemBinding.apply {
                    durationitem = duration
                }
                durationShiftItemBinding.executePendingBindings()
            }
        }

        companion object {
            fun fromGacha(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = DurationGachaItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding, mode)
            }

            fun fromShift(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = DurationShiftItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding, mode)
            }
        }
    }
}

class DurationDiffCallback : DiffUtil.ItemCallback<Duration>() {
    override fun areItemsTheSame(oldItem: Duration, newItem: Duration): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Duration, newItem: Duration): Boolean {
        return oldItem == newItem
    }
}