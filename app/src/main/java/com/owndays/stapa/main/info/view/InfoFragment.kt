package com.owndays.stapa.main.info.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.ServiceLocator
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.InfoFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.NotificationItemAdapter
import com.owndays.stapa.main.view.BADGE_NOTI
import com.owndays.stapa.main.view.MILEWAIT_FRAGMENT
import com.owndays.stapa.main.view.MainActivity
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.LocaleManager
import com.owndays.stapa.utils.getViewModelFactory

class InfoFragment : Fragment(){

    private lateinit var viewDataBinding: InfoFragmentBinding
    private lateinit var listAdapter: NotificationItemAdapter
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = InfoFragmentBinding.inflate(layoutInflater).apply {
            mainviewmodel = mainViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()

        mainViewModel.refreshNotifications()
    }

    private fun setupView(){

    }

    private fun setupEvent(){
        mainViewModel.apply {
            //Special case, refresh badge
            mileNotiDisplay.observe(viewLifecycleOwner, { _notiCount ->
                (activity as MainActivity)?.setBadge(BADGE_NOTI, _notiCount)
            })
            onOpenMileWait.observe(viewLifecycleOwner, EventObserver {
                (activity as MainActivity)?.navigate(MILEWAIT_FRAGMENT)
            })
            onOpenNotiDetail.observe(viewLifecycleOwner, EventObserver { _notificationId ->
                val notificationDetailDialogFragment = NotificationDetailDialogFragment(_notificationId)
                notificationDetailDialogFragment.show(childFragmentManager, NOTIFICATION_DETAIL_DIALOG_FRAGMENT)
            })
            onOpenOrderHistory.observe(viewLifecycleOwner, EventObserver {
                //TO ORDER HISTORY
            })
            onRefreshNoti.observe(viewLifecycleOwner, EventObserver {
                listAdapter.notifyDataSetChanged()
            })
            onCheckLanguage.observe(viewLifecycleOwner, EventObserver { _langCode ->
                if(ServiceLocator.getLanguage() != _langCode){
                    ServiceLocator.setLanguage(_langCode)
                    LocaleManager.setLocale(requireContext())
                    (activity as MainActivity)?.recreate()
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun setupAdapter(){
        listAdapter = NotificationItemAdapter(mainViewModel)
        viewDataBinding.notiListRecyclerView.adapter = listAdapter
    }
}