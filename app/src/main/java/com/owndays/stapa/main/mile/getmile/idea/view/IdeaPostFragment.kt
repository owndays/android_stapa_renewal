package com.owndays.stapa.main.mile.getmile.idea.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.owndays.stapa.R
import com.owndays.stapa.databinding.IdeaPostFragmentBinding
import com.owndays.stapa.main.view.IDEA_CREATEPOST_FRAGMENT
import com.owndays.stapa.main.view.IDEA_POST_HISTORY_FRAGMENT
import com.owndays.stapa.main.view.IDEA_TIMELINE_FRAGMENT
import com.owndays.stapa.main.view.MainActivity

class IdeaPostFragment : Fragment(){

    private lateinit var viewDataBinding : IdeaPostFragmentBinding

    companion object{
        fun newInstance() : IdeaPostFragment{
            return IdeaPostFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = IdeaPostFragmentBinding.inflate(layoutInflater).apply {
            navclick = navClickListener
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupView()
        setupEvent()
    }

    private fun setupView(){

    }

    private fun setupEvent(){

    }

    private val navClickListener = View.OnClickListener {
        when(it.id){
            R.id.btnIdeaPost -> {
                (activity as MainActivity)?.navigate(IDEA_CREATEPOST_FRAGMENT)
            }
            R.id.btnIdeaPostHistory -> {
                (activity as MainActivity)?.navigate(IDEA_POST_HISTORY_FRAGMENT)
            }
            R.id.btnOtherIdeaPost -> {
                (activity as MainActivity)?.navigate(IDEA_TIMELINE_FRAGMENT)
            }
        }
    }
}