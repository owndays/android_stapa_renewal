package com.owndays.stapa.main.mile.getmile.idea.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.data.model.Category
import com.owndays.stapa.databinding.IdeaFilterCategoryDialogFragmentBinding
import com.owndays.stapa.main.adapters.CategoryItemAdapter
import com.owndays.stapa.main.adapters.MODE_IDEA_CATEGORY
import com.owndays.stapa.main.adapters.viewmodel.CategoryViewModel
import com.owndays.stapa.main.mile.getmile.idea.viewmodel.IdeaViewModel
import com.owndays.stapa.utils.getViewModelFactory

const val IDEA_FILTER_CATEGORY_DIALOG_FRAGMENT = "IdeaFilterCategoryDialogFragment"

class IdeaFilterCategoryDialogFragment(private val mode: String) : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: IdeaFilterCategoryDialogFragmentBinding
    private lateinit var listAdapter: CategoryItemAdapter
    private val ideaViewModel by activityViewModels<IdeaViewModel> { getViewModelFactory() }
    private val categoryViewModel by activityViewModels<CategoryViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = IdeaFilterCategoryDialogFragmentBinding.inflate(layoutInflater).apply {
            ideaviewmodel = ideaViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewDataBinding.apply {
            if(ideaViewModel.selectedCategoryItem.value == null)
                categoryViewModel.setCategoryItem(Category(0, getString(R.string.store_all_item_category0)))
            btnClose.setOnClickListener {
                dismiss()
            }
            btnComplete.setOnClickListener {
                ideaViewModel.setFilterCategory(categoryViewModel.selectedCategoryItem.value, mode)
                dismiss()
            }
        }
    }

    private fun setupEvent(){
        //Special case, refresh check on category item
        categoryViewModel.selectedCategoryItem.observe(viewLifecycleOwner, {
            listAdapter.notifyDataSetChanged()
        })
    }

    private fun setupAdapter(){
        listAdapter = CategoryItemAdapter(MODE_IDEA_CATEGORY, categoryViewModel)
        viewDataBinding.categoryListRecyclerView.adapter = listAdapter
    }
}