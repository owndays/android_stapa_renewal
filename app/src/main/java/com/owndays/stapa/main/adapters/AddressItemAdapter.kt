package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.owndays.stapa.data.model.Address
import com.owndays.stapa.databinding.AddressItemBinding
import com.owndays.stapa.databinding.AddressStoreItemBinding
import com.owndays.stapa.databinding.CategoryItemBinding
import com.owndays.stapa.databinding.IdeaCategoryItemBinding
import com.owndays.stapa.main.adapters.AddressItemAdapter.ViewHolder
import com.owndays.stapa.main.setting.view.AddressItemSwipeListener
import com.owndays.stapa.main.setting.viewmodel.AddressViewModel
import okhttp3.internal.notify

const val MODE_PROFILE_ADDRESS = "ProfileAddress"
const val MODE_STORE_ADDRESS = "StoreAddress"

class AddressItemAdapter(private val mode: String,
                         private val addressViewModel: AddressViewModel,
                         private val onEditAddress: (address: Address) -> Unit,
                         private val onShortDeleteAddress: (address: Address) -> Unit) : ListAdapter<Address, ViewHolder>(AddressDiffCallback()), AddressItemSwipeListener{

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(mode, item, addressViewModel, onEditAddress, onShortDeleteAddress)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if(mode == MODE_PROFILE_ADDRESS)
            return ViewHolder.fromProfile(parent, mode)
        else return ViewHolder.fromStore(parent, mode)
    }

    class ViewHolder : RecyclerView.ViewHolder {

        private lateinit var addressItemBinding: AddressItemBinding
        private lateinit var addressStoreItemBinding: AddressStoreItemBinding
        private var mode: String

        constructor(binding: AddressItemBinding, _mode: String) : super(binding.root) {
            mode = _mode
            addressItemBinding = binding
        }

        constructor(binding: AddressStoreItemBinding, _mode: String) : super(binding.root) {
            mode = _mode
            addressStoreItemBinding = binding
        }

        fun bind(mode: String,
                 address: Address,
                 addressViewModel: AddressViewModel,
                 onEditAddress: (address: Address) -> Unit,
                 onShortDeleteAddress: (address: Address) -> Unit) {
            if(mode == MODE_PROFILE_ADDRESS){
                addressItemBinding.apply {
                    addressitem = address
                    addressviewmodel = addressViewModel

                    layoutBg.setOnClickListener { onEditAddress.invoke(address) }
                    btnDelete.setOnClickListener { onShortDeleteAddress.invoke(address) }
                }
                addressItemBinding.executePendingBindings()
            }else if(mode == MODE_STORE_ADDRESS){
                addressStoreItemBinding.apply {
                    addressitem = address
                    addressviewmodel = addressViewModel

                    imgCheck.isSelected = address.id == addressViewModel.addressItem.value?.id

                    layoutBg.setOnClickListener {
                        addressViewModel.setAddress(address)
                        notifyChange()
                    }
                }
                addressStoreItemBinding.executePendingBindings()
            }
        }

        companion object {
            fun fromProfile(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = AddressItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding, mode)
            }

            fun fromStore(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = AddressStoreItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding, mode)
            }
        }
    }

    private fun refreshShowDelete(){
        for(i in 0 until itemCount){
            getItem(i).showDelete = false
        }
    }

    override fun onItemSwipe(position: Int, direction: Int) {
        if(direction == ItemTouchHelper.LEFT){
            val item = getItem(position)
            if(!item.default.isNullOrEmpty() && item.default != "0"){
                refreshShowDelete()
                getItem(position).showDelete = true
                notifyDataSetChanged()
            }
        }else if(direction == ItemTouchHelper.RIGHT){
            getItem(position).showDelete = false
            notifyDataSetChanged()
        }
    }
}

class AddressDiffCallback : DiffUtil.ItemCallback<Address>() {
    override fun areItemsTheSame(oldItem: Address, newItem: Address): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Address, newItem: Address): Boolean {
        return oldItem == newItem
    }
}