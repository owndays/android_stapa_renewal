package com.owndays.stapa.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.owndays.stapa.R
import com.owndays.stapa.data.model.Product
import com.owndays.stapa.databinding.ProductListCartItemBinding
import com.owndays.stapa.databinding.ProductListItemBinding
import com.owndays.stapa.databinding.ProductStoreItemBinding
import com.owndays.stapa.main.adapters.ProductItemAdapter.ViewHolder
import com.owndays.stapa.main.store.viewmodel.StoreViewModel
import java.text.DecimalFormat

const val PRODUCT_STORE_MODE = "ProductStoreMode"
const val PRODUCT_LIST_MODE = "ProductListMode"
const val PRODUCT_CART_MODE = "ProductCartMode"
const val PRODUCT_CHECKOUT_MODE = "ProductCheckoutMode"

class ProductItemAdapter(private val mode: String,
                         private val openProductDetail: (product: Product) -> Unit,
                         private val deleteProduct: (product: Product) -> Unit,
                         private val onChangeAmount: (product: Product) -> Unit) : ListAdapter<Product, ViewHolder>(ProductDiffCallback()){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item, mode, openProductDetail, deleteProduct, onChangeAmount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (mode == PRODUCT_STORE_MODE) {
            return ViewHolder.fromStore(parent, mode)
        } else if (mode == PRODUCT_LIST_MODE) {
            return ViewHolder.fromList(parent, mode)
        } else {
            return ViewHolder.fromCart(parent, mode)
        }
    }

    class ViewHolder : RecyclerView.ViewHolder {

        private lateinit var productStoreItemBinding: ProductStoreItemBinding
        private lateinit var productListItemBinding: ProductListItemBinding
        private lateinit var productListCartItemBinding: ProductListCartItemBinding
        private var mode: String

        constructor(binding: ProductStoreItemBinding, _mode: String) : super(binding.root) {
            mode = _mode
            productStoreItemBinding = binding
        }

        constructor(binding: ProductListItemBinding, _mode: String) : super(binding.root) {
            mode = _mode
            productListItemBinding = binding
        }

        constructor(binding: ProductListCartItemBinding, _mode: String) : super(binding.root) {
            mode = _mode
            productListCartItemBinding = binding
        }

        fun bind(product: Product,
                 mode: String,
                 openProductDetail: (product: Product) -> Unit,
                 deleteProduct: (product: Product) -> Unit,
                 onChangeAmount: (product: Product) -> Unit) {
            if(mode == PRODUCT_STORE_MODE){
                productStoreItemBinding.apply {
                    productitem = product

                    val formatter = DecimalFormat("#,###")
                    textMileAmount.text = formatter.format(product.mile)
                    Glide.with(root)
                        .load(product.imageUrl)
                        .placeholder(R.drawable.noimage)
                        .into(imgProduct)

                    layoutBg.setOnClickListener { openProductDetail(product) }
                }
                productStoreItemBinding.executePendingBindings()
            }else if(mode == PRODUCT_LIST_MODE){
                productListItemBinding.apply {
                    productitem = product

                    val formatter = DecimalFormat("#,###")
                    textMileAmount.text = formatter.format(product.mile)
                    Glide.with(root)
                        .load(product.imageUrl)
                        .placeholder(R.drawable.noimage)
                        .into(imgProduct)

                    layoutBg.setOnClickListener { openProductDetail(product) }
                }
                productListItemBinding.executePendingBindings()
            }else if(mode == PRODUCT_CART_MODE){
                productListCartItemBinding.apply {
                    productitem = product

                    val formatter = DecimalFormat("#,###")
                    textMileAmount.text = formatter.format(product.mile)
                    Glide.with(root)
                        .load(product.imageUrl)
                        .placeholder(R.drawable.noimage)
                        .into(imgProduct)

                    btnAmount.setOnClickListener { onChangeAmount(product) }
                    btnDelete.setOnClickListener { deleteProduct(product) }
                }
                productListCartItemBinding.executePendingBindings()
            }else if(mode == PRODUCT_CHECKOUT_MODE){
                productListCartItemBinding.apply {
                    productitem = product

                    val formatter = DecimalFormat("#,###")
                    textMileAmount.text = formatter.format(product.mile)
                    Glide.with(root)
                        .load(product.imageUrl)
                        .placeholder(R.drawable.noimage)
                        .into(imgProduct)

                    btnDelete.visibility = View.GONE
                }
                productListCartItemBinding.executePendingBindings()
            }
        }

        companion object {
            fun fromStore(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ProductStoreItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding, mode)
            }

            fun fromList(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ProductListItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding, mode)
            }

            fun fromCart(parent: ViewGroup, mode: String): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ProductListCartItemBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding, mode)
            }
        }
    }

    class ProductDiffCallback : DiffUtil.ItemCallback<Product>() {
        override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean {
            return oldItem == newItem
        }
    }
}