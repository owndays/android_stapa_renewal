package com.owndays.stapa.main.store.view

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.databinding.SortBottomDialogFragmentBinding
import com.owndays.stapa.main.adapters.SortItemAdapter
import com.owndays.stapa.main.store.viewmodel.StoreViewModel
import com.owndays.stapa.utils.getViewModelFactory

const val SORT_BOTTOMDIALOG_FRAGMENT = "SortBottomDialogFragment"

class SortBottomDialogFragment : BottomSheetDialogFragment(){

    private lateinit var viewDataBinding : SortBottomDialogFragmentBinding
    private lateinit var listAdapter : SortItemAdapter
    private val storeViewModel by activityViewModels<StoreViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = SortBottomDialogFragmentBinding.inflate(layoutInflater).apply {
            storeviewmodel = storeViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
        setupAdapter()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        //expanded bottom sheet just in case too many item
        return super.onCreateDialog(savedInstanceState).apply {
            setOnShowListener { dialog ->
                (dialog as BottomSheetDialog).findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)?.apply {
                    BottomSheetBehavior.from(this).state = BottomSheetBehavior.STATE_EXPANDED
                }
            }
        }
    }

    private fun setupView(){
        viewDataBinding.apply {
            btnClose.setOnClickListener { dismiss() }
        }
    }

    private fun setupEvent(){
        storeViewModel.onSortSelect.observe(viewLifecycleOwner, EventObserver {
            dismiss()
        })
    }

    private fun setupAdapter(){
        listAdapter = SortItemAdapter(storeViewModel)
        viewDataBinding.sortListRecyclerView.adapter = listAdapter
    }
}