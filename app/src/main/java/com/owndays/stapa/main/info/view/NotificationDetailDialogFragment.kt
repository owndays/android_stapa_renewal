package com.owndays.stapa.main.info.view

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_COMPACT
import androidx.fragment.app.activityViewModels
import com.owndays.stapa.R
import com.owndays.stapa.databinding.NotificationDetailDialogFragmentBinding
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.getViewModelFactory

const val NOTIFICATION_DETAIL_DIALOG_FRAGMENT = "NotificationDetailDialogFragment"

class NotificationDetailDialogFragment(private val notificationId: Int) : AppCompatDialogFragment(){

    private lateinit var viewDataBinding: NotificationDetailDialogFragmentBinding
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = NotificationDetailDialogFragmentBinding.inflate(layoutInflater).apply {
            mainviewmodel = mainViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()

        mainViewModel.loadNotificationDetail(notificationId)
    }

    private fun setupView(){
        val width = ViewGroup.LayoutParams.MATCH_PARENT
        val height = ViewGroup.LayoutParams.MATCH_PARENT
        requireDialog().window?.setLayout(width, height)
        requireDialog().window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun setupEvent(){
        //special case, setup view with notification detail
        mainViewModel.notiItem.observeForever { _notification ->
            viewDataBinding.apply {
                textNotiTitle.text = _notification.title
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                    textBody.text = Html.fromHtml(_notification.body?:"", FROM_HTML_MODE_COMPACT)
                else textBody.text = Html.fromHtml(_notification.body?:"")
                when(_notification.type){
                    3 -> { imgIcon.setImageResource(R.drawable.ic_noti_info) }
                    //HTML
                    4 -> { imgIcon.setImageResource(R.drawable.ic_noti_info)}
                    //HTML
                    5 -> { imgIcon.setImageResource(R.drawable.ic_noti_star) }
                    else -> { imgIcon.setImageResource(R.drawable.ic_noti_info)}
                }
                btnBack.setOnClickListener{
                    dismiss()
                }
            }
        }
    }
}