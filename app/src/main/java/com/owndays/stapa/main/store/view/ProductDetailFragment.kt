package com.owndays.stapa.main.store.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.owndays.stapa.R
import com.owndays.stapa.data.EventObserver
import com.owndays.stapa.data.model.Slider
import com.owndays.stapa.databinding.ProductDetailFragmentBinding
import com.owndays.stapa.dialog.view.DEFAULT_OK_DIALOG
import com.owndays.stapa.dialog.view.DefaultOkDialog
import com.owndays.stapa.main.adapters.SliderItemAdapter
import com.owndays.stapa.main.store.viewmodel.StoreViewModel
import com.owndays.stapa.main.viewmodel.MainViewModel
import com.owndays.stapa.utils.getViewModelFactory
import timber.log.Timber
import java.text.DecimalFormat

class ProductDetailFragment : Fragment(){

    private lateinit var viewDataBinding: ProductDetailFragmentBinding
    private lateinit var sliderAdapter: SliderItemAdapter
    private val storeViewModel by activityViewModels<StoreViewModel> { getViewModelFactory() }
    private val mainViewModel by activityViewModels<MainViewModel> { getViewModelFactory() }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewDataBinding = ProductDetailFragmentBinding.inflate(layoutInflater).apply {
            storeviewmodel = storeViewModel
        }
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = viewLifecycleOwner
        setupView()
        setupEvent()
    }

    private fun setupView(){
        viewDataBinding.apply {
            val configuration = resources.configuration
            val screenWidthDp = configuration.screenWidthDp
            val sliderHeight = screenWidthDp * 0.855

            val scale = resources.displayMetrics.density

            var sliderParam = newsSliderPager.layoutParams
            sliderParam.height = (sliderHeight * scale + 0.5).toInt()
            newsSliderPager.layoutParams = sliderParam

            val formatter = DecimalFormat("#,###")
            textMileAmount.text = formatter.format(mainViewModel.mileItem.value?.balance?:0)

            btnAddCart.setOnClickListener {
                storeViewModel.addProductToCart()
                val cartDialogFragment = CartDialogFragment()
                cartDialogFragment.show(childFragmentManager, CART_DIALOG_FRAGMENT)
            }
        }
    }

    private fun setupEvent(){
        storeViewModel.apply {
            onLoadProductComplete.observe(viewLifecycleOwner, EventObserver { _product ->
                var sliderToDisplay = ArrayList<Slider>()
                _product.imageUrls?.forEach { _imgUrl ->
                    sliderToDisplay.add(Slider(0, _imgUrl))
                }
                if(sliderToDisplay.isNotEmpty()){
                    sliderAdapter = SliderItemAdapter()
                    sliderAdapter.updateDataItems(sliderToDisplay)
                } else {
                    sliderAdapter = SliderItemAdapter()
                    var singleSliderArray = ArrayList<Slider>().apply { add(Slider(0, "")) }
                    sliderAdapter.updateDataItems(singleSliderArray)
                }
                viewDataBinding.apply {
                    newsSliderPager.adapter = sliderAdapter
                    TabLayoutMediator(layoutTab, newsSliderPager) { _, _ -> }.attach()

                    //Add tab padding >> could do layer-list but transparent area will be clickable
                    val sideMargin = (resources.getDimension(R.dimen.icon_size_semismall) / resources.displayMetrics.density).toInt()
                    (layoutTab.getChildAt(0) as ViewGroup)?.children.forEach { _tab ->
                        val newMarginParam  = (_tab.layoutParams as ViewGroup.MarginLayoutParams)
                        newMarginParam?.setMargins(sideMargin, 0 , sideMargin, 0)
                        _tab.requestLayout()
                    }
                    layoutTab.requestLayout()

                    autoPlay(newsSliderPager)

                    val formatter = DecimalFormat("#,###")
                    textProductMileAmount.text = formatter.format(_product.mile?:0)

                    btnAddCart.isEnabled = true
                    btnAddCart.text = getString(R.string.store_add_cart_btn)

                    //Check purchase allow
                    if(_product.purchaseAllow != true || _product.purchaseAllow == null){
                        btnAddCart.isEnabled = false
                        btnAddCart.text = getString(R.string.store_item_detail_already_purchased)
                    }
                    //Check Mile Enough
                    if(mainViewModel.mileItem.value?.balance?:0 < _product.mile?:0){
                        btnAddCart.isEnabled = false
                        btnAddCart.text = getString(R.string.store_cart_not_enough_mile)
                    }
                    //Check Quantity
                    if(_product.stock?.qty == 0 || _product.stock?.qty == null){
                        btnAddCart.isEnabled = false
                        btnAddCart.text = getString(R.string.store_cart_out_of_stock)
                    }
                    if(btnAddCart.isEnabled)
                        btnAddCart.icon = requireContext().getDrawable(R.drawable.ic_cart)
                    else btnAddCart.icon = null
                }
            })
            onPopupApiError.observe(viewLifecycleOwner, EventObserver { errorMessage ->
                val errorDialog = DefaultOkDialog("", errorMessage) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
            onPopupError.observe(viewLifecycleOwner, EventObserver { errorStringId ->
                val errorDialog = DefaultOkDialog("", getString(errorStringId)) {}
                errorDialog.show(childFragmentManager, DEFAULT_OK_DIALOG)
            })
        }
    }

    private fun autoPlay(viewPager: ViewPager2){
        var currentCount = viewPager.currentItem
        viewPager.postDelayed({run{
            try{
                if(viewPager.adapter != null && viewPager.adapter?.itemCount?:0 > 0){
                    if(currentCount >= ((viewPager.adapter?.itemCount?:0) - 1))
                        currentCount = 0
                    else currentCount++
                    viewPager.currentItem = currentCount
                    autoPlay(viewPager)
                }
            }catch (e: Exception){
                Timber.e(e.message)
            }
        } }, 4000)
    }
}